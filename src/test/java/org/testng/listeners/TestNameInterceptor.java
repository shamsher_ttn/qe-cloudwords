package org.testng.listeners;

import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;

import org.testng.IMethodInstance;
import org.testng.IMethodInterceptor;
import org.testng.ITestContext;
import org.testng.annotations.Test;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.tm.TestlinkIntegration;

public class TestNameInterceptor implements IMethodInterceptor {

  List<Integer> testnamesFromTL = new ArrayList<Integer>();

  public List<IMethodInstance> intercept(
      List<IMethodInstance> methods, ITestContext context) {
    int testPlanId = Integer.parseInt(Automation.configHashMap.get("PLAN_ID"));

    List<IMethodInstance> result = new ArrayList<IMethodInstance>();

    try {
      if (testnamesFromTL.size() == 0) {
        testnamesFromTL = getTestnamesFromTestlink(testPlanId);
      }
      for (IMethodInstance m : methods) {
        Test test = m.getMethod().getConstructorOrMethod()
            .getMethod().getAnnotation(Test.class);
        if (testnamesFromTL.contains(Integer.parseInt(test.testName()))) {
          result.add(m);
        }
      }

    } catch (MalformedURLException e) {
      e.printStackTrace();
    }

    return result;
  }

  private List<Integer> getTestnamesFromTestlink(int testPlanId)
      throws MalformedURLException {
    TestlinkIntegration tl = new TestlinkIntegration();
    return tl.getTestcaseByTestplanId(testPlanId);
  }
}