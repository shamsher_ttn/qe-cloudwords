/**
 * This test script is intend to create to test customers through system admin functionality
 *
 */

package com.cloudwords.qe.testsuites.usercreation;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.CustomerQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.AdminPage;
import com.cloudwords.qe.fw.pageobjects.CustomerCreationPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class CustomerEditTest extends BrowserStackTestNG {
  String testCaseName = "CustomerEditTest";
  private static final Logger logger = Logger.getLogger(CustomerEditTest.class);


  @Test(dataProvider = "getCustomerData", testName = "7")
  public void TestCustomerEdit(Hashtable<String, String> data) {

    testReport = extent.startTest("Customer edit Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
    }
    try {
      if (CustomerQueries.verifyUser(data.get("CustomerEmail"))) {
    	  driver.get(Automation.configHashMap.get("BASEURL"));
          LoginUtil.loginWithCredentials(data.get("AdminUser"), data.get("AdminPwd"));
          AdminPage page = new AdminPage(driver, testReport);
          page.clickCustCreateLink();
          CustomerCreationPage create = new CustomerCreationPage(driver, testReport);
          create.editCustomer(data.get("CustomerName"), data.get("AccountStatusIndex"),data.get("EditVendorID"));
          /*create.createCustomer(data.get("CustomerName"), data.get("CustomerEmail"),
              data.get("CustFirstName"), data.get("CustLastName"), data.get("Password"), data.get("BrandIdx"),
              data.get("EditionIdx"),data.get("AccountStatusIndex"),data.get("VendorID"));*/
          Thread.sleep(5000L);
          boolean status = CustomerQueries.verifyUser(data.get("CustomerEmail"));
          if (status) {
            s_assert.assertTrue(true, "Customer is edited. Test Case is pass");
          } else {
            s_assert.assertTrue(false, "Customer is not available. Test Case is fail");
          }
      }else{
       Assert.assertTrue(false);
      }
    }catch (InterruptedException e1) {
      e1.printStackTrace();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
    LoginUtil.logout();
    s_assert.assertAll();
  }



  @DataProvider
  public Object[][] getCustomerData() {
    return TestUtil.getData(testCaseName, xls);
  }

}
