/**
 * This test script is intend to create to test creation of vendor through system admin functionality
 *
 */

package com.cloudwords.qe.testsuites.usercreation;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.ChangePwdUtil;
import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.CustomerQueries;
import com.cloudwords.qe.fw.dbqueries.VendorQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.AdminPage;
import com.cloudwords.qe.fw.pageobjects.VendorCreationPage;
import com.cloudwords.qe.fw.utillib.FwUtil;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class VendorsCreationTest extends BrowserStackTestNG {
	String testCaseName = "VendorCreationTest";
	private static final Logger logger = Logger.getLogger(VendorsCreationTest.class);
	@Test(dataProvider = "getVendordata", testName = "2604")
	public void VendorCreationTest(Hashtable<String, String> data) throws InterruptedException {

		testReport = extent.startTest("VendorCreation Test");
		if (!TestUtil.isExecutable(testCaseName, xls) || data
				.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
			testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
			throw new SkipException("Skipping the test as Rnumode is N");
		}else{
			try {
				if (CustomerQueries.verifyUser(data.get("Email"))) {
					throw new SkipException("Skipping the test as Test Data is already available in Db");
				}else{
					driver.get(Automation.configHashMap.get("BASEURL"));
					LoginUtil.loginWithCredentials(data.get("AdminUser"), data.get("AdminPwd"));
					AdminPage page = new AdminPage(driver, testReport);
					page.clickvendorCreateLink();
					VendorCreationPage create = new VendorCreationPage(driver, testReport);
					create.createAutomateVendor(data.get("VendorName"), data.get("Email"),
							data.get("VendorFirstName"), data.get("VendorLastName"), data.get("Password"),
							data.get("BrandIndex"),data.get("VendorType"));
					Thread.sleep(10000L);
					FwUtil.storeData(data.get("VendorID"), String.valueOf(VendorQueries.getVendorID(data.get("VendorName"))));
					int intValue = VendorQueries.verifyAutomateVendor(data.get("VendorName"));
					if (intValue == 1) {
						//FwUtil.storeData(data.get("VendorID"), String.valueOf(VendorQueries.getVendorID(data.get("AutomateVendorName"))));
						Assert.assertEquals(data.get("VendorType"), "Automate");
						Assert.assertTrue(true, "Vendor is automated vendor. Test Case is pass");
					} else {
						Assert.assertTrue(true, "Vendor is manual vendor. Test Case is pass");
					}
					if (CustomerQueries.verifyUser(data.get("Email"))) {
						ChangePwdUtil.ChangePassword(data.get("Password"), data.get("NewPassword"),
								data.get("Email"),data.get("VendorFirstName"), data.get("VendorLastName"));
					} else {
						Assert.assertTrue(false);
					}
				}
			}catch (SQLException e) {
				e.printStackTrace();
			}
		}
		LoginUtil.logout();
	}

	@DataProvider
	public Object[][] getVendordata() {
		return TestUtil.getData(testCaseName, xls);
	}

}
