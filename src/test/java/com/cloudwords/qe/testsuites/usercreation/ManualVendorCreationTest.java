/**
 * This test script is intend to create to test creation of vendor through system admin functionality
 *
 */

package com.cloudwords.qe.testsuites.usercreation;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.ChangePwdUtil;
import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.CustomerQueries;
import com.cloudwords.qe.fw.dbqueries.VendorQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.AdminPage;
import com.cloudwords.qe.fw.pageobjects.VendorCreationPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class ManualVendorCreationTest extends BrowserStackTestNG {
  String testCaseName = "ManualVendorCreationTest";
  private static final Logger logger = Logger.getLogger(ManualVendorCreationTest.class);

  @Test(dataProvider = "getVendordata", testName = "7")
  public void VendorCreationTest(Hashtable<String, String> data) throws InterruptedException {

    testReport = extent.startTest("AutomateVendorCreation Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    try {
      if (CustomerQueries.verifyUser(data.get("ManualEmail"))) {
        throw new SkipException("Skipping the test as Test Data is already available in Db");
      }
      driver.get(Automation.configHashMap.get("BASEURL"));
      LoginUtil.loginWithCredentials(data.get("AdminUser"), data.get("AdminPwd"));
      AdminPage page = new AdminPage(driver, testReport);
      page.clickvendorCreateLink();
      VendorCreationPage create = new VendorCreationPage(driver, testReport);
      create.createManualVendor(data.get("ManualVendorName"), data.get("ManualEmail"),
          data.get("VendorFirstName"), data.get("VendorLastName"), data.get("Password"),
          data.get("BrandIndex"));
      Thread.sleep(10000L);
      int intValue = VendorQueries.verifyAutomateVendor(data.get("ManualVendorName"));
      if (intValue == 1) {
        Assert.assertTrue(false, "Vendor is automated vendor. Test Case is fail");
      } else {
    	  Automation.STOREHASHMAP.put(data.get("VendorID"),
                  String.valueOf(VendorQueries.getVendorID(data.get("ManualVendorName"))));
          Assert.assertTrue(true, "Vendor is not automated vendor. Test Case is pass");
      }
      if (CustomerQueries.verifyUser(data.get("ManualEmail"))) {
          ChangePwdUtil.ChangePassword(data.get("Password"), data.get("NewPassword"),
              data.get("ManualEmail"),data.get("VendorFirstName"), data.get("VendorLastName"));
        } else {
          Assert.assertTrue(false);
        }
    } catch (SQLException e) {
      e.printStackTrace();
    }
    LoginUtil.logout();
  }



  @DataProvider
  public Object[][] getVendordata() {
    return TestUtil.getData(testCaseName, xls);
  }

}
