/**
 * This test script is intend to test creation of internal translator role users
 * for customer functionality specifically for indesign files translation project
 *
 */

package com.cloudwords.qe.testsuites.usercreation;

import java.sql.SQLException;
import java.util.Hashtable;

import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.testng.log4testng.Logger;

import com.cloudwords.qe.fw.apputil.ChangePwdUtil;
import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.CustomerQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.LandingPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class CustomerAdminCreateInternalTranslaorForInDesignTest extends Automation {

	String testCaseName = "CustomerAdminCreateInternalTranslatorUserTest";
	private static final Logger logger = Logger.getLogger(CustomerAdminCreateInternalTranslaorForInDesignTest.class);

	@Test(dataProvider = "getInternalTranslatorForInDesignData")
	public void test(Hashtable<String, String> data) throws InterruptedException {
		testReport = extent.startTest("CustomerAdminCreatePMTest");
		if (!TestUtil.isExecutable(testCaseName, xls)
				|| data.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
			testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
			throw new SkipException("Skipping the test as Rnumode is N");
		}
		testReport.log(LogStatus.INFO, "Starting login test");
		testReport.log(LogStatus.INFO, "Opening browser");
		driver.get(Automation.configHashMap.get("BASEURL"));
		try {
			if (CustomerQueries.verifyUser(data.get("Email"))) {
				throw new SkipException("Skipping the test as Test Data is already available in Db");
			} else {
				testReport.log(LogStatus.INFO, "Starting login test");
				testReport.log(LogStatus.INFO, "Opening browser");
				driver.get(Automation.configHashMap.get("BASEURL"));
				testReport.log(LogStatus.INFO, "Logging in");
				LoginUtil.doLogin(data.get("Username"), data.get("Password"));
				LandingPage landingPage = new LandingPage(driver, testReport);
				landingPage.createNewInternalTranslatorUserbyCustomerAdminAndEnabled(data.get("FirstName"),
						data.get("LastName"), data.get("Email"), data.get("Role"));
				Thread.sleep(5000L);
				if (CustomerQueries.verifyUser(data.get("Email"))) {
					ChangePwdUtil.ChangePassword(data.get("OldPassword"), data.get("NewPassword"), data.get("Email"),
							data.get("FirstName"), data.get("LastName"));
				} else {
					Assert.assertTrue(false);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		LoginUtil.logout();
		s_assert.assertAll();
	}

	@DataProvider
	public Object[][] getInternalTranslatorForInDesignData() {
		return TestUtil.getData(testCaseName, xls);
	}

}
