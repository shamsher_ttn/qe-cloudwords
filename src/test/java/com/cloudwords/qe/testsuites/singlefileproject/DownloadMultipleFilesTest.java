/**
 * This test script is intend to test scenario to download multiple files from project
 *
 */package com.cloudwords.qe.testsuites.singlefileproject;

import java.io.File;
import java.util.Hashtable;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.FileHandler;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.FileUtil;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class DownloadMultipleFilesTest extends BrowserStackTestNG {
  String testCaseName = "DownloadMultipleFilesTest";
  private static final Logger logger = Logger.getLogger(DownloadMultipleFilesTest.class);


  @Test(testName = "7", dataProvider = "getData")
  public void MultipleFileTest(Hashtable<String, String> data) throws Exception {
    testReport = extent.startTest("Download File Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    driver.get(Automation.configHashMap.get("BASEURL"));
    LoginUtil.doLogin(data.get("ReviewerUserName"), data.get("ReviewerPwd"));
    driver.findElement(By.xpath("//*[@id='taskStatusFilter']/div/a[1]")).click();
    WebElement paramWebElement = driver
        .findElement(By.xpath("//*[@id='paneListContentContainer']/table"));
    String[] arrayColumnData = WebTable.StoreColumnData(paramWebElement, data.get("StoreKeyName"),
        data.get("ColumnNum"));
    int i;
    for (i = 0; i < arrayColumnData.length; i++) {
      if (arrayColumnData[i].contains(data.get("ProjectName"))) {
        break;
      }
    }
    int rowNum = i + 2;
    WebTable.ClickCell(paramWebElement, String.valueOf(rowNum), data.get("ColumnNum"));
    FileHandler.createFolder(
        PublicVariables.DOWNLOAFFILEPATH + PublicVariables.SEP + data.get("ProjectName"));
    CommonUtil.Wait(3);
    List<WebElement> tableList = driver.findElements(By.xpath(
        "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div/div[2]/table"));
    System.out.println(tableList.size());
    String part1 = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/div[1]/div/div[2]/table[";
    String part2 = "]";
    int column;
    for (int j = 1; j <= tableList.size(); j++) {
      WebElement locator = driver.findElement(By.xpath(part1 + j + part2));
      int rowCount = WebTable.getRowCount(locator);
      if (rowCount != 1) {
        for (int k = 1; k <= rowCount; k++) {
          if (j == 3) {
            column = 4;
          } else {
            column = 3;
          }
          String docName = driver
              .findElement(By.xpath(part1 + j + part2 + "/tbody/tr[" + k + "]/td[2]")).getText();
          System.out.println(docName);
          WebTable.ClickLinkAtCellTH(locator, "", String.valueOf(k), String.valueOf(column));
          String e = part1 + j + part2 + "/tbody/tr[" + k + "]/td[4]/div/div/ul/li";
          List<WebElement> list = driver.findElements(By.xpath(e));
          System.out.println(list);
          String ele1 = e + "[";
          String ele2 = "]/a";
          for (int l = 1; l <= list.size(); l++) {
            String finalloc = ele1 + l + ele2;
            WebElement loc = driver.findElement(By.xpath(finalloc));
            String fileName = loc.getText();
            FileHandler.createFolder(PublicVariables.DOWNLOAFFILEPATH + PublicVariables.SEP
                + data.get("ProjectName") + PublicVariables.SEP + docName);
            String fileURL = loc.getAttribute("href");
            if (!fileName.contains("Bilingual review file")) {
              driver.get(fileURL);
              CommonUtil.Wait(5);
              FileUtil.CopyandMoveFile(PublicVariables.DOWNLOAFFILEPATH + PublicVariables.SEP
                  + data.get("ProjectName") + PublicVariables.SEP + docName, fileName);
            } else {
              break;
            }
          }
          WebTable.ClickLinkAtCellTH(locator, "", String.valueOf(k), String.valueOf(column));
        }
      }
    }
    LoginUtil.logout();
  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }

  public boolean isFileDownloaded(String downloadPath, String fileName) {
    boolean flag = false;
    File dir = new File(downloadPath);
    File[] dir_contents = dir.listFiles();

    for (int i = 0; i < dir_contents.length; i++) {
      if (dir_contents[i].getName().equals(fileName))
        return flag = true;
    }

    return flag;
  }

}
