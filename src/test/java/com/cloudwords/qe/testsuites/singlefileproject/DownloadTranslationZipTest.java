/**
 * This test script is intend to create to test scenario to download translation zip file from project
 *
 */
package com.cloudwords.qe.testsuites.singlefileproject;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class DownloadTranslationZipTest extends BrowserStackTestNG {
  String testCaseName = "DownloadTranslationZipTest";
  private static final Logger logger = Logger.getLogger(DownloadTranslationZipTest.class);

  @Test(dataProvider = "getData", testName = "7")
  public void DownlaodTranslationsZipReviewerTest(Hashtable<String, String> data) {

    testReport = extent.startTest("Downlaod File Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    driver.get(Automation.configHashMap.get("BASEURL"));
    LoginUtil.doLogin(data.get("ReviewerLogin"), data.get("ReviewerPwd"));
    driver.findElement(By.xpath("//*[@id='taskStatusFilter']/div/a[1]")).click();
    WebElement paramWebElement = driver
        .findElement(By.xpath("//*[@id='paneListContentContainer']/table"));
    String[] arrayColumnData = WebTable.StoreColumnData(paramWebElement, data.get("StoreKeyName"),
        data.get("ColumnNum"));
    int i;
    for (i = 0; i < arrayColumnData.length; i++) {
      if (arrayColumnData[i].contains(data.get("ProjectName"))) {
        break;
      }
    }
    int rowNum = i + 2;
    WebTable.ClickCell(paramWebElement, String.valueOf(rowNum), data.get("ColumnNum"));
    CommonUtil.Wait(5);
    WebElement locator = driver.findElement(By.xpath("//*[@id='fileReviewViewContainerContent']/a[2]"));
    CommonUtil.Wait(5);
    locator.click();
    LoginUtil.logout();
  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }
}
