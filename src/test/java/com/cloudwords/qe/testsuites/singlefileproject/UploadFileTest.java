/**
 * This test script is intend to test scenario of uploading single or multiple file in project for translation
 *
 */
package com.cloudwords.qe.testsuites.singlefileproject;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.ProjectQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.ProjectCreationPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class UploadFileTest extends BrowserStackTestNG {
  String testCaseName = "UploadFileTest";
  private static final Logger logger = Logger.getLogger(UploadFileTest.class);

  @Test(dataProvider="getData",testName = "7")
  public void SingleFileUploadTest(Hashtable<String, String> data) throws SQLException {
    testReport = extent.startTest("Upload File Test");

    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    driver.get(Automation.configHashMap.get("BASEURL"));
    ProjectCreationPage createPrj = new ProjectCreationPage(driver, testReport);

    createPrj.SimpleSingleFileProjectCreation(data.get("PmUser"), data.get("PmPwd"),
        data.get("ProjectName"), data.get("IndexNoIntendedUse"), data.get("IndexNoPriority"),
        data.get("filePath"), data.get("SrcLangIndex"), data.get("TargetLangIndex"),data.get("Date"));
    int count = ProjectQueries.fileUploadCount(data.get("ProjectName"), data.get("language"));

    if (count != 1) {
      s_assert.assertTrue(false);
    }
    s_assert.assertTrue(true);
    LoginUtil.logout();
    s_assert.assertAll();
  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }

}
