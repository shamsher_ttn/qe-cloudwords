/**
 * This test scripts is intend to test scenarios when user 1st time login in system
 *
 */

package com.cloudwords.qe.testsuites.logintest;

import java.sql.SQLException;
import java.util.Hashtable;

import org.openqa.selenium.support.PageFactory;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.testng.log4testng.Logger;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.LoginQueries;
import com.cloudwords.qe.fw.pageobjects.LoginPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.fw.wrapper.General;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class FirstTimeLogin extends BrowserStackTestNG {
  private static final Logger logger = Logger.getLogger(FirstTimeLogin.class);

  @Test(dataProvider = "getFirstLoginData" ,testName="7")
  public void firstLogin(Hashtable<String, String> data) throws Throwable {
    if (!TestUtil.isExecutable("FirstLoginTest", xls))
      throw new SkipException("Skipping the test");
    LoginQueries.setFirstLogin(data.get("UserName"));
    try {
      LoginUtil.loginWithCredentials(data.get("UserName"), data.get("Password"));
      s_assert.assertTrue(driver.getTitle().contains("/verify-password"),
          "User logged in 1st time in system but sytem didn't show ");
    } catch (Exception | AssertionError e) {
      throw e;
    }
    s_assert.assertAll();
  }

  @Test(dataProvider = "getFirstLoginData",testName="7")
  public void firstLoginWithDisabledUser(Hashtable<String, String> data) throws Throwable {

    if (!TestUtil.isExecutable("FirstLoginTest", xls))
      throw new SkipException("Skipping the test");
    try {
      LoginQueries.setFirstLogin(data.get("Username"));
      LoginQueries.disableUserLogin(data.get("Username"));
      LoginPage login = PageFactory.initElements(driver, LoginPage.class);
      testReport.log(LogStatus.INFO,
          "Entering username and password for first login with disabled user..");
      login.enetrUserName(data.get("UserName"));
      login.enterPassWord(data.get("Password"));
      LoginUtil.assertLoginFail();
      LoginQueries.enableUserLogin(data.get("Username"));
    } catch (Exception | AssertionError e) {
      driver.get(Automation.configHashMap.get("BASEUR;"));
      // General.OpenURL();
      LoginQueries.enableUserLogin(data.get("Username"));
      logger.error(e.toString());
      throw e;
    }
    s_assert.assertAll();
  }

  @Test(dataProvider = "getFirstLoginData",testName="7")
  public void existingUser_Login(Hashtable<String, String> data) {

    if(!TestUtil.isExecutable("FirstLoginTest", xls))
      throw new SkipException("Skipping the test");
    try {
      LoginQueries.disableFirstLogin(data.get("Username"));
      LoginUtil.doLogin(data.get("Username"), data.get("Password"));
    } catch (SQLException e) {
      e.printStackTrace();
    }
    logger.info("First login is disabled for the user..");
    General.OpenURL();
    LoginUtil.loginWithCredentials(data.get("UserName"), data.get("Password"));
    s_assert.assertAll();
  }

  @DataProvider
  public Object[][] getFirstLoginData() {
    return TestUtil.getData("FirstLoginTest", xls);
  }
}
