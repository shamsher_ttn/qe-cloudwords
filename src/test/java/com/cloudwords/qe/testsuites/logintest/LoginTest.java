/**
 * This test scripts is intend to test scenarios of login functionality
 *
 */

package com.cloudwords.qe.testsuites.logintest;

import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.testng.log4testng.Logger;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.LoginQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.LandingPage;
import com.cloudwords.qe.fw.pageobjects.LoginPage;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class LoginTest extends BrowserStackTestNG {
  String testCaseName = "LoginTest";
  private static final Logger logger = Logger.getLogger(LoginTest.class);

  /**
   * @param data
   * @param testname
   * @throws InterruptedException
   */
  @Test(dataProvider = "getLoginData", testName = "5",priority=2)
  public void landingPageLinksVerification(Hashtable<String, String> data) throws InterruptedException {

    testReport = extent.startTest("Login Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    testReport.log(LogStatus.INFO, "Starting login test");
    testReport.log(LogStatus.INFO, "Opening browser");
    driver.get(Automation.configHashMap.get("BASEURL"));
    //General.OpenURL();
    testReport.log(LogStatus.INFO, "Logging in");
    Object page = LoginUtil.doLogin(data.get("Username"), data.get("Password"));
    CommonUtil.Wait(5);
    if (page instanceof LandingPage) {
      s_assert.assertTrue(true,"retuned object is landing page object");
      LoginUtil.logout();
    } else {
      s_assert.assertTrue(false,"retuned object is not landing page object");
    }
    s_assert.assertAll();
  }

  @Test(dataProvider = "getLoginData", testName = "7",priority=1)
  public void loginWithDisabledUserTest(Hashtable<String, String> data) {

    testReport = extent.startTest("Login with disabled user Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    testReport.log(LogStatus.INFO, "Starting Login with disabled user test");
    testReport.log(LogStatus.INFO, "Opening browser");

    try {
      LoginQueries.disableUserLogin(data.get("Username"));
      driver.get(Automation.configHashMap.get("BASEURL"));
      LoginPage login = new LoginPage(driver, testReport);
      logger.info("Disabling user and trying login");
      login.enetrUserName(data.get("Username"));
      login.enterPassWord(data.get("Password"));
      LoginUtil.assertLoginFail();
      logger.info("Enabling user and trying login");
      LoginQueries.enableUserLogin(data.get("Username"));
      LoginUtil.logout();
    } catch (Exception e) {
      System.out.println(e);
    }
    s_assert.assertAll();
  }

  @DataProvider
  public Object[][] getLoginData() {
    return TestUtil.getData(testCaseName, xls);
  }

}