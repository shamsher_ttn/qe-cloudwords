package com.cloudwords.qe.testsuites.base;

import java.io.FileReader;
import java.net.URL;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.browserstack.local.Local;
import com.cloudwords.qe.fw.base.Automation;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class BrowserStackTestNG extends Automation{

	private static final Logger logger = Logger.getLogger(BrowserStackTestNG.class);
	private Local l;

	@BeforeSuite(alwaysRun=true,enabled=false)
	@Parameters(value="config")
	public void Setup(String config_file) {
		try {
			Automation.LoadData("config/config.properties");
			Automation.LoadappData("config/appconfig.properties");
			Automation.LoadData("config/env_config.properties");
			Automation.setUp();
			driver.manage().timeouts().implicitlyWait(
					Long.parseLong(configHashMap.get("TIMEOUT").toString()), TimeUnit.SECONDS);

			JSONParser parser = new JSONParser();
			JSONObject config = (JSONObject) parser.parse(new FileReader("src/test/resources/bsconfig/" + config_file));

			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capabilities.setCapability("browser", configHashMap.get("browser"));
			capabilities.setCapability("browser_version", configHashMap.get("browser_version"));
			capabilities.setCapability("os", configHashMap.get("os"));
			capabilities.setCapability("os_version", configHashMap.get("os_version"));
			capabilities.setCapability("browserstack.debug", "true");


			Map<String, String> commonCapabilities = (Map<String, String>) config.get("capabilities");
			Iterator it = commonCapabilities.entrySet().iterator();
			while (it.hasNext()) {
				Map.Entry pair = (Map.Entry)it.next();
				if(capabilities.getCapability(pair.getKey().toString()) == null){
					capabilities.setCapability(pair.getKey().toString(), pair.getValue().toString());
				}
			}

			String username = System.getenv("BROWSERSTACK_USERNAME");
			if(username == null) {
				username = (String) config.get("user");
			}

			String accessKey = System.getenv("BROWSERSTACK_ACCESS_KEY");
			if(accessKey == null) {
				accessKey = (String) config.get("key");
			}

			if(capabilities.getCapability("browserstack.local") != null && capabilities.getCapability("browserstack.local") == "true"){
				l = new Local();
				Map<String, String> options = new HashMap<String, String>();
				options.put("key", accessKey);
				l.start(options);
			}

			TESTLINKMAP.put("CF_PLATFORM", configHashMap.get("os"));
			TESTLINKMAP.put("CF_BROWSER", configHashMap.get("browser"));
			TESTLINKMAP.put("CF_BROWSERVERSION", configHashMap.get("browser_version"));
			TESTLINKMAP.put("CF_PLATFORMVERSION", "os_version");

			driver = new RemoteWebDriver(new URL("http://"+username+":"+accessKey+"@"+config.get("server")+"/wd/hub"), capabilities);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	@AfterMethod(alwaysRun=true)
	public void quit() throws Exception  {
		if (extent != null) {
			extent.endTest(testReport);
			extent.flush();
			if (extent != null) {
				extent.endTest(testReport);
				extent.flush();
			}
		}

	}

	@AfterSuite(alwaysRun=true)
	public void close() throws Exception{
		driver.quit();
		extent.close();
	}
}
