/**
 * This test script intend to test scenario of creating translation
 * project with multiple files upload for translation
 */
package com.cloudwords.qe.testsuites.multiplefileproject;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.ProjectQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.ProjectCreationPage;
import com.cloudwords.qe.fw.utillib.FileUtil;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class MultipleFileUploadTest extends BrowserStackTestNG {

	private static final Logger logger = Logger.getLogger(MultipleFileUploadTest.class);

	String testCaseName = "MultipleFilesUploadTest";// getClass().getName().toString();

	@Test(dataProvider="getData",testName = "7")

	public void MultipleFilesUploadTest(Hashtable<String, String> data) throws SQLException {

		testReport = extent.startTest("Upload File Test");

		if (!TestUtil.isExecutable(testCaseName, xls) || data
				.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
			testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
			throw new SkipException("Skipping the test as Rnumode is N");
		}
		driver.get(Automation.configHashMap.get("BASEURL"));
		ProjectCreationPage createPrj = new ProjectCreationPage(driver, testReport);
		createPrj.SimpleMultipleFilesProjectCreation(data.get("PmUser"), data.get("PmPwd"),
				data.get("ProjectName"), data.get("IndexNoIntendedUse"), data.get("IndexNoPriority"),
				data.get("DirPath"), data.get("SrcLangIndex"), data.get("TargetLangIndex"),data.get("Date"),data.get("VendorName"));
		int uploadedFilesCount = FileUtil.getFilesCount( data.get("DirPath"));
		int dbCount = ProjectQueries.fileUploadCount(data.get("ProjectName"), data.get("language"));

		if (uploadedFilesCount != dbCount) {
			Assert.assertTrue(false, "Uploaded files not updated in db properly");
		}
		Assert.assertTrue(true);
		LoginUtil.logout();
	}

	@DataProvider
	public Object[][] getData() {
		return TestUtil.getData(testCaseName, xls);
	}
}