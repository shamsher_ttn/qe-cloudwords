/**
 * This test script intend to test scenario of creating translation
 * project with multiple combination i.e. selecting different languages
 * add multiple files
 */
package com.cloudwords.qe.testsuites.multiplefileproject;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.ProjectQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.ProjectCreationPage;
import com.cloudwords.qe.fw.utillib.FileUtil;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class MultiCombPrjTest extends BrowserStackTestNG {

  private static final Logger logger = Logger.getLogger(MultiCombPrjTest.class);
  String testCaseName = "MultipleLocalFilesProjectTest";

  @Test(dataProvider = "getData", testName = "2626")
  public void MultipleFilesUploadTest(Hashtable<String, String> data) throws SQLException {

    testReport = extent.startTest("Upload File Test");
    try {
      if (!TestUtil.isExecutable(testCaseName, xls) || data
          .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
        testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
        throw new SkipException("Skipping the test as Rnumode is N");
      }
      driver.get(Automation.configHashMap.get("BASEURL"));
      ProjectCreationPage createPrj = new ProjectCreationPage(driver, testReport);
      createPrj.LocalProjectCreation(data.get("PmUser"), data.get("PmPwd"),
          data.get("ProjectName"), data.get("IntendedUse"), data.get("Priority"),
          data.get("DirPath"), data.get("SrcLang"), data.get("TargetLang"),
          data.get("Date"),data.get("VendorName"));
      Thread.sleep(5000L);
      int uploadedFilesCount = (FileUtil.getFilesCount(PublicVariables.SORUCEFILEPATH));
      Thread.sleep(15000L);
      int dbCount = ProjectQueries.fileUploadCount(data.get("ProjectName"), data.get("language"));
      if (uploadedFilesCount != dbCount) {
        s_assert.assertTrue(false, "Uploaded files not updated in db properly");
      }else{
        s_assert.assertTrue(true);
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    LoginUtil.logout();
    s_assert.assertAll();
  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }

}
