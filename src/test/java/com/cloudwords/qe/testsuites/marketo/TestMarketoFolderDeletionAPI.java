/**
 *
 */
package com.cloudwords.qe.testsuites.marketo;

import java.text.ParseException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.marketoapi.MarketoDataDeletion;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author shamsher
 * @date 14-Nov-2016
 */
@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })

public class TestMarketoFolderDeletionAPI extends BrowserStackTestNG{
  String testCaseName = "DeleteFolderMarketoUsingAPI";
  private static final Logger logger = Logger.getLogger(TestMarketoFolderDeletionAPI.class);

  @Test(testName = "2577",dataProvider="getMarketoData")
  public void MarketoFolderDeletion(Hashtable<String, String>data) throws ParseException, InterruptedException {
    try{
      testReport = extent.startTest("DeleteFolderMarketoUsingAPI");
      logger.info("Launching test case DeleteFolderMarketo");
      if (!TestUtil.isExecutable(testCaseName, xls) || data
          .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
        testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
        throw new SkipException("Skipping the test as Rnumode is N");
      }
      testReport.log(LogStatus.INFO, "Starting Marketo delete test");
      MarketoDataDeletion page = new MarketoDataDeletion(driver, testReport);
      page.MarketoTestDataDelete(data.get("campaignName"), data.get("programName"));

    }catch(Exception e){
      logger.error(e);
    }
  }

  @DataProvider
  public Object[][] getMarketoData() {
    return TestUtil.getData(testCaseName, xls);
  }
}
