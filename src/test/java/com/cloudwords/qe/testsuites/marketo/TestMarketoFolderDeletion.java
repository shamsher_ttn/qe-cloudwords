/**
 * This test script intend to test scenario of delete folder in marketo
 */
package com.cloudwords.qe.testsuites.marketo;

import java.text.ParseException;
import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.testng.log4testng.Logger;

import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.MarketoPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author shamsher
 * @date 14-Nov-2016
 */
@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class TestMarketoFolderDeletion extends BrowserStackTestNG{
  String testCaseName = "DeleteFolderMarketo";
  private static final Logger logger = Logger.getLogger(TestMarketoFolderCreation.class);

  @Test(testName = "5",dataProvider="getMarketoData")
  public void MarketoFolderDeletion(Hashtable<String, String>data) throws ParseException, InterruptedException {
    try{
      testReport = extent.startTest("DeleteFolderMarketo");
      logger.info("Launching test case DeleteFolderMarketo");
      if (!TestUtil.isExecutable(testCaseName, xls) || data
          .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
        testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
        throw new SkipException("Skipping the test as Rnumode is N");
      }
      testReport.log(LogStatus.INFO, "Starting login test");
      testReport.log(LogStatus.INFO, "Opening browser");
      driver.get("https://app-sj09.marketo.com/");

      MarketoPage page = new MarketoPage(driver, testReport);
      page.MarketoLogin();
      page.DeleteProgramFolder(data.get("ProgramName"), data.get("CampaignName"));

    }catch(Exception e){
      logger.error(e);
    }
  }

  @DataProvider
  public Object[][] getMarketoData() {
    return TestUtil.getData(testCaseName, xls);
  }
}
