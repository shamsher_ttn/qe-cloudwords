package com.cloudwords.qe.testsuites.marketo;

import java.text.ParseException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.marketoapi.MarketoDataCreation;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })

public class TestMarketoFolderCreationAPI extends BrowserStackTestNG {

  String testCaseName = "CreateFolderMarketoUsingAPI";

  private static final Logger logger = Logger.getLogger(TestMarketoFolderCreationAPI.class);

  @Test(testName = "2534", dataProvider = "getMarketoData")
  public void MarketoFolderCreation(Hashtable<String, String> data)
      throws ParseException, InterruptedException {
    try {
      testReport = extent.startTest("CreateFolderMarketoUsingAPI");
      logger.info("Launching test case CreateFolderMarketoUsingAPI");
      if (!TestUtil.isExecutable(testCaseName, xls) || data
          .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
        testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
        throw new SkipException("Skipping the test as Rnumode is N");
      }
      testReport.log(LogStatus.INFO, "Starting Marketo test");

      MarketoDataCreation marketoPage = new MarketoDataCreation(driver, testReport);
      marketoPage.MarketoTestDataCreation(data.get("campaignfolder"),
          data.get("progamName"), data.get("emailName"), data.get("programType"),
          data.get("programChannel"));
      Thread.sleep(5000L);
    } catch (Exception e) {
      logger.error(e);
    }
  }

  @DataProvider
  public Object[][] getMarketoData() {
    return TestUtil.getData(testCaseName, xls);
  }
}
