/**
 * This test script intend to test scenario of validating
 * users with given marketo credentials and permission given by customer admin
 */
package com.cloudwords.qe.testsuites.marketo;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.CustSettingsPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.fw.wrapper.WaitTool;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;


@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class ValidatingUsers extends BrowserStackTestNG {

  String testCaseName = "MarketoUserTest";
  private static final Logger logger = Logger.getLogger(ValidatingUsers.class);
  @Test(dataProvider = "getData", testName = "2532")
  public void MarketoUserValidatingTest(Hashtable<String, String>data) {
    testReport = extent.startTest("Marketo user credentials  Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    driver.manage().window().maximize();
    driver.get(Automation.configHashMap.get("BASEURL"));
    LoginUtil.loginWithCredentials(data.get("UserName"), data.get("UserPassword"));
    CustSettingsPage createPage = new CustSettingsPage(driver, testReport);
    createPage.enableMarketoforUsers( data.get("MarketoUserName"), data.get("MarketoPassword"));
   // WaitTool.waitForElement(driver, By.cssSelector("span#notificationText"), 15);
    if(!driver.findElement(By.id("marketoServerError")).getText().contains("Incorrect email or password.")){
      s_assert.assertTrue(true);
    }else{
      s_assert.assertTrue(false , "User credentials are wrong");
    }
    LoginUtil.logout();
    s_assert.assertAll();
  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }
}
