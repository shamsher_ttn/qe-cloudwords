/**
 * This test script intend to test scenario of creating
 * content in marketo in specific campaign folder,
 * program folder with different local assest
 */
package com.cloudwords.qe.testsuites.marketo;

import java.text.ParseException;
import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.testng.log4testng.Logger;

import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.MarketoPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })

public class TestMarketoFolderCreation extends BrowserStackTestNG {
  String testCaseName = "CreateFolderMarketo";
  private static final Logger logger = Logger.getLogger(TestMarketoFolderCreation.class);

  @Test(testName = "5",dataProvider="getMarketoData")
  public void MarketoFolderCreation(Hashtable<String, String>data) throws ParseException, InterruptedException {
    try{
      testReport = extent.startTest("CreateFolderMarketo");
      logger.info("Launching test case CreateFolderMarketo");
      if (!TestUtil.isExecutable(testCaseName, xls) || data
          .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
        testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
        throw new SkipException("Skipping the test as Rnumode is N");
      }
      testReport.log(LogStatus.INFO, "Starting login test");
      testReport.log(LogStatus.INFO, "Opening browser");
      driver.get("https://app-sj09.marketo.com/");

      MarketoPage page = new MarketoPage(driver, testReport);
      page.MarketoLogin();
      Thread.sleep(5000L);
      // page.CreationWorkspace(data.get("WorkspaceName"), data.get("WorkspaceLang"));
      page.CreationCampaignFolder(data.get("WorkspaceName"), data.get("CampaignName"), data.get("CampaignDesc"));
      page.CreationNewProgram(data.get("CampaignName"), data.get("ProgramName"), data.get("ProgramType"), data.get("Channel"));
      page.CreationForm(data.get("ProgramName"), data.get("FormName"));

    }catch(Exception e){
      logger.error(e);
    }
  }

  @DataProvider
  public Object[][] getMarketoData() {
    return TestUtil.getData(testCaseName, xls);
  }
}
