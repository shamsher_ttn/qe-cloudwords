package com.cloudwords.qe.testsuites.eloqua;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.EloquaPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class TestEloquaContentDeletion extends BrowserStackTestNG{

	private String testCaseName ="DeleteEloquaFolder";
	private static final Logger logger = Logger.getLogger(TestEloquaContentDeletion.class);

	@Test(testName="2587",dataProvider="getEloquaData")
	public void EloquaFolderDeletion(Hashtable<String, String>data) {
		try{
			testReport = extent.startTest("DeleteFolderEloqua");
			if (!TestUtil.isExecutable(testCaseName, xls) || data
					.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
				testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
				throw new SkipException("Skipping the test as Rnumode is N");
			}
			driver.manage().window().maximize();
			logger.info("Launching test case DeleteFolderEloqua");
			testReport.log(LogStatus.INFO, "Opening browser");
			driver.get("https://login.eloqua.com/");
			EloquaPage page = new EloquaPage(driver, testReport);
			page.EloquaLogin();
			page.DeleteContent(data.get("EmailName"));
		}catch(Exception e){
			logger.error(e);
		}
	}


	@DataProvider
	public Object[][] getEloquaData() {
		return TestUtil.getData(testCaseName , xls);
	}
}
