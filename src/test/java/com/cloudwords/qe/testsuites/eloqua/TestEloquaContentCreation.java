package com.cloudwords.qe.testsuites.eloqua;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.openqa.selenium.StaleElementReferenceException;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.eloqua.EloquaCreateEmail;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class TestEloquaContentCreation extends BrowserStackTestNG{

	String testCaseName = "EloquaContentCreation";

	private static final Logger logger =Logger.getLogger(TestEloquaContentCreation.class);
	@Test(dataProvider = "getData", testName = "2585")
	public void TestEloquaCreation(Hashtable<String, String>data) {
	    testReport = extent.startTest("Eloqua content creation  Test");

		if (!TestUtil.isExecutable(testCaseName, xls) || data
				.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
			testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
			throw new SkipException("Skipping the test as Rnumode is N");
		}
		try{
			EloquaCreateEmail page = new EloquaCreateEmail();
			page.CreateTestEmail(data.get("EmailName"));
		}catch(NullPointerException e){
			logger.error(e);
		}catch (StaleElementReferenceException e) {
			logger.error(e);
		}catch(Exception e){
			logger.error(e);
		}

	}

	@DataProvider
	public Object[][] getData() {
		return TestUtil.getData(testCaseName, xls);
	}
}
