/**
 * This test script intend to test scenario of validating
 * users with given eloqua credentials and permission given by customer admin
 */

package com.cloudwords.qe.testsuites.eloqua;

import java.util.Hashtable;

import org.openqa.selenium.By;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.testng.log4testng.Logger;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.CustSettingsPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class ValidatingUsers extends BrowserStackTestNG {

  String testCaseName = "EloquaUserTest";
  private static final Logger logger =Logger.getLogger(ValidatingUsers.class);

  @Test(dataProvider = "getData", testName = "2583")
  public void EloquaUserValidatingTest(Hashtable<String, String>data) {
    testReport = extent.startTest("Eloqua user credentials  Test");

    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    driver.manage().window().maximize();
    driver.get(Automation.configHashMap.get("BASEURL"));
    LoginUtil.loginWithCredentials(data.get("UserName"), data.get("UserPassword"));
    CustSettingsPage createPage = new CustSettingsPage(driver, testReport);
    createPage.enableEloquaforUsers(data.get("CompanyName"), data.get("EloquaUserName"), data.get("EloquaPassword"));
    if(!driver.findElement(By.xpath("//*[@id='eloquaCredentials']/div/div[1]/div[1]/span[2]")).getText().contains("The supplied login credentials are invalid")){
      s_assert.assertTrue(true);
    }else{
      s_assert.assertTrue(false);
    }
    LoginUtil.logout();
    s_assert.assertAll();
  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }
}
