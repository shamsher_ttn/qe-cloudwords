/**
 * This test script intend to test scenario of creating translation
 * project in which file uploaded through eloqua interface integrated with our system
 */

package com.cloudwords.qe.testsuites.eloqua;

import java.text.ParseException;
import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.testng.log4testng.Logger;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.ProjectCreationPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class EloquaProjectCreationTest extends BrowserStackTestNG {

  String testCaseName = "MultipleCombiEloquaPrjTest";
  private static final Logger logger =Logger.getLogger(EloquaProjectCreationTest.class);
  @Test(dataProvider = "getData", testName = "2589")
  public void MarketoPrjCreationTest(Hashtable<String, String> data) {
    testReport = extent.startTest("Eloqua Project Creation");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    try {
      driver.manage().window().maximize();
      driver.get(Automation.configHashMap.get("BASEURL"));
      ProjectCreationPage createPrj = new ProjectCreationPage(driver, testReport);
      createPrj.EloquaProjectCreation(data.get("Username"), data.get("Password"),
          data.get("ProjectDeliveryType"), data.get("ProjectName"), data.get("Intendeduse"),
          data.get("ProjectPriority"), data.get("SrcMaterials"), data.get("SrcLang"),
          data.get("TargetLang"), data.get("TemplateBooleanStatus"), data.get("TemplateName"),
          data.get("Date"), data.get("FileName"),data.get("VendorName"));
    } catch (ParseException | InterruptedException e) {
      e.printStackTrace();
    }
  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }

}
