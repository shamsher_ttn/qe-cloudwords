/**
 *This test scripts intend to test scenario of creating project of indesign files
 */
package com.cloudwords.qe.testsuites.indesign;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.indesign.ProjectCreationPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author gaurav
 * @date 29-Sep-2016
 */
@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class InDesignCreateProjectTest extends BrowserStackTestNG {

  String testCaseName = "inDesignCreateProjectTest";
  private static final Logger logger = Logger.getLogger(InDesignCreateProjectTest.class);

  @Test(dataProvider = "getinDesignCreateProjectData", testName = "7", priority = 2)
  public void landingPageLinksVerification(Hashtable<String, String> data)
      throws InterruptedException {

    testReport = extent.startTest("inDesign Create Project Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    testReport.log(LogStatus.INFO, "Starting  inDesign create project test");
    driver.get(Automation.configHashMap.get("BASEURL"));

    ProjectCreationPage pcrPage = new ProjectCreationPage(driver, testReport);

    String val = pcrPage.SimpleMultipleFilesProjectCreationForInDesign(data.get("PmUser"),
        data.get("PmPwd"), data.get("ProjectName"), data.get("IndexNoIntendedUse"),
        data.get("IndexNoPriority"), data.get("DirPath"), data.get("SrcLangIndex"),
        data.get("TargetLangIndex"), data.get("translationDate"), data.get("reviewDate"),data.get("Reviewer_Name"));

    System.out.println("value returned by method is " + val);

    System.out.println("peoject status is"+ driver.findElement(By.id("project.status")).getText());

    pcrPage.waitUntilElementGetsValue("project.status", "Waiting for Internal Translator",280,20);

    System.out.println("peoject status is"+ driver.findElement(By.id("project.status")).getText());

    if (driver.findElement(By.id("project.status")).getText()
        .equalsIgnoreCase("Waiting for Internal Translator")) {
      LoginUtil.logout();
      System.out.println("wait for the page to be change to Waiting for Internal Translator");
      /* UploadTranslationPage utlPage = new UploadTranslationPage(driver, testReport);

      utlPage.selectProjectTaskAndUploadTranslationFile(val);*/
    } else {
      System.out.println("project status does not changed");
    }

  }

  @DataProvider
  public Object[][] getinDesignCreateProjectData() {
    return TestUtil.getData(testCaseName, xls);
  }

}