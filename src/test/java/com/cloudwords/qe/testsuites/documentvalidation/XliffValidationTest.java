/**
 * This test case is intended to test scenario where we verify & validate  xliff file against standards
 */

package com.cloudwords.qe.testsuites.documentvalidation;

import java.io.File;
import java.util.Hashtable;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import com.cloudwords.xliffchecker.XLIFFChecker;

import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class XliffValidationTest extends BrowserStackTestNG {
  private static final Logger logger = Logger.getLogger(XliffValidationTest.class);

  String testCaseName = "XliffValidationTest";

  @Test(dataProvider = "getData", testName = "7")
  public void xliffValidationTest(Hashtable<String, String> data) {
    testReport = extent.startTest("Xliff Validation Test");
    try {
      File dir = new File(data.get("DirPath"));
      String celdata = data.get("extension");
      String[] extensions;
      extensions=celdata.split(",");
      System.out.println("Getting all .xlf and .xliff files in " + dir.getCanonicalPath()
      + " including those in subdirectories");
      List<File> files = (List<File>) FileUtils.listFiles(dir, extensions, true);
      System.out.println("Total no. of files found in given directory path : " + files.size());
      for (File file : files) {
        System.out.println("file: " + file.getCanonicalPath());
        XLIFFChecker.XliffChecker(file.getCanonicalPath());
        for (int i = 0; i < XLIFFChecker.resultList.size(); i++) {
          XLIFFChecker.resultList.get(i);
          System.out.println(XLIFFChecker.resultList.get(i));
        }
        if (XLIFFChecker.resultList
            .contains("Selected file is a valid XLIFF 1.2 Transitional document.")
            || XLIFFChecker.resultList
            .contains("Selected file is a valid XLIFF 1.2 Strict document")) {
          s_assert.assertTrue(true, file.getCanonicalPath() + " is valid xliff file");
        } else {
          s_assert.assertTrue(false, file.getCanonicalPath() + " is not valid xliff file");
        }
        XLIFFChecker.resultList.clear();
        System.out.println("**************************************************");
      }

    } catch (Exception e) {
    }
    s_assert.assertAll();

  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }
}
