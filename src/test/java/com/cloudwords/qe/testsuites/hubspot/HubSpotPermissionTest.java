package com.cloudwords.qe.testsuites.hubspot;

import java.sql.SQLException;
import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.IntegrationQueries;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.CustSettingsPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class HubSpotPermissionTest extends BrowserStackTestNG {

  private static final Logger logger = Logger.getLogger(HubSpotPermissionTest.class);
      String testCaseName = "HubSpotEnablePermissionTest";

  @Test(dataProvider = "getData", testName = "7")
  public void HubSpotPermission(Hashtable<String, String> data) throws SQLException {

    try{
      testReport = extent.startTest("HubSpot persmission Test");
      if (!TestUtil.isExecutable(testCaseName, xls) || data
          .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
        testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
        throw new SkipException("Skipping the test as Rnumode is N");
      }
      driver.manage().window().maximize();
      driver.get(Automation.configHashMap.get("BASEURL"));
      LoginUtil.loginWithCredentials(data.get("CustomerAdminUser"), data.get("CustomerAdminPwd"));
      CustSettingsPage createPage = new CustSettingsPage(driver, testReport);
      createPage.hubspotoUserPermission(data.get("UserEmail"), data.get("Permissions"));
      String[] userList = data.get("UserEmail").split(",");
      Thread.sleep(10000L);
      for(int i = 0 ; i<userList.length;i++){
        if (IntegrationQueries.verifyMarketoAccess(userList[i])) {
          s_assert.assertTrue(true, "User have permission to access HubSpot");
        }else{
          s_assert.assertTrue(false);
        }
      }
      LoginUtil.logout();
    }catch(Exception e){
      System.out.println(e);
    }
    s_assert.assertAll();
  }




  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }


}
