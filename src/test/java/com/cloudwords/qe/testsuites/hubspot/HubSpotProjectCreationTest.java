package com.cloudwords.qe.testsuites.hubspot;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.ProjectCreationPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;



@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class HubSpotProjectCreationTest extends BrowserStackTestNG {

  String testCaseName = "MultipleCombiHubSpotTest";
  private static final Logger logger = Logger.getLogger(HubSpotProjectCreationTest.class);

  @Test(dataProvider = "getData", testName = "7")
  public void HubSpotPrjCreationTest(Hashtable<String, String> data) {
    testReport = extent.startTest("TestTranslation");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    try {
      driver.manage().window().maximize();
      driver.get(Automation.configHashMap.get("BASEURL"));
      ProjectCreationPage createPrj = new ProjectCreationPage(driver, testReport);
      createPrj.HubSpotProjectCreation(data.get("Username"), data.get("Password"), data.get("ProjectDeliveryType"), data.get("ProjectName"),
          data.get("Intendeduse"), data.get("ProjectPriority"), data.get("SrcMaterials"), data.get("SrcLang"), data.get("TargetLang"), data.get("TemplateBooleanStatus"),
          data.get("TemplateName"), data.get("Date"), data.get("FileName"));
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @DataProvider
  public Object[][] getData() {
    return TestUtil.getData(testCaseName, xls);
  }

}
