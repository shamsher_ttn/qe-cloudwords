package com.cloudwords.qe.testsuites.hubspot;

import java.util.Hashtable;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.CustSettingsPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.fw.wrapper.WaitTool;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class ValidatingUsers extends BrowserStackTestNG {

	String testCaseName = "HubSpotUserTest";
	private static final Logger logger = Logger.getLogger(ValidatingUsers.class);

	@Test(dataProvider = "getData", testName = "7")
	public void HubSpotUserValidatingTest(Hashtable<String, String> data) {
		testReport = extent.startTest("HubSpot user credentials  Test");
		if (!TestUtil.isExecutable(testCaseName, xls)
				|| data.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
			testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
			throw new SkipException("Skipping the test as Rnumode is N");
		}
		driver.manage().window().maximize();
		driver.get(Automation.configHashMap.get("BASEURL"));
		LoginUtil.loginWithCredentials(data.get("UserName"), data.get("UserPassword"));
		CustSettingsPage createPage = new CustSettingsPage(driver, testReport);
		createPage.enableHubSpotforUsers(data.get("HubSpotID"), data.get("HubSpotUserName"), data.get("HubSpotPwd"));
		WaitTool.waitForElement(driver, By.cssSelector("span#notificationText"), 10);
		if (!driver.findElement(By.xpath("//*[@id='invalidHubSpotCredentialsMessage']")).getText()
				.contains("Failed to get authorization from HubSpot")) {
			s_assert.assertTrue(true);
		} else {
			s_assert.assertTrue(false);
		}
		LoginUtil.logout();
		s_assert.assertAll();
	}

	@DataProvider
	public Object[][] getData() {
		return TestUtil.getData(testCaseName, xls);
	}
}
