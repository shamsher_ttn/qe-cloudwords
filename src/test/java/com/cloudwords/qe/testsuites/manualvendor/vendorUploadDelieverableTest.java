/**
 * This test script is intend to test scenario when vendor upload deliverable into the system
 */

package com.cloudwords.qe.testsuites.manualvendor;

import java.util.Hashtable;

import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.testng.log4testng.Logger;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.LoginPage;
import com.cloudwords.qe.fw.pageobjects.ProjectDescriptionPage;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.fw.wrapper.General;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })

public class vendorUploadDelieverableTest extends BrowserStackTestNG{

  String testCaseName = "SelectBidTest";
  private static final Logger logger = Logger.getLogger(vendorUploadDelieverableTest.class);

  @Test(dataProvider = "vendorUploadDelievrables")
  public void selectVendorForBidding(Hashtable<String, String> data) throws Exception {
    testReport = extent.startTest("vendorUploadDelievrables Test");
    if (!TestUtil.isExecutable(testCaseName, xls) || data
        .get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
      testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
      throw new SkipException("Skipping the test as Rnumode is N");
    }
    testReport.log(LogStatus.INFO, "Starting login test");
    testReport.log(LogStatus.INFO, "Opening browser");
    General.OpenURL();
    new LoginPage(driver, testReport);
    testReport.log(LogStatus.INFO, "Logging in");
    LoginUtil.doLogin(data.get("Username"), data.get("Password"));
    ProjectDescriptionPage page1 = new ProjectDescriptionPage(driver, testReport);
    testReport.log(LogStatus.INFO,"starting vendor upload delievrable test");
    page1.vendorUploadLanguageDelieverable(data.get("ProjectName"));
    if (page1 instanceof ProjectDescriptionPage) {
    }
  }

  @DataProvider
  public Object[][] vendorUploadDelievrables() {
    return TestUtil.getData(testCaseName, xls);
  }
}