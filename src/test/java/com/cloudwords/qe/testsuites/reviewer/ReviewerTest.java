package com.cloudwords.qe.testsuites.reviewer;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.io.FilenameUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.SkipException;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.ReviewerPage;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class ReviewerTest extends BrowserStackTestNG {
	String projectName, ProjectId, language, fileName, docName, emailSubject;

	String testCaseName = "ReviewerTest";

	@Test(dataProvider = "getData", testName = "2619")
	public void reviewTranslation(Hashtable<String, String> data) throws Exception {
		testReport = extent.startTest("TestReviwer");
		if (!TestUtil.isExecutable(testCaseName, xls)
				|| data.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
			testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
			throw new SkipException("Skipping the test as Rnumode is N");
		}

		driver.get(Automation.configHashMap.get("BASEURL"));
		Object page = LoginUtil.doLogin(data.get("Username"), data.get("Password"));
		CommonUtil.Wait(5);

		ReviewerPage reviewPage = new ReviewerPage(driver, testReport);

		int numberOfProjectsToBeReviewed = reviewPage.reviewProjects();
		int columnsCount = reviewPage.getColumnCount();
		WebElement selectProject;

		if (numberOfProjectsToBeReviewed != 0 && columnsCount == 6) {

			for (int i = 1; i <= numberOfProjectsToBeReviewed; i++) {
				selectProject = driver
						.findElement(By.cssSelector(".full-width>tbody>tr:nth-child(1)>td:nth-child(2)>a"));
				projectName = selectProject.getText();
				selectProject.click();
				Thread.sleep(4000);
		        if(driver.findElements(By.xpath("//*[@id='highlight-tip-container']/div/span[2]")).size()!=0){
		          driver.findElement(By.xpath("//*[@id='highlight-tip-container']/div/span[2]")).click();
		        }
				CommonUtil.Wait(5);

				List<WebElement> targetLanguages = driver
						.findElements(By.xpath("//*[starts-with(@id, 'tab_language_')]"));
				ProjectId = reviewPage.getProjectDetails();
				CommonUtil.Wait(3);
				int countTargetlang = targetLanguages.size();

				if (countTargetlang != 0) {
					for (int j = 0; j < countTargetlang; j++) {
						Map<String, Object> getValue = new HashMap<String, Object>();
						getValue = reviewPage.isReviewButtonPresent(j);
						Set<String> getLanguage = getValue.keySet();
						for (String key : getLanguage) {
							language = key;
						}

						boolean reviewButton = (boolean) getValue.get(language);
						WebElement reviewSourceFilesTable = driver.findElement(
								By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div[1]/div/div/div[2]/table[3]"));
						int rowCount = WebTable.getRowCount(reviewSourceFilesTable);

						for (int k = 1; k <= rowCount; k++) {
							if (reviewButton) {
								fileName = reviewPage.getFileNameForTranslation();
								String fileExt = FilenameUtils.getExtension(fileName);
								reviewPage.reviewTranslation(j, data.get("feedBackText"), data.get("translationText"),
										data.get("feedbackLineNum"), data.get("ediTranslationLineNum"), fileExt,
										fileName, k, reviewPage);
								CommonUtil.Wait(3);
							} else {
								System.out.println(
										"Either Review button does not exist or File has already been reviewed");
							}
						}
					}

					String projectStatus = "#project-status-container>a>h3";
					reviewPage.waitUntilElementGetsValue(projectStatus, "Project Closed", 240, 25);
					reviewPage.switchToProjectListing();
					CommonUtil.Wait(3);
				}
			}
		} else {
			System.out.println("There are no Projects pending for Review");
		}
	}

	@AfterTest
	public void beforeEveryTest() {
		CommonUtil.Wait(5);
		LoginUtil.logout();
	}

	@DataProvider
	public Object[][] getData() {
		return TestUtil.getData(testCaseName, xls);
	}

}
