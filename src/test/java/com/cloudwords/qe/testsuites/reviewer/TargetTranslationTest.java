package com.cloudwords.qe.testsuites.reviewer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.SkipException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import org.testng.listeners.TestMethodListener;
import org.testng.listeners.TestNameInterceptor;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.pageobjects.ReviewTranslationPage;
import com.cloudwords.qe.fw.pageobjects.ReviewerPage;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.TestUtil;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.cloudwords.qe.testsuites.base.BrowserStackTestNG;
import com.relevantcodes.extentreports.LogStatus;

@Listeners(value = { TestMethodListener.class, TestNameInterceptor.class })
public class TargetTranslationTest extends BrowserStackTestNG {
	String testCaseName = "TestTargetTranslation";
	int cellNumXML, cellNumXL, cell1, cell2, celldiff;
	String projectName, ProjectId, language, fileName, docName;
	CellStyle style = null;
	FileOutputStream fileOutput;
	XSSFWorkbook workbook;
	XSSFSheet spreadSheet;
	String xmlFilePath;

	ReviewTranslationPage translate = new ReviewTranslationPage(driver, testReport);

	@Test(priority = 1, dataProvider = "getData", testName = "2623")
	public void webTableToExcel(Hashtable<String, String> data) throws Exception {
		testReport = extent.startTest("TestTranslation");

		if (!TestUtil.isExecutable(testCaseName, xls)
				|| data.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
			testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
			throw new SkipException("Skipping the test as Rnumode is N");
		}

		ReviewerPage reviewPage = new ReviewerPage(driver, testReport);
		driver.get(Automation.configHashMap.get("BASEURL"));
		Object page = LoginUtil.doLogin(data.get("Username"), data.get("Password"));
		CommonUtil.Wait(5);

		int numberOfProjectsToBeReviewed = reviewPage.reviewProjects();
		int columnsCount = reviewPage.getColumnCount();
		WebElement selectProject;

		if (numberOfProjectsToBeReviewed != 0 && columnsCount == 6) {
			for (int i = 0; i < numberOfProjectsToBeReviewed; i++) {
				selectProject = driver.findElement(
						By.cssSelector(".full-width>tbody>tr:nth-child(" + ((i * 2) + 1) + ")>td:nth-child(2)>a"));
				projectName = selectProject.getText();
				selectProject.click();
				Thread.sleep(4000);
		        if(driver.findElements(By.xpath("//*[@id='highlight-tip-container']/div/span[2]")).size()!=0){
		          driver.findElement(By.xpath("//*[@id='highlight-tip-container']/div/span[2]")).click();
		        }
				Thread.sleep(4000);

				List<WebElement> targetLanguages = driver
						.findElements(By.xpath("//*[starts-with(@id, 'tab_language_')]"));
				ProjectId = reviewPage.getProjectDetails();
				Thread.sleep(3000);
				int countTargetlang = targetLanguages.size();

				if (countTargetlang != 0) {
					for (int j = 0; j < countTargetlang; j++) {
						Map<String, Object> getValue = new HashMap<String, Object>();
						getValue = reviewPage.isReviewButtonPresent(j);
						Set<String> getLanguage = getValue.keySet();
						for (String key : getLanguage) {
							language = key;
						}

						boolean reviewButton = (boolean) getValue.get(language);
						WebElement reviewSourceFilesTable = driver.findElement(
								By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div[1]/div/div/div[2]/table[3]"));
						int rowCount = WebTable.getRowCount(reviewSourceFilesTable);
						for (int k = 1; k <= rowCount; k++) {
							if (reviewButton) {
								fileName = reviewPage.getFileNameForTranslation(k, testCaseName);
								String fileExtension = FilenameUtils.getExtension(fileName);
								fileName = FilenameUtils.removeExtension(fileName);

								if (fileExtension.equals("docx") || (fileExtension.equals("xlf")
										&& !fileName.equals("assets-metadata") && !fileName.equals("tags-metadata"))) {
									try {
										spreadSheet = createSpreadsheet(data,
												 PublicVariables.DOWNLOAFFILEPATH + PublicVariables.SEP + projectName + "_"
									                        + ProjectId + PublicVariables.SEP + language + PublicVariables.SEP
									                        + fileName + PublicVariables.SEP + "WebTableToSpreadsheet.xlsx");
									} catch (Exception e) {
										System.out.println(fileName + " does not exist " + e);
									}
									reviewPage.translateWithoutFeedback(j, k);
									configureXlColumns(data);

									FileOutputStream fileOutput = new FileOutputStream(
											PublicVariables.DOWNLOAFFILEPATH + PublicVariables.SEP + projectName + "_"
							                        + ProjectId + PublicVariables.SEP + language + PublicVariables.SEP
							                        + fileName + PublicVariables.SEP + "WebTableToSpreadsheet.xlsx");
									workbook.write(fileOutput);
									fileOutput.flush();
									fileOutput.close();

									compareDataInEachRow(data);
									CommonUtil.Wait(5);

									driver.navigate().back();
									CommonUtil.Wait(3);

								} else {
									System.out.println("This is Jpeg File, No Xliffs are generated");
								}
							} else {
								System.out.println("This target language is either Reviewed or In Revision");
							}
						}
					}
					reviewPage.switchToProjectListing();
				}
			}

		} else {
			System.out.println("There are no Projects pending for Review");
		}
		CommonUtil.Wait(5);
		LoginUtil.logout();
		System.out.println("Logout after TestTarget Translation");
	}

	public void configureXlColumns(Hashtable<String, String> data)
			throws ParserConfigurationException, SAXException, IOException, InterruptedException {

		String xmlFilePath = PublicVariables.DOWNLOAFFILEPATH+ PublicVariables.SEP + projectName+"_"
		        + ProjectId + PublicVariables.SEP + language + PublicVariables.SEP + fileName +
		        PublicVariables.SEP + fileName+"_TranslatedFile.xlf";

		String xmlTagName = data.get("xmlNode");
		NodeList nodeList = translate.readXMLData(xmlFilePath, xmlTagName);
		Thread.sleep(5000);

		// Scroll to Bottom of Page
		WebElement scrollToBottom = driver.findElement(By.cssSelector("#tablewrapper>ul>li:nth-child(1)"));
		translate.scrollToWebElement(driver, scrollToBottom);

		WebElement defaultActivePage = driver.findElement(By.cssSelector(".pagination>li.active"));
		int pageNumber = Integer.parseInt(defaultActivePage.getText());
		Assert.assertEquals(pageNumber, 1);

		List<WebElement> pages = driver.findElements(By.cssSelector(".pagination>li"));
		int pagecount = 1;
		if (pages.size() > 1) {
			pagecount = pages.size() - 2;
		}
		for (int i = 1; i <= pagecount; i++) {
			translate.clickPageAndScrollToBottom(driver, scrollToBottom, pagecount, i);
			List<WebElement> rows = driver.findElements(By.cssSelector(".line-by-line>tbody"));
			int iRowsCount = rows.size();
			for (int j = 1; j <= iRowsCount; j++) {
				WebElement getRow = null;
				int k = (i - 1) * 20 + j;

				getRow = driver.findElement(
						By.cssSelector("tbody:nth-child(" + j + ")>tr:nth-child(1)>td:nth-child(4)>div:nth-child(1)"));
				cellNumXML = 5;
				cellNumXL = 4;
				cell1 = 4;
				cell2 = 5;
				celldiff = 4;
				XSSFRow excelRow = translate.writeXMLToExcel(this.testCaseName, spreadSheet, nodeList, j, k,
						cellNumXML);
				translate.writeWebDataToExcel(driver, getRow, excelRow, cellNumXL);
			}
		}
	}

	public XSSFSheet createSpreadsheet(Hashtable<String, String> data, String xlFile)
			throws IOException, InvalidFormatException {
		String cell1Header = Automation.appconfigHashMap.get("targetCel1Header");
		String cell2Header = Automation.appconfigHashMap.get("targetCel2Header");
		int createCellNum1 = Integer.parseInt(Automation.appconfigHashMap.get("createCellNum4"));
		int createCellNum2 = Integer.parseInt(Automation.appconfigHashMap.get("createCellNum5"));
		int createRowNum = Integer.parseInt(Automation.appconfigHashMap.get("createRowNum"));

		File file = new File(xlFile);
		FileInputStream fileInput = new FileInputStream(file);
		OPCPackage opc = OPCPackage.open(fileInput);
		workbook = (XSSFWorkbook) WorkbookFactory.create(opc);

		String spreadsheetName = Automation.appconfigHashMap.get("spreadsheetName");
		XSSFSheet spreadSheet = workbook.getSheet(spreadsheetName);

		style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);

		if (spreadSheet == null) {
			spreadSheet = workbook.createSheet("DataStorage");
		}

		XSSFRow row = spreadSheet.getRow(createRowNum);
		if (row == null) {
			row = spreadSheet.createRow(createRowNum);
			row.createCell(createCellNum1).setCellValue(cell1Header);
			row.getCell(createCellNum1).setCellStyle(style);
			row.createCell(createCellNum2).setCellValue(cell2Header);
			row.getCell(createCellNum2).setCellStyle(style);

		} else {
			row.createCell(createCellNum1).setCellValue(cell1Header);
			row.getCell(createCellNum1).setCellStyle(style);
			row.createCell(createCellNum2).setCellValue(cell2Header);
			row.getCell(createCellNum2).setCellStyle(style);
		}

		return spreadSheet;
	}

	// Compare value between two cells in excel spreadsheet
	public void compareDataInEachRow(Hashtable<String, String> data) throws IOException {
		if (!TestUtil.isExecutable(testCaseName, xls)
				|| data.get(PublicVariables.TEST_CASES_RUNMODE_COLUMN).equals(PublicVariables.RUNMODE_NOVALUE)) {
			testReport.log(LogStatus.SKIP, "Skipping the test as Rnumode is N");
			throw new SkipException("Skipping the test as Rnumode is N");
		}

		String filePath =PublicVariables.DOWNLOAFFILEPATH + PublicVariables.SEP + projectName+"_"
		        + ProjectId + PublicVariables.SEP + language + PublicVariables.SEP + fileName+PublicVariables.SEP
		        + "WebTableToSpreadsheet.xlsx";

		File file = new File(filePath);
		String sheetName = Automation.appconfigHashMap.get("spreadsheetName");

		FileInputStream fileInput = new FileInputStream(file);
		XSSFSheet spreadSheet = null;
		XSSFWorkbook workbook = null;
		FileOutputStream out = null;
		workbook = new XSSFWorkbook(fileInput);
		spreadSheet = workbook.getSheet(sheetName);
		style = workbook.createCellStyle();
		style.setFillForegroundColor(IndexedColors.RED.getIndex());
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);

		// Compare Source Data from Web and XML
		translate.compareWebAndXMLData(spreadSheet, data.get("translatedData"), cell1, cell2, celldiff, style);
		out = new FileOutputStream(file);
		workbook.write(out);

		// Assert while comparing data
		// translate.assertTranslationComparison(spreadSheet,
		// data.get("translatedData"), cell1, cell2, s_assert);
		workbook.close();
		out.close();
	}

	@DataProvider
	public Object[][] getData() {
		return TestUtil.getData(testCaseName, xls);
	}
}
