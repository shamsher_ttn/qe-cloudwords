/**
 *
 */
package com.cloudwords.qe.fw.base;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.asserts.SoftAssert;

import com.cloudwords.qe.fw.libraries.FileHandler;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.utillib.ExtentManager;
import com.cloudwords.qe.fw.utillib.FileUtil;
import com.cloudwords.qe.fw.utillib.FwUtil;
import com.cloudwords.qe.fw.utillib.XLReader;
import com.cloudwords.qe.fw.wrapper.WebDriverFactory;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.ExtentTest;

import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

/**
 * @author Shamsher Singh
 * @date 03-Aug-2016
 */
public class Automation {

  private static final String TASKLIST = "tasklist";
  private static String KILL = "taskkill /F /IM ";
  public static HashMap<String, String> configHashMap = new HashMap<String, String>();
  public static HashMap<String, String> appconfigHashMap = new HashMap<String, String>();
  public static final Map<String, String> STOREHASHMAP = new HashMap<String, String>();
  public static final Map<String, String>TESTLINKMAP=new HashMap<String,String>();
  public static WebDriver driver;
  public static WebDriverFactory webDriverObj = new WebDriverFactory();
  public static browserTypeEnum browserType = null;
  public static String browser = null;
  public static ExtentTest testReport;
  public ExtentReports extent = ExtentManager.getInstance();
  protected static XLReader xls = new XLReader(System.getProperty("user.dir") + PublicVariables.SEP
      + "src/test/resources" + PublicVariables.SEP + "xlfiles" + PublicVariables.SEP
      + PublicVariables.TEST_CASE_SHEET);
  private static final Logger logger = Logger.getLogger(Automation.class);
  public static SoftAssert s_assert = new SoftAssert();
  public static String WebObjectMsg = "";
  public static boolean EXISTLOCATORVALUE;
  public static boolean setImplicitWait = false;
  public static boolean waitForObjectToExist = true;
  public static enum browserTypeEnum {
    internetexplorer, firefox, chrome, grid
  };
  public static String downloadFilepath = PublicVariables.DOWNLOAFFILEPATH;
  public static long maximumFindObjectTime;
  public static long defaultFindObjectTime = 120000L;
  public static long IntervalTimeOut = 2000L;
  /**
   * @throws Exception
   */
  public static void setUp() throws Exception {

    if (new File(downloadFilepath).exists()) {
      FileUtil.deleteDirectory(new File(downloadFilepath));
      FileHandler.createFolder(downloadFilepath);
    }
    FileHandler.createFolder(downloadFilepath);
    try {

      browser = configHashMap.get("BROWSERTYPE").toLowerCase();
      browserType = browserTypeEnum.valueOf(browser);

      switch (browserType) {
      case internetexplorer:
        driver = getIEDriverInstance();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        break;

      case firefox:
        driver = getFFDriverInstance();
        driver.manage().window().maximize();
        break;

      case chrome:
        driver = getChromeDriverInstance();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        break;

      case grid:
        driver = getGridInstance();
        driver.manage().window().maximize();
        driver.manage().deleteAllCookies();
        break;
      }

    /*  *//** Implicit Wait **//*
      driver.manage().timeouts().implicitlyWait(
          Long.parseLong(configHashMap.get("TIMEOUT").toString()), TimeUnit.SECONDS);*/
    } catch (NullPointerException npe) {
      logger.error("Null Values Found in SetUp Function , please check StackTrace: <br/>"
          + FwUtil.getStackTrace(npe));
    } catch (Exception e) {
      logger.error("Error from Setup " + FwUtil.getStackTrace(e));
    }
  }

  /** Returns an IE Driver's Instance **/
  public static WebDriver getIEDriverInstance() throws InterruptedException, Exception {
    return webDriverObj.createDriver("msie");
  }

  /** Returns a FireFox Driver's Instance **/
  public static WebDriver getFFDriverInstance() throws Exception {
    return new FirefoxDriver();
  }

  /** Returns a Chrome Driver's Instance **/
  public static WebDriver getChromeDriverInstance() throws Exception {
    return webDriverObj.createDriver("chrome");
  }

  /** Returns a Grid Instance **/
  public static WebDriver getGridInstance() throws Exception {
    return webDriverObj.createDriver("grid");
  }

  /**
   * @param serviceName
   * @return
   * @throws Exception
   */
  public static boolean isProcessRunning(String serviceName) throws Exception {

    Process p = Runtime.getRuntime().exec(TASKLIST);
    BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));
    String line;
    while ((line = reader.readLine()) != null) {
      System.out.println(line);
      if (line.contains(serviceName)) {
        return true;
      }
    }
    return false;
  }

  /**
   * @param serviceName
   * @throws Exception
   */
  public static void killProcess(String serviceName) throws Exception {
    KILL = System.getenv("SystemRoot") + KILL;
    Runtime.getRuntime().exec(KILL + serviceName);
  }

  /**
   * @param filePath
   */
  public static void LoadData(String filePath) {
    Properties prop = new Properties();
    FileInputStream proFileName;
    File sTempFile;
    try {

      sTempFile = new File(filePath);
      proFileName = new FileInputStream(sTempFile.getAbsolutePath());
      prop.load(proFileName);
      Set<String> propertyNames = prop.stringPropertyNames();
      for (String Property : propertyNames) {
        configHashMap.put(Property, prop.getProperty(Property));
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  /**
   * @param testName
   * @param failureMessage
   * @throws TestLinkAPIException
   */
  /*public void reportFailure(String testcaseID, String failureMessage) throws TestLinkAPIException {
    String screenshotPath = FwUtil.captureSnapshot(driver, testcaseID);
    testReport.log(LogStatus.FAIL, failureMessage);
    testReport.addScreenCapture(screenshotPath);

    // String bugId =JiraHandler.createIssue(Automation.configHashMap.get("JIRA_PROJECTNAME"), "");
    TestLinkHandler.reportTestCaseResult(testcaseID, failureMessage, TestLinkAPIResults.TEST_FAILED,
        null);
    Assert.fail(failureMessage);
  }*/

  public static void LoadappData(String filePath) {
    Properties prop = new Properties();
    FileInputStream proFileName;
    File sTempFile;
    try {

      sTempFile = new File(filePath);
      proFileName = new FileInputStream(sTempFile.getAbsolutePath());
      prop.load(proFileName);
      Set<String> propertyNames = prop.stringPropertyNames();
      for (String Property : propertyNames) {
        appconfigHashMap.put(Property, prop.getProperty(Property));
        // System.out.println(Property +" : " + prop.getProperty(Property));
      }
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

}
