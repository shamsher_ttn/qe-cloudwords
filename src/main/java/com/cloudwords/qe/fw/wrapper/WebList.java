/**
 *
 */
package com.cloudwords.qe.fw.wrapper;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.NumberUtil;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author Shamsher Singh
 * @date 13-Aug-2016
 */
public class WebList extends Automation {
    private static final Logger logger = Logger.getLogger(WebList.class);


  public void VerifyItemsExist(WebElement query, Object items, String existence) {

    boolean itemsExist = true;
    if (!CommonUtil.isBoolean(existence)) {
      Automation.testReport.log(LogStatus.WARNING,
          "Since a valid value for existence is not given, assuming the default value of 'TRUE'");
      itemsExist = true;
    } else {
      itemsExist = Boolean.parseBoolean(existence.trim());
    }
    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL, " Failed to retrieve the locator value.");
    }
    Select listBox = new Select(query);
    List listItems = listBox.getOptions();

    String[] actualItems = new String[listItems.size()];
    for (int i = 0; i < listItems.size(); i++) {
      actualItems[i] = ((WebElement) listItems.get(i)).getAttribute("text");
    }

    boolean stat = false;

    ArrayList expectedItemsList = (ArrayList) items;
    ArrayList actualItemsList = (ArrayList) CommonUtil.convertArrayToList(actualItems);

    String msg = "";
    String status = "";
    if (itemsExist) {
      msg = " Expected :Item should exist :";
      stat = CommonUtil.isSubList(actualItemsList, expectedItemsList);
      if (stat) {
        status = "Passed";
        // result = 0;
      } else {
        status = "Failed";
        // result = 2;
      }
    } else {
      msg = " Expected :Item should not exist :";
      stat = CommonUtil.elementNotExistInMainList(actualItemsList, expectedItemsList);
      if (stat) {
        status = "Passed";
        // result = 0;
      } else {
        status = "Failed";
        // result = 2;
      }
    }
    msg = msg + CommonUtil.UtilMsg;
    Automation.testReport.log(LogStatus.FAIL, msg);

  }

  public static void SelectItemByIndex(WebElement query, String intIndex) {

    int idx = 1;

    if (intIndex.equals("")) {
      Automation.testReport.log(LogStatus.INFO,
          "Since a valid value for index is not given, assuming the default value as '1'");
      idx = 1;
    } else {
      if (!NumberUtil.isPostiveInteger(intIndex)) {
        Automation.testReport.log(LogStatus.FAIL, " The intIndex value '" + intIndex
            + "' is not a valid positive integer, please verify.");
      }

      idx = Integer.parseInt(intIndex.trim()) - 1;
    }

    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL, "Failed to retrieve the locator value.");
    }
    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL, "The list does not exist , please verify");

    }

    Select listBox = new Select(query);

    listBox.selectByIndex(idx);

    List<WebElement> items = listBox.getAllSelectedOptions();

    for (WebElement webElement : items) {
      System.out.println(webElement.getText());
      Automation.testReport.log(LogStatus.PASS,
          "The item " + webElement.getText() + " is selected successfully");
    }
    try {
      // Thread.sleep(Automation.IntervalTimeOut);
    } catch (Exception localException) {
    }

    System.out.println(intIndex);
    Automation.testReport.log(LogStatus.FAIL,
        " The item indexed '" + intIndex + "' is selected successfully.");
  }

  public static void SelectItemByValue(WebElement query, String value) {
   if (value.equals("")) {
      Automation.testReport.log(LogStatus.INFO,
          "Since a valid value for index is not given, assuming the default value ");
    }

    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL, "Failed to retrieve the locator value.");
    }
    Select listBox = new Select(query);

    listBox.selectByValue(value);

    List<WebElement> items = listBox.getAllSelectedOptions();

    for (WebElement webElement : items) {
      System.out.println(webElement.getText());
      Automation.testReport.log(LogStatus.PASS,
          "The item " + webElement.getText() + " is selected successfully");
    }
    try {
      // Thread.sleep(Automation.IntervalTimeOut);
    } catch (Exception localException) {
    }

    System.out.println(value);
    Automation.testReport.log(LogStatus.FAIL,
        " The item valued '" + value + "' is selected successfully.");
  }

  public static void SelectItemByVisibleText(WebElement query, String value) {
    if (value.equals("")) {
      Automation.testReport.log(LogStatus.INFO,
          "Since a valid value for index is not given, assuming the default value ");
    }

    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL, "Failed to retrieve the locator value.");
    }
    Select listBox = new Select(query);

    listBox.selectByVisibleText(value);

    List<WebElement> items = listBox.getAllSelectedOptions();

    for (WebElement webElement : items) {
      System.out.println(webElement.getText());
      Automation.testReport.log(LogStatus.PASS,
          "The item " + webElement.getText() + " is selected successfully");
    }
  }

}
