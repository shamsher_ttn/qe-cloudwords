/**
 *
 */
package com.cloudwords.qe.fw.wrapper;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.apache.commons.io.FileUtils;
import org.apache.http.client.fluent.Request;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebElement;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.FwUtil;
import com.cloudwords.qe.fw.utillib.NumberUtil;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author Shamsher Singh
 * @date 13-Aug-2016
 */
public class General extends Automation {

  private static final Logger logger = Logger.getLogger(General.class);
  private static final int BUFFER_SIZE = 4096;

  static {
    // for localhost testing only
    javax.net.ssl.HttpsURLConnection
    .setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {

      public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
        if (hostname.equals("localhost")) {
          return true;
        }
        return false;
      }
    });
  }

  public static void OpenURL() {
    int resp_code;
    String url = Automation.configHashMap.get("BASEURL");
    if ((url == null) || (url.trim().equals(""))) {
      Automation.testReport.log(LogStatus.FAIL,
          "The url '" + url + "' is invalid , please verify.");
    }

    try {
      resp_code = Request.Get(url).execute().returnResponse().getStatusLine().getStatusCode();
      if (resp_code == 200) {
        logger.info("The TestURL in config.properties is a valid URL");
        Automation.testReport.log(LogStatus.PASS,
            "The TESTURL in Config.properties is a valid URL");
        Automation.driver.get(url);
        Automation.testReport.log(LogStatus.PASS, " The URL '" + url + "' is set successfullAutomation.Automation.drivery.");
      } else {
        //  Assert.assertTrue(false);
        Automation.testReport.log(LogStatus.FAIL,
            "The TESTURL in Config.properties is an INVALID URL, please check and update");
        logger.warn("The TestURL in config.properties is an INVALID URL, please check and update");
      }
    } catch (Exception localException) {

      logger.error(
          "The Server for the TestURL mentioned in config.properties is down, please check StackTrace: <br/>"
              + FwUtil.getStackTrace(localException));
    }

  }

  public static void DeleteCookiesOnCurrentDomain() {

    try {
      Automation.driver.manage().deleteAllCookies();
    } catch (Exception e) {
      Automation.testReport.log(LogStatus.FAIL,
          "Action: DeleteCookiesOnCurrentDomain " + "Status: Failed    "
              + "Message: Exception while deleting the cookies. " + " Exception: "
              + e.getMessage());
      Automation.testReport.log(LogStatus.ERROR,
          "Action: DeleteCookiesOnCurrentDomain " + "Status: Failed    "
              + "Message: Exception while deleting the cookies. " + " StackTrace: <br/>"
              + FwUtil.getStackTrace(e));
    }
    Automation.testReport.log(LogStatus.PASS, "Action: DeleteCookiesOnCurrentDomain " + "    "
        + "Status: Passed    " + "Message: All the cookies deleted successfully.");

  }

  public static void CompareString(String firstString, String secondString,
      String blnCaseSensitive) {

    if ((firstString == null) || (secondString == null)) {
      Automation.testReport.log(LogStatus.FAIL, "Action: Comparing strings " + "    "
          + "Status : Failed \t\t"
          + "Message : One / Both of the strings send are either Empty, Null or is not a string.Please verify.");
    }

    if (blnCaseSensitive.equalsIgnoreCase("")) {
      blnCaseSensitive = "True";

      Automation.testReport.log(LogStatus.INFO,
          "Message: The CaseSensitive data passed is empty. Hence its set to the default value 'True'");
    } else if (!CommonUtil.isBoolean(blnCaseSensitive)) {
      blnCaseSensitive = "True";

      Automation.testReport.log(LogStatus.INFO, "Message: The CaseSensitive data passed is '"
          + blnCaseSensitive + "'. Hence its set to the default value 'True'");
    } else {
      blnCaseSensitive = blnCaseSensitive.trim();
    }

    firstString = firstString.replace("\n", " ");
    secondString = secondString.replace("\n", " ");
    boolean compareResult;

    if (Boolean.parseBoolean(blnCaseSensitive))
      compareResult = firstString.equals(secondString);
    else {
      compareResult = firstString.equalsIgnoreCase(secondString);
    }
    if (compareResult) {

      Automation.testReport.log(LogStatus.PASS,
          "Action: Comparing strings " + "    " + "Status : Passed \t\t" + "Message : The String '"
              + firstString + "' and '" + secondString + "' are equal."
              + " With CaseSensitive comparison as : " + blnCaseSensitive);
    } else {
      Automation.testReport.log(LogStatus.FAIL,
          "Action: Comparing strings" + "    " + "Status : Failed \t\t" + "Message : The String '"
              + firstString + "' and '" + secondString + "' are not equal."
              + " With CaseSensitive comparison as : " + blnCaseSensitive + ", Please Verify.");
    }
  }

  public static void RefreshPage() {
    try {
      Automation.driver.navigate().refresh();
    } catch (Exception e) {
      Automation.testReport.log(LogStatus.FAIL, "Action: " + "    "
          + "Message: Exception while refreshing the page. " + " Exception: " + e.getMessage());
      Automation.testReport.log(LogStatus.ERROR,
          "Action: " + "Message: Exception while refreshing the page " + " StackTrace: <br/>"
              + FwUtil.getStackTrace(e));
    }
    Automation.testReport.log(LogStatus.PASS,
        "Action: " + "Message: Page is refreshed successfully");
  }

  public static void SelectFrame(WebElement element) {

    if (element == null) {
      Automation.testReport.log(LogStatus.WARNING, "Action: " + "    " + "Status: Failed    "
          + "Message: Either the Locator/LocatorType is invalid. Please verify.");
    }

    Automation.driver.switchTo().defaultContent();
    Automation.testReport.log(LogStatus.PASS, "Action: " + "    " + "Status: Passed    "
        + "Message:Switched to the default frame successfully.");

    if (element == null) {
      Automation.testReport.log(LogStatus.FAIL, "Action: " + "    " + "Status: Failed    "
          + "Message: Either the frame does not exist or an error occurred, please verify");
    }
    try {
      Automation.driver.switchTo().frame(element);
      Automation.testReport.log(LogStatus.PASS, "Action: " + "    " + "Status: Passed    "
          + "Message: The frame is selected successfully.");

    } catch (NoSuchFrameException nsfe) {
      Automation.testReport.log(LogStatus.ERROR,
          "Action: " + "    " + "Status: Failed    "
              + "Message: Failed to select the frame. Either the frame does not exist "
              + "OR the given element is neither an IFRAME nor a FRAME element");

    }
  }

  public static void CompareArray(Object firstArray, Object secondArray, String match) {

    ArrayList mainList = (ArrayList) firstArray;
    ArrayList secondaryList = (ArrayList) secondArray;
    boolean status = false;
    String stat = "";
    String msg = "";
    if ((match == null) || (match.trim().equals(""))) {
      Automation.testReport.log(LogStatus.WARNING,
          "The paramter 'match' is not given,so assuming the default value as 'exact'");
      match = "exact";
    }

    if (match.trim().equalsIgnoreCase("exact")) {
      status = CommonUtil.compareStringListInSequence(mainList, secondaryList, false, true);
      if (status) {
        msg = "The arrays " + mainList.toString() + " and " + secondaryList.toString()
        + " are matched exactly successfully.";
        stat = "Passed";

      } else {
        msg = "The arrays " + mainList.toString() + " and " + secondaryList.toString()
        + " are not matched exactly.";
        stat = "Failed";

      }
    } else if (match.trim().equalsIgnoreCase("partial")) {
      status = CommonUtil.isSubList(mainList, secondaryList);
      if (status) {
        msg = "The array " + secondaryList.toString() + " is a sublist of " + mainList.toString()
        + ".";
        stat = "Passed";

      } else {
        msg = "The array " + secondaryList.toString() + " is not a sublist of "
            + mainList.toString() + ".";
        stat = "Failed";

      }
    } else {
      Automation.testReport.log(LogStatus.FAIL, "Action: " + "    " + "Status: Failed   "
          + "Message:  The given match " + "'" + match + "'" + " is not supported");
    }

    msg = msg + CommonUtil.UtilMsg;
    Automation.testReport.log(LogStatus.INFO,
        "Action: " + "    " + "Status: " + stat + "    " + "Message:  " + msg);

  }

  public static void VerifyStringContainsValue(String strMainString, String strSubString,
      String blnCaseSensitive) {

    if ((strMainString == null) || (strSubString == null)) {
      Automation.testReport.log(LogStatus.FAIL, "Action: " + "    " + "Status : Failed \t\t"
          + "Message : One / Both of the strings send are either Empty, Null or is not a string.Please verify.");
    }

    if (blnCaseSensitive.equalsIgnoreCase("")) {
      blnCaseSensitive = "True";

      Automation.testReport.log(LogStatus.WARNING,
          "Message: The CaseSensitive data passed is empty. Hence its set to the default value 'True'");
    } else if (!CommonUtil.isBoolean(blnCaseSensitive)) {
      blnCaseSensitive = "True";

      Automation.testReport.log(LogStatus.WARNING, "Message: The CaseSensitive data passed is '"
          + blnCaseSensitive + "'. Hence its set to the default value 'True'");
    } else {
      blnCaseSensitive = blnCaseSensitive.trim();
    }

    strMainString = strMainString.replace("\n", " ");
    strSubString = strSubString.replace("\n", " ");
    boolean compareResult;
    if (Boolean.parseBoolean(blnCaseSensitive))
      compareResult = strMainString.contains(strSubString);
    else {
      compareResult = strMainString.toUpperCase().contains(strSubString.toUpperCase());
    }
    if (compareResult) {
      Automation.testReport.log(LogStatus.PASS,
          "Action: " + "    " + "Status : Passed \t\t" + "Message : The String '" + strMainString
          + "' contains  the substring '" + strSubString
          + "' With CaseSensitive comparison as : " + blnCaseSensitive);
    } else {

      Automation.testReport.log(LogStatus.FAIL,
          "Action: " + "    " + "Status : Failed" + "Message : The String '" + strMainString
          + "' does not contains  the substring '" + strSubString
          + "' With CaseSensitive comparison as : " + blnCaseSensitive + ", Please Verify.");
    }

  }

  public static void AttachFile(WebElement query, String filePath) {

    if ((filePath == null) || (filePath.trim().equals("")) || (filePath.isEmpty())) {
      Automation.testReport.log(LogStatus.FAIL,
          "Status: Failed    " + "Message: The file path is invalid , please verify.");

    }

    File file = new File(filePath);
    if (!file.exists()) {
      Automation.testReport.log(LogStatus.FAIL,
          "Status: Failed    " + "Message: The file does not exist , please verify.");

    }

    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL,
          "Status: Failed    " + "Message: Failed to retrieve the locator value.");

      Automation.testReport.log(LogStatus.FAIL,
          "Status: Failed    " + "Message: The file input object having the locator " + "'" + query
          + "'" + " does not exist , please verify");
    }

    query.sendKeys(new CharSequence[] { filePath.trim() });

  }

  public static void selectDate(String date) throws ParseException {
    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    Date dateToBeSelected = df.parse(date);
    Date currentDate = new Date();
    String monthYearDisplayed = Automation.driver
        .findElement(By.xpath("//*[@id='ui-datepicker-div']/div[1]/div/div")).getText();
    System.out.println(monthYearDisplayed);
    String month = new SimpleDateFormat("MMMM").format(dateToBeSelected);
    String year = new SimpleDateFormat("yyyy").format(dateToBeSelected);
    String day = new SimpleDateFormat("d").format(dateToBeSelected);
    String monthYearToBeSelected = month + " " + year;
    System.out.println(monthYearToBeSelected);

    while (true) {
      if (monthYearToBeSelected.equals(monthYearDisplayed)) {
        // select date
        try {
          Thread.sleep(5000L);
        } catch (InterruptedException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
        Automation.driver.findElement(By.xpath("//a[text()='" + day + "']")).click();
        System.out.println("Found and selected");
        break;

      } else { // navigate to correct month and year

        if (dateToBeSelected.after(currentDate)) {
          Automation.driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div[2]/div/a/span")).click();
        } else {
          Automation.driver.findElement(By.xpath("//*[@id='ui-datepicker-div']/div[1]/div/a/span")).click();
        }

      }

      monthYearDisplayed = Automation.driver
          .findElement(By.xpath("//*[@id='ui-datepicker-div']/div[1]/div/div")).getText();
    }

  }

  public static void StoreSubString(String strKey, String strMainString, String startIdx, String uptoLen) {
    String stSubString = "";

    if ((strKey == null) || (strKey.trim().equalsIgnoreCase(""))) {
      Automation.testReport.log(LogStatus.FAIL,  "Message: The Key passed is not valid. Please Verify.");
    }

    if (startIdx.equalsIgnoreCase("")) {
      startIdx = "1";

      Automation.testReport.log(LogStatus.WARNING,
          "Message: The start index data is empty. Hence its set to the default value '1'");
    } else {
      if (!NumberUtil.isPostiveInteger(startIdx)) {
        Automation.testReport.log(LogStatus.FAIL,"Message: The start index '" + startIdx + "' is not a valid integer, please verify.");
      }
      startIdx = startIdx.trim();
    }

    if (uptoLen.equalsIgnoreCase("")) {
      uptoLen = String.valueOf(strMainString.length());

      Automation.testReport.log(LogStatus.INFO,
          "Message: The upto length index passed is empty. Hence its set to the default value 'Main string length'= "
              + uptoLen);
    } else {
      if (!NumberUtil.isPostiveInteger(uptoLen)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: The length upto '" + uptoLen + "' is not a valid integer, please verify.");
      }
      uptoLen = uptoLen.trim();
    }

    int intLen = Integer.parseInt(uptoLen);
    int intStart = Integer.parseInt(startIdx);
    int intLenMainStr = strMainString.length();
    if (intLenMainStr > 0) {
      if (intLenMainStr >= intLen) {
        if ((intStart > 0) && (intStart <= intLenMainStr)) {
          if (intLen >= intStart) {
            try {
              stSubString = strMainString.substring(intStart, intLen);
              if (!FwUtil.storeData(strKey, stSubString))
                ; // break label621;
              Automation.testReport.log(LogStatus.PASS,"Message: The data '"
                  + stSubString + "' is stored succesfully in the key '" + strKey + "'");
            } catch (Exception e) {
              Automation.testReport.log(LogStatus.ERROR,"Exception occured in PressKeys : " + e);
            }
          } else {
            Automation.testReport.log(LogStatus.FAIL, "Message: The start  position provided '" + intStart
                + "' is greater than the length of the substring '" + intLen
                + "' . Please Verify.");
          }
        } else {
          Automation.testReport.log(LogStatus.FAIL, "Message:The start  position provided '" + intStart
              + "' is greater than the length of the substring '" + intLen + "' . Please Verify.");
        }
      } else {
        Automation.testReport.log(LogStatus.FAIL, "Message:The length provided '"
            + intLen + "' is greater than the length of the String '" + strMainString
            + "' . Please Verify.");

      }
    } else {
      Automation.testReport.log(LogStatus.FAIL, "Message:The String is Empty. Please Verify");
    }

  }

  public static void StoreStringLength(String key, String strMainstr, String intValue) {


    if ((key == null) || (key.trim().equalsIgnoreCase(""))) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The key passed is either null/empty, please verify.");
    }

    if (intValue.equalsIgnoreCase("")) {
      intValue = "0";

      Automation.testReport.log(LogStatus.FAIL,
          "Message: The increment value is empty. Hence its set to the default value '0'");
    } else {
      if (!NumberUtil.isInteger(intValue)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: The value '" + intValue + "' is not a valid integer, please verify.");

      }
      intValue = intValue.trim();
    }

    int intLength = strMainstr.length() + Integer.parseInt(intValue);

    if (FwUtil.storeData(key, Integer.toString(intLength))) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The data '" + intLength + "' is stored succesfully in the key '" + key + "'");

    } else {
      Automation.testReport.log(LogStatus.FAIL, "Message: Failed to store the data '" + intLength + "' in the key '" + key + "'");

    }
  }

  public static void ComparePattern(String strPattern, String strData, String blnCaseSensitive) {
    if ((strPattern == null) || (strPattern.equalsIgnoreCase("")) || (strData == null)) {
      Automation.testReport.log(LogStatus.FAIL,"Message : Either the pattern/data sent are either Empty, Null or not a string. Please verify.");
    }

    if (blnCaseSensitive.equalsIgnoreCase("")) {
      blnCaseSensitive = "True";

      Automation.testReport.log(LogStatus.INFO,
          "Message: The CaseSensitive data passed is empty. Hence its set to the default value 'True'");
    } else if (!CommonUtil.isBoolean(blnCaseSensitive)) {
      blnCaseSensitive = "True";

      Automation.testReport.log(LogStatus.INFO,
          "Message: The CaseSensitive data passed is invalid. Hence its set to the default value 'True'");
    } else {
      blnCaseSensitive = blnCaseSensitive.trim();
    }
    try {
      boolean caseSensitiveFlag = Boolean.parseBoolean(blnCaseSensitive);

      Pattern pattern = caseSensitiveFlag ? Pattern.compile(strPattern)
          : Pattern.compile(strPattern, 2);

      Matcher matcher = pattern.matcher(strData);
      boolean match = matcher.matches();
      if (match) {
        Automation.testReport.log(LogStatus.PASS, "Message : The data '" + strData + "' matches the Pattern  '" + strPattern
            + "' With CaseSensitive comparison as : " + blnCaseSensitive);
      } else {
        Automation.testReport.log(LogStatus.FAIL, "Message : The data '" + strData + "' does not match the Pattern '" + strPattern
            + "', With CaseSensitive comparison as : " + blnCaseSensitive + ", Please Verify.");
      }
    } catch (IllegalArgumentException patSysExc) {
      Automation.testReport.log(LogStatus.FAIL,"Message : A syntax error occurred in the pattern '" + strPattern
          + "', Please Verify.");
    }
  }

  public static void VerifyDifference(String int1, String int2, String differenceInt) {
    if (int1.equalsIgnoreCase("")) {
      int1 = "0";

      Automation.testReport.log(LogStatus.INFO,
          "Message: The first integer data is empty. Hence its set to the default value '0'");
    } else {
      if (!NumberUtil.isInteger(int1)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: The integer '" + int1 + "' is not a valid integer, please verify.");
         }
      int1 = int1.trim();
    }

    if (int2.equalsIgnoreCase("")) {
      int2 = "0";

      Automation.testReport.log(LogStatus.INFO,
          "Message: The second integer data is empty. Hence its set to the default value '0'");
    } else {
      if (!NumberUtil.isInteger(int2)) {
        Automation.testReport.log(LogStatus.FAIL,"Message: The integer '" + int2 + "' is not a valid integer, please verify.");
      }
      int2 = int2.trim();
    }

    if (differenceInt.equalsIgnoreCase("")) {
      differenceInt = "0";

      Automation.testReport.log(LogStatus.INFO,
          "Message: The difference data is empty. Hence its set to the default value '0'");
    } else {
      if (!NumberUtil.isInteger(differenceInt)) {
        Automation.testReport.log(LogStatus.FAIL,"Message: The difference '"
                + differenceInt + "' is not a valid integer, please verify.");
      }
      differenceInt = differenceInt.trim();
    }

    int diff = Integer.parseInt(int1) - Integer.parseInt(int2);
    if (diff == Integer.parseInt(differenceInt)) {
      Automation.testReport.log(LogStatus.PASS, "Message : The diffrence is equal to the intended difference " + differenceInt);
    } else {
      diff = Integer.parseInt(int2) - Integer.parseInt(int1);
      if (diff == Integer.parseInt(differenceInt)) {
        Automation.testReport.log(LogStatus.PASS, "Message : The diffrence is equal to the intended difference " + differenceInt);
      } else {
        Automation.testReport.log(LogStatus.FAIL,"Message : The diffrence is not equal to the intended difference " + differenceInt);
      }
    }
  }

  public static void  StoreSplitString(String strKey, String mainStr, String delimiter, String instance) {
    if ((strKey == null) || (strKey.trim().equalsIgnoreCase(""))) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The Key passed is not valid. Please Verify.");
   }

    if ((mainStr == null) || (delimiter == null)) {
      Automation.testReport.log(LogStatus.FAIL, "Message: Either the main string to be split OR the delimiter is not valid. Please Verify.");
     }

    if (instance.equalsIgnoreCase("")) {
      instance = "1";

      Automation.testReport.log(LogStatus.INFO,
          "Message: The Instance data is empty. Hence its set to the default value '1'");
    } else {
      if (!NumberUtil.isPostiveInteger(instance)) {
        Automation.testReport.log(LogStatus.FAIL,"Message: The instance '" + instance
            + "' is not a valid integer, please verify.Instance to be store starts at 1.");
        }
      instance = instance.trim();
    }
    int inst = Integer.parseInt(instance);
    String[] splitData;
    try {
      splitData = mainStr.split(delimiter);
    } catch (PatternSyntaxException pe) {
      splitData = mainStr.split("\\" + delimiter);
    }
    if (inst > splitData.length) {
      Automation.testReport.log(LogStatus.FAIL,"Message: After spliting the string '" + mainStr + "' " + splitData.length
          + " instance is created. " + inst + " instance does not exist.");
     }
    String data = splitData[(inst - 1)];
    if (FwUtil.storeData(strKey, data)) {
      Automation.testReport.log(LogStatus.PASS, "Message: The data '" + data + "' is stored successfully in the key '" + strKey + "'.");
    } else {
      Automation.testReport.log(LogStatus.FAIL, "Message: Failed to store the data '" + data + "' in the key " + strKey + "'.");
    }
  }

  public static void downloadsaveFile(String fileName,
      String fileUrl) throws MalformedURLException, IOException {
      FileUtils.copyURLToFile(new URL(fileUrl), new File(fileName));
     }


}
