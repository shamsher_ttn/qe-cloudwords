package com.cloudwords.qe.fw.wrapper;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxProfile;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.libraries.PublicVariables;


public class WebDriverFactory {

	private static WebDriverFactory instance;
	private static final String CHROME_DRIVER_LOCATION = "webdriver.chrome.driver";
	private static final String CHROME_DRIVER_NOT_DEFINED = "notDefined";
	private final String chromeDriverEnv;
	private final Map<String, WebDriverCreator> drivers = new HashMap<String, WebDriverCreator>();
	private static final Logger logger = Logger.getLogger(WebDriverFactory.class);

	public WebDriverFactory() {

		initializeWebDriverCreators();
		chromeDriverEnv = PublicVariables.CURRENTDir + PublicVariables.SEP + "resources"
				+ PublicVariables.SEP + "chromedriver.exe";
		System.setProperty(CHROME_DRIVER_LOCATION,
				(chromeDriverEnv != null) ? chromeDriverEnv : CHROME_DRIVER_NOT_DEFINED);
	}

	public static WebDriverFactory getInstance() {
		if (instance == null) {
			instance = new WebDriverFactory();
		}
		return instance;
	}

	private boolean isChromeDriverInstalled() {
		return System.getProperty(CHROME_DRIVER_LOCATION) != CHROME_DRIVER_NOT_DEFINED;
	}

	private void initializeWebDriverCreators() {
		logger.info("Initializing WebDriverCreators method to initalize required web driver");
		drivers.put("msie", new IEWebDriverCreator());
		drivers.put("chrome", new ChromeWebDriverCreator());
		drivers.put("chromium", new ChromiumWebDriverCreator());
		drivers.put("ghost", new GhostWebDriverCreator());
		drivers.put("grid", new GridWebDriverCreator());
		drivers.put("remotegrid", new RemoteGridWebDriverCreator());
		drivers.put("firefox", new FirefoxWebDriverCreator());
	}

	public WebDriver createDriver(String driverName) {
		if (drivers.containsKey(driverName)) {
			return drivers.get(driverName).create();
		} else {
			EventFiringWebDriver eventFiringWebDriver;
			WebDriver fireFoxDriver = new FirefoxDriver();
			eventFiringWebDriver = new EventFiringWebDriver(fireFoxDriver);
			String listenerEnabled = System.getProperty("driverlistener", "false");
			if (listenerEnabled.equals("true")) {
				return eventFiringWebDriver;
			} else {
				return fireFoxDriver;
			}
		}
	}


	private boolean isFirefoxExtensionsEnabled() {
		String extensionsEnabled =System.getProperty("firefoxExtensions", "false"); return
				"TRUE".equals(extensionsEnabled.toUpperCase()); }


	private FirefoxProfile getFirefoxProfile() {
		FirefoxProfile profile = new FirefoxProfile();
		profile.setAssumeUntrustedCertificateIssuer(true); // in order to enable natives on linux
		profile.setEnableNativeEvents(true);

		// automatically download files: profile.setPreference("browser.download.folderList", 2);
		profile.setPreference("browser.download.manager.showWhenStarting", false);
		profile.setPreference("browser.download.dir", System.getProperty("java.io.tmpdir"));
		profile.setPreference("browser.helperApps.neverAsk.saveToDisk", "application/msexcel");

		if (isFirefoxExtensionsEnabled()) {
			ClassLoader classLoader =
					Thread.currentThread().getContextClassLoader(); URL firebug =
					classLoader.getResource("extensions/firefox/firebug-1.7X.0b4.xpi"); URL fireStarter =
					classLoader.getResource("extensions/firefox/fireStarter-0.1a6.xpi"); URL netExport =
					classLoader.getResource("extensions/firefox/netExport-0.8b21.xpi");
					try {
						File firebugFile =new File(firebug.toURI()); File fireStarterFile = new File(fireStarter.toURI());
						File netExprotFile = new File(netExport.toURI()); try { profile.addExtension(firebugFile);
						profile.addExtension(fireStarterFile); profile.addExtension(netExprotFile);
						profile.setPreference("extensions.firebug.onByDefault", true);
						profile.setPreference("extensions.firebug.currentVersion", "2.0");
						profile.setPreference("extensions.firebug.previousPlacement", 1);
						profile.setPreference("extensions.firebug.defaultPanelName", "net");
						profile.setPreference("extensions.firebug.net.enableSites", true);
						profile.setPreference("extensions.firebug.netexport.alwaysEnableAutoExport", "true");
						} catch(IOException e) {
							throw new RuntimeException(e);
						}
					} catch (URISyntaxException e) {
						throw new
						RuntimeException(e);
					}
		} return profile;
	}


	/** ------------------------------------------------------------------------ **/
	/** WebDriver Creators **/
	/** ------------------------------------------------------------------------ **/

	private interface WebDriverCreator {
		WebDriver create();
	}

	private class IEWebDriverCreator implements WebDriverCreator {

		public WebDriver create() {
			DesiredCapabilities ieCapabilities = DesiredCapabilities.internetExplorer();
			ieCapabilities.setCapability("unexpectedAlertBehaviour", "ignore");
			ieCapabilities.setCapability("enablePersistentHover", true);
			ieCapabilities.setCapability(
					InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS, true);// Add this
			// desiredcapabilities
			// when the
			// security
			// level of IE not set to same.
			ieCapabilities.setBrowserName("SELENIUM");
			ieCapabilities.setJavascriptEnabled(true);
			ieCapabilities.setCapability("browserstack.ie.enablePopups", "true");
			ieCapabilities.setCapability("acceptSslCerts", "true");
			ieCapabilities.setCapability("browserstack.ie.noFlash", "true");
			File file = new File(PublicVariables.CURRENTDir + PublicVariables.SEP + "src/test/resources"
					+ PublicVariables.SEP + "IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver", file.getAbsolutePath());
			return new InternetExplorerDriver(ieCapabilities);
		}
	}

	private class ChromeWebDriverCreator implements WebDriverCreator {

		public WebDriver create() {
			if (!isChromeDriverInstalled()) {
				throw new RuntimeException("Chrome driver not installed");
			}
			ChromeOptions options = new ChromeOptions();
			options.addArguments("--ignore-certificate-errors");
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--disable-translate");
			options.addArguments("--start-maximized");
			HashMap<String, Object> chromePrefs = new HashMap<String, Object>();
			chromePrefs.put("profile.default_content_settings.popups", 0);
			chromePrefs.put("download.default_directory", Automation.downloadFilepath);
			HashMap<String, Object> chromeOptionsMap = new HashMap<String, Object>();
			options.setExperimentalOption("prefs", chromePrefs);
			options.addArguments("--test-type");

			DesiredCapabilities cap = DesiredCapabilities.chrome();
			cap.setCapability(ChromeOptions.CAPABILITY, chromeOptionsMap);
			cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			cap.setCapability(ChromeOptions.CAPABILITY, options);
			File file = new File(PublicVariables.CURRENTDir + PublicVariables.SEP + "src/test/resources"
					+ PublicVariables.SEP + OSDetector());
			System.setProperty("webdriver.chrome.driver", file.getAbsolutePath());
			return new ChromeDriver(options);
		}
	}

	private class ChromiumWebDriverCreator implements WebDriverCreator {

		public WebDriver create() {
			if (!isChromeDriverInstalled()) {
				throw new RuntimeException("Chrome/Chromium driver not installed");
			}
			ChromeOptions options = new ChromeOptions();
			options.setBinary(new File("/usr/bin/chromium-browser"));
			options.addArguments("--ignore-certificate-errors");
			options.addArguments("--disable-popup-blocking");
			options.addArguments("--disable-translate");
			options.addArguments("--start-maximized");
			return new ChromeDriver(options);
		}
	}

	private class GridWebDriverCreator implements WebDriverCreator {

		public WebDriver create() {
			DesiredCapabilities capability = DesiredCapabilities.firefox();
			FirefoxProfile profile = new FirefoxProfile();
			profile.setEnableNativeEvents(true);
			capability.setCapability(FirefoxDriver.PROFILE, profile);
			WebDriver driver = null;
			try {
				driver = new RemoteWebDriver(new URL("http://localhost:4444/wd/hub"), capability);
				// SessionId sessionId = ((RemoteWebDriver) driver).getSessionId();
				// logger.info("Running test on node: " +
				// SeleniumGridHandler.getNodeIpBySessionId(sessionId));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			return driver;
		}

	}

	private class RemoteGridWebDriverCreator implements WebDriverCreator {

		public WebDriver create() {
			DesiredCapabilities capability = DesiredCapabilities.firefox();
			FirefoxProfile profile = new FirefoxProfile();
			profile.setEnableNativeEvents(true);
			capability.setCapability(FirefoxDriver.PROFILE, profile);
			WebDriver driver = null;
			try {
				driver = new RemoteWebDriver(new URL("http://0.0.0.0:4444/wd/hub"), capability);
				// SessionId sessionId = ((RemoteWebDriver) driver).getSessionId();
				// logger.info("Running test on node: " +
				// SeleniumGridHandler.getNodeIpBySessionId(sessionId));
			} catch (MalformedURLException e) {
				e.printStackTrace();
			}
			return driver;
		}

	}

	private class GhostWebDriverCreator implements WebDriverCreator {

		public WebDriver create() {
			String ghostDriverUrl = System.getProperty("ghost.url", "http://localhost:4444");
			try {
				return new RemoteWebDriver(new URL(ghostDriverUrl), DesiredCapabilities.chrome());
			} catch (MalformedURLException e) {
				throw new RuntimeException(e);
			}
		}
	}
	private class FirefoxWebDriverCreator implements WebDriverCreator {

		public WebDriver create() {
			FirefoxProfile profile = new FirefoxProfile();
			profile.setPreference("browser.download.folderList", 2);
			profile.setPreference("browser.download.manager.showWhenStarting", false);
			profile.setPreference("browser.download.dir", Automation.downloadFilepath);
			profile.setPreference("browser.helperApps.neverAsk.openFile",
					"text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
			profile.setPreference("browser.helperApps.neverAsk.saveToDisk",
					"text/csv,application/x-msexcel,application/excel,application/x-excel,application/vnd.ms-excel,image/png,image/jpeg,text/html,text/plain,application/msword,application/xml");
			profile.setPreference("browser.helperApps.alwaysAsk.force", false);
			profile.setPreference("browser.download.manager.alertOnEXEOpen", false);
			profile.setPreference("browser.download.manager.focusWhenStarting", false);
			profile.setPreference("browser.download.manager.useWindow", false);
			profile.setPreference("browser.download.manager.showAlertOnComplete", false);
			profile.setPreference("browser.download.manager.closeWhenDone", false);
			WebDriver driver = new FirefoxDriver(profile);
			return driver;
		}
	}

	public static String OSDetector () {
		String os = System.getProperty("os.name").toLowerCase();
		if (os.contains("win")) {
			return "Windows-chromedriver.exe";
		} else if (os.contains("nux") || os.contains("nix")) {
			return "Linux-chromedriver";
		}else if (os.contains("mac")) {
			return "Mac-chromedriver";
		}else{
			return "Others";
		}
	}
}
