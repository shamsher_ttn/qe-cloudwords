/**
 *
 */
package com.cloudwords.qe.fw.wrapper;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

import com.cloudwords.qe.fw.base.Automation;
import com.relevantcodes.extentreports.LogStatus;


/**
 * @author shamsher
 * @date 08-Nov-2016
 */
public class WebButton extends Automation{


  public static void RightClick(WebElement query)
  {

    if ((query == null) && (!EXISTLOCATORVALUE)) {
      testReport.log(LogStatus.FAIL,"Status: Failed    " + "Message: Failed to retrieve the locator value.");

    }if (query == null) {
      testReport.log(LogStatus.FAIL,"Status: Failed    " + "Message: The element does not exist , please verify");

    }

    try {
      Actions action = new Actions(driver).contextClick(query);
      action.build().perform();
      testReport.log(LogStatus.PASS, "Status: Passed    " + "Message: The right clicked successfully.");
    } catch (Exception e) {
      testReport.log(LogStatus.FAIL,"Status: Failed    " +
          "Message: Failed to select the option. The option may not exist.Please verify" + e.getMessage());
    }

  }

}
