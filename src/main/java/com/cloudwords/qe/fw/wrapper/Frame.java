/**
 *
 */
package com.cloudwords.qe.fw.wrapper;

import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.WebElement;

import com.cloudwords.qe.fw.base.Automation;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author Shamsher Singh
 * @date 25-Aug-2016
 */
public class Frame extends Automation {
  /**
   * @param locator
   */
    private static final Logger logger = Logger.getLogger(Frame.class);

  public static void SelectFrame(WebElement locator) {
    if (locator == null) {
      Automation.testReport.log(LogStatus.FAIL,
          "Action: " + "    " + "Message: Failed to retrieve the locator value.");
    }
    if (locator == null) {
      Automation.testReport.log(LogStatus.FAIL,
          "Action: " + "    " + "Message:  Does not exist , please verify");
    }
    try {
      Automation.driver.switchTo().frame(locator);
      Automation.testReport.log(LogStatus.PASS,
          "Action: " + "    " + "Message: Selected successfully.");
    } catch (NoSuchFrameException nsfe) {
      Automation.testReport.log(LogStatus.FAIL,
          "Action: " + "    " + "Message: Failed to select the frame. Either the "
              + " does not exist "
              + "OR the given element is neither an IFRAME nor a FRAME element");

    }

  }
}