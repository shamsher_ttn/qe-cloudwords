/**
 *
 */
package com.cloudwords.qe.fw.wrapper;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.FwUtil;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author Shamsher Singh
 * @date 13-Aug-2016
 */
public class WebObject extends Automation {

  public static String WebObjectMsg = "";
  public static boolean EXISTLOCATORVALUE;
  public static boolean setImplicitWait = false;
  public static boolean waitForObjectToExist = true;
  public static long implicitWaitTime = 0L;
  private static final Logger logger = Logger.getLogger(WebObject.class);


  public static boolean isElementPresent(String locator) {
    Automation.testReport.log(LogStatus.INFO, "Trying to find element -> " + locator);
    int s = Automation.driver.findElements(By.xpath(locator)).size();
    if (s == 0) {
      Automation.testReport.log(LogStatus.INFO, "Element not found");
      return false;
    } else {
      Automation.testReport.log(LogStatus.INFO, "Element found");
      return true;
    }
  }

  public static void StorePropertyValue(WebElement query, String strKey, String property) {

    if ((strKey == null) || (strKey.trim().equals("")) || (strKey.isEmpty())) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The Key passed is not valid. Please Verify.");
    }

    if ((property == null) || (property.trim().equals("")) || (property.isEmpty())) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The property passed is not valid. Please Verify.");
    }

    if ((query == null) && (!EXISTLOCATORVALUE)) {
      Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");

    }
    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The  does not exist , please verify");

    }
    String prop = null;
    if (property.trim().equalsIgnoreCase("text")) {
      prop = query.getText();
    } else {
      prop = query.getAttribute(property);
    }

    if (prop == null) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The property " + "'"
              + property + "'" + " does not exist for the object.Failed to store the '" + property
              + "' property value.");
    }
    if (FwUtil.storeData(strKey, prop)) {
      Automation.testReport.log(LogStatus.PASS, "Message: " + property + " property value '" + prop
          + "' is stored successfully in the key '" + strKey + "'.");
    } else {
      Automation.testReport.log(LogStatus.FAIL, "Message: Failed to store the '" + property + "' property value.");
    }
  }

  public static void VerifyExistence(WebElement query, String existence) {

    boolean objExist = true;

    if ((!CommonUtil.isBoolean(existence)) || (existence.equals(""))
        || (existence.trim().isEmpty())) {
     Automation.testReport.log(LogStatus.INFO,
          "Since a valid value for existence is not given, assuming the default value as 'TRUE'");
      objExist = true;
    } else {
      objExist = Boolean.parseBoolean(existence.trim());
    }

    if ((query == null) && (!EXISTLOCATORVALUE)) {
      Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");
    }

    if (query != null) {
      if (objExist) {
        Automation.testReport.log(LogStatus.PASS,"Message: The object class exists.");
      } else {
        Automation.testReport.log(LogStatus.FAIL,"Message: The object class exists.");
      }

    } else if (objExist) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The object class does not exists.");
    } else {
      Automation.testReport.log(LogStatus.PASS,"Message: The object class does not exists.");
    }
  }

  public static void VerifyVisibility(WebElement query, String visibility) {

    boolean objVisible = true;

    if ((!CommonUtil.isBoolean(visibility)) || (visibility.equals(""))
        || (visibility.trim().isEmpty())) {
      Automation.testReport.log(LogStatus.INFO,
          "Since a valid value for visibility is not given, assuming the default value as 'TRUE'");
      objVisible = true;
    } else {
      objVisible = Boolean.parseBoolean(visibility.trim());
    }

    if ((query == null) && (!EXISTLOCATORVALUE)) {
      Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");
    }
    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The " + query + " does not exist , please verify");

    }

    int intVisible = visible(query, true);
    if (intVisible == 0) {
      if (objVisible) {
        Automation.testReport.log(LogStatus.PASS,"Message: The " + query + " is visible.");
      } else {
        Automation.testReport.log(LogStatus.PASS, "Message: The " + query + " is visible.");
      }

    } else if (objVisible) {
      Automation.testReport.log(LogStatus.FAIL,"Message: The " + query + " is not visible.");
    } else {
      Automation.testReport.log(LogStatus.FAIL, "Message: The " + query + " is not visible.");
    }
  }

  public static void VerifyEnablity(WebElement query, String enablity) {

    boolean objVisible = true;

    if ((!CommonUtil.isBoolean(enablity)) || (enablity.equals(""))
        || (enablity.trim().isEmpty())) {
      Automation.testReport.log(LogStatus.INFO,
          "Since a valid value for visibility is not given, assuming the default value as 'TRUE'");
      objVisible = true;
    } else {
      objVisible = Boolean.parseBoolean(enablity.trim());
    }

    if ((query == null) && (!EXISTLOCATORVALUE)) {
      Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");
    }
    if (query == null) {
      Automation.testReport.log(LogStatus.FAIL, "Message: The " + query + " does not exist , please verify");

    }

    int intVisible = enabled(query, true);
    if (intVisible == 0) {
      if (objVisible) {
        Automation.testReport.log(LogStatus.PASS,"Message: The " + query + " is enable.");
      } else {
        Automation.testReport.log(LogStatus.PASS, "Message: The " + query + " is enable.");
      }

    } else if (objVisible) {
      Automation.testReport.log(LogStatus.FAIL,"Message: The " + query + " is not enable.");
    } else {
      Automation.testReport.log(LogStatus.FAIL, "Message: The " + query + " is not enable.");
    }
  }

  public static int visible(WebElement element, boolean blnVisible) {
    int stat = 1;

    boolean visible = false;
    visible = element.isDisplayed();
    if (blnVisible) {
      if (visible)
        stat = 0;
      else {
        stat = 2;
      }
    } else if (!visible)
      stat = 0;
    else {
      stat = 2;
    }

    return stat;
  }

  public static int enabled(WebElement element, boolean blnEditable) {
    int stat = 1;

    boolean editable = false;
    editable = element.isEnabled();
    if (blnEditable) {
      if (editable)
        stat = 0;
      else {
        stat = 2;
      }
    } else if (!editable)
      stat = 0;
    else {
      stat = 2;
    }

    return stat;
  }

}
