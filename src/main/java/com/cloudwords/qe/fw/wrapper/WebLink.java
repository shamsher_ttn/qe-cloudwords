/**
 *
 */
package com.cloudwords.qe.fw.wrapper;

import java.util.Iterator;
import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;

import com.cloudwords.qe.fw.base.Automation;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author Shamsher Singh
 * @date 27-Aug-2016
 */
public class WebLink extends Automation {

    private static final Logger logger = Logger.getLogger(WebLink.class);


  public void VerifyLinkOnPage(String link) {
    if ((link == null) || (link.trim().equals("")) || (link.isEmpty())) {
      Automation.testReport.log(LogStatus.FAIL,
          "Message: The link's text passed is null. Please Verify.");
      Assert.assertTrue(false, "The link's text passed is null. Please Verify.");
    }

    List<WebElement> links = Automation.driver.findElements(By.tagName("a"));

    for (WebElement webElement : links) {
      if (!webElement.getText().equalsIgnoreCase(link))
        continue;
      Automation.testReport.log(LogStatus.PASS,
          "Message: The link '" + link + "' is present on the page.");
    }

    Automation.testReport.log(LogStatus.FAIL,
        "Action: " + "Message: The link '" + link + "' is not present on the page, please verify.");
    Assert.assertTrue(false, "The link '" + link + "' is not present on the page, please verify.");
  }

  public void clickLinkByHref(String href ) {
    List<WebElement> anchors = driver.findElements(By.tagName("a"));
    Iterator<WebElement> i = anchors.iterator();

    while(i.hasNext()) {
      WebElement anchor = i.next();
      if(anchor.getAttribute("href").contains(href)) {
        anchor.click();
        break;
      }
    }
  }
}
