/**
 *
 */
package com.cloudwords.qe.fw.wrapper;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.FwUtil;
import com.cloudwords.qe.fw.utillib.NumberUtil;
import com.cloudwords.qe.fw.utillib.StringUtil;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author shamsher
 * @date 24-Sep-2016
 */
public class WebTable extends Automation {
    private static final Logger logger = Logger.getLogger(WebTable.class);


  protected static String TableMsg;

  /**
   * @param paramWebElement
   * @param paramInt1
   * @param paramInt2
   * @param paramInt3
   * @return
   */
  public static WebElement getTableCellTH(WebElement paramWebElement, int paramInt1, int paramInt2,
      int paramInt3) {
    int i = 0;
    WebElement localWebElement;
    if (paramInt2 <= paramInt1) {
      localWebElement = paramWebElement.findElement(By.xpath(
          "thead/tr[" + String.valueOf(paramInt2) + "]/td[" + String.valueOf(paramInt3) + "]"));
    } else {
      i = paramInt2 - paramInt1;
      localWebElement = paramWebElement.findElement(
          By.xpath("tbody/tr[" + String.valueOf(i) + "]/td[" + String.valueOf(paramInt3) + "]"));
    }
    return localWebElement;
  }

  /**
   * @param paramArrayOfString
   * @param paramString
   * @return
   */
  protected static int getDataInstance(String[] paramArrayOfString, String paramString) {
    int i = 0;
    for (int j = 0; j < paramArrayOfString.length; j++) {
      if (!StringUtil.stringEqual(paramArrayOfString[j], paramString, false, true, true)) {
        continue;
      }
      i++;
    }
    return i;
  }

  /**
   * @param paramWebElement
   * @param paramInt
   * @param paramBoolean
   * @return
   */
  protected static String[] getColumnData(WebElement paramWebElement, int paramInt,
      boolean paramBoolean) {
    TableMsg = "";
    String[] arrayOfString = null;
    int i = getRowCount(paramWebElement);
    Automation.testReport.log(LogStatus.INFO, "rowcnt= " + i);
    int j = 0;
    WebElement localWebElement1 = null;
    List localList = paramWebElement.findElements(By.xpath("tbody/tr"));
    arrayOfString = new String[localList.size()];
    int k = 0;
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext()) {
      WebElement localWebElement2 = (WebElement) localIterator.next();
      try {
        localWebElement1 = localWebElement2.findElement(
            By.xpath(".//*[local-name(.)='th' or local-name(.)='td'][" + paramInt + "]"));
        arrayOfString[k] = localWebElement1.getText();
        k += 1;
        j = 1;
      } catch (Exception localException) {
        if (paramBoolean) {
          arrayOfString[k] = "";
          k += 1;
          continue;
        }
        arrayOfString = null;
        break;
      }
    }
    if (j == 0) {
      arrayOfString = null;
    }
    return arrayOfString;
  }

  /**
   * @param paramWebElement
   * @param paramInt
   * @return
   */
  protected static String[] getRowData(WebElement paramWebElement, int paramInt) {
    String[] arrayOfString = null;
    int i = paramWebElement.findElements(By.xpath("thead/tr")).size();
    int j = 0;
    List localList = null;
    if (paramInt <= i) {
      localList = paramWebElement.findElements(By.xpath("thead/tr[" + paramInt + "]/*"));
      j = localList.size();
    } else {
      int k = paramInt - i;
      localList = paramWebElement.findElements(By.xpath("tbody/tr[" + k + "]/*"));
      j = localList.size();
    }
    arrayOfString = new String[j];
    int k = 0;
    Iterator localIterator = localList.iterator();
    while (localIterator.hasNext()) {
      WebElement localWebElement = (WebElement) localIterator.next();
      String str = localWebElement.getText();
      arrayOfString[k] = str;
      k++;
    }
    return arrayOfString;
  }

  /**
   * @param paramWebElement
   * @return
   */
  public static int getRowCount(WebElement paramWebElement) {
    TableMsg = "";
    int i = -1;
    int j = paramWebElement.findElements(By.xpath("thead/tr")).size();
    int k = paramWebElement.findElements(By.xpath("tbody/tr")).size();
    if (j + k == 0) {
      i = -1;
      TableMsg = "Either the table does not have any rows OR the row xpath is wrong.";
    } else {
      i = j + k;
      Automation.testReport.log(LogStatus.INFO, "rows= " + i);
    }
    return i;
  }

  /**
   * @param paramWebElement
   * @param paramInt
   * @return
   */
  protected static int getColCount(WebElement paramWebElement, int paramInt) {
    int i = -1;
    int j = paramWebElement.findElements(By.xpath("thead/tr")).size();
    int k = 0;
    int m = 0;
    if (paramInt <= j) {
      k = paramWebElement.findElements(By.xpath("thead/tr[" + String.valueOf(paramInt) + "]/*"))
          .size();
    } else {
      m = paramInt - j;
      k = paramWebElement.findElements(By.xpath("tbody/tr[" + String.valueOf(m) + "]/*")).size();
    }
    if (k < 1) {
      i = -1;
      TableMsg = "Either the table does not have the row:" + paramInt + " OR the xpath is wrong.";
    } else {
      i = k;
    }
    return i;
  }

  /**
   * @param paramWebElement
   * @param paramString
   * @return
   */
  protected int getColHeaderIndex(WebElement paramWebElement, String paramString) {
    List localList = paramWebElement.findElements(By.xpath("thead/tr/th"));
    if (localList.size() > 0) {
      for (int i = 0; i < localList.size(); i++) {
        if (((WebElement) localList.get(i)).getText().equals(paramString)) {
          return i + 1;
        } else {
          return -1;
        }
      }
    }
    return 0;
  }

  /**
   * @param paramString
   * @param paramInt
   * @return
   */
  protected String xpathColCount(String paramString, int paramInt) {
    return paramString + "/tbody/tr[" + String.valueOf(paramInt) + "]/*";
  }

  /**
   * @param paramString
   * @return
   */
  protected String xpathRowCount(String paramString) {
    return paramString + "/tbody/tr";
  }

  /**
   * @param paramString
   * @param paramInt1
   * @param paramInt2
   * @return
   */
  protected String xpathRowCol(String paramString, int paramInt1, int paramInt2) {
    return paramString + "/tbody/TR[" + String.valueOf(paramInt1) + "]/TD["
        + String.valueOf(paramInt2) + "]";
  }

  /**
   * @param paramInt
   * @return
   */
  protected int getHtmlRowIndex(int paramInt) {
    return paramInt - 1;
  }

  /**
   * @param paramInt
   * @return
   */
  protected int getHtmlColumnIndex(int paramInt) {
    return paramInt - 1;
  }

  /**
   * @param paramWebElement
   * @param paramInt1
   * @param paramInt2
   * @param paramInt3
   * @return
   */
  public static WebElement getTableCell(WebElement paramWebElement, int paramInt1, int paramInt2,
      int paramInt3) {
    int i = 0;
    WebElement localWebElement = null;
    if (paramInt2 <= paramInt1) {
      localWebElement = paramWebElement.findElement(By.xpath(
          "/thead/TR[" + String.valueOf(paramInt2) + "]/TH[" + String.valueOf(paramInt3) + "]"));
    } else {
      i = paramInt2 - paramInt1;
      localWebElement = paramWebElement.findElement(
          By.xpath("tbody/TR[" + String.valueOf(i) + "]/TD[" + String.valueOf(paramInt3) + "]"));
    }
    return localWebElement;
  }

  /**
   * @param localWebElement
   * @param strStoreKey
   */
  public static void StoreRowCount(WebElement localWebElement, String strStoreKey) {

    try {
      if ((strStoreKey == null) || (strStoreKey.trim().equals(""))) {
        Automation.testReport.log(LogStatus.ERROR,
            "Message: The Key passed is not valid. Please Verify.");

      }
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);

      if (localWebElement == null) {
        Automation.testReport.log(LogStatus.ERROR,
            "Message: The webtable does not exist , please verify");

      }
      int k = getRowCount(localWebElement);
      if (k == -1) {
        Automation.testReport.log(LogStatus.ERROR, "Message: " + TableMsg + ", please verify");

      } else if (FwUtil.storeData(strStoreKey, String.valueOf(k))) {
        Automation.testReport.log(LogStatus.INFO, "Message: The row count '" + k
            + " is stored successfully in the key " + strStoreKey + "'.");

      } else {
        Automation.testReport.log(LogStatus.ERROR,
            "Message: Failed to store the row count in the key " + strStoreKey + "'.");

      }

    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.ERROR,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));

    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));
    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
  }

  /**
   * @param localWebElement
   * @param strStoreKey
   * @param strColumnNum
   * @return
   */
  public static String[] StoreColumnData(WebElement localWebElement, String strStoreKey,
      String strColumnNum) {
    String[] arrayOfString = null;
    try {

      if (!NumberUtil.isPostiveInteger(strColumnNum)) {
        Automation.testReport.log(LogStatus.WARNING, "Message: The column '" + strColumnNum
            + "' is not a valid integer, please verify. Column index starts from 1.");
      }
      if ((strStoreKey == null) || (strStoreKey.trim().equals(""))) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message: The Key passed is not valid. Please Verify.");
      }

      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);

      if (localWebElement == null) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message: The webtable does not exist , please verify");

      }
      int k = Integer.parseInt(strColumnNum.trim());
      int m = getColCount(localWebElement, 1);
      if (k > m) {
        Automation.testReport.log(LogStatus.WARNING, "Message: Cannot access col '" + k
            + "' , table have '" + m + "' columns, please verify");

      }
      arrayOfString = getColumnData(localWebElement, k, false);
      if (arrayOfString == null) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message: Failed to retrieve the '" + k + "' column data, please verify");

      }
      System.out.println(arrayOfString);
      String str2 = FwUtil.convertToArrayString(arrayOfString);
      System.out.println(str2);
      if (FwUtil.storeData(strStoreKey, String.valueOf(str2))) {
        Automation.testReport.log(LogStatus.INFO,
            "Message:The column data " + str2 + " is stored successfully in the key " + strStoreKey
            + ". " + "The items are " + '^' + " seperated.");

      } else {
        Automation.testReport.log(LogStatus.WARNING, "Message: Failed to store the items " + str2
            + " in the key " + strStoreKey + ". Please verify.");
      }
    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.WARNING,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));

    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.WARNING, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));
    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
    return arrayOfString;

  }

  /**
   * @param localWebElement
   * @param strStoreKey
   * @param strRowNum
   * @return
   */
  public static String[] StoreRowData(WebElement localWebElement, String strStoreKey,
      String strRowNum) {
    String[] arrayOfString = null;
    try {
      if (!NumberUtil.isPostiveInteger(strRowNum)) {
        Automation.testReport.log(LogStatus.WARNING, "Message: The column '" + strRowNum
            + "' is not a valid integer, please verify. Column index starts from 1.");
      }
      if ((strStoreKey == null) || (strStoreKey.trim().equals(""))) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message: The Key passed is not valid. Please Verify.");
      }
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if (localWebElement == null) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message: The webtable does not exist , please verify");
      }
      int k = Integer.parseInt(strRowNum.trim());
      int m = getRowCount(localWebElement);
      if (k > m) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message: Cannot access row '" + k + "' , table have '" + m + "' rows, please verify");
      }
      arrayOfString = getRowData(localWebElement, k);
      if (arrayOfString == null) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message: Failed to retrieve the '" + k + "' row data, please verify");
      }
      String str2 = FwUtil.convertToArrayString(arrayOfString);
      if (FwUtil.storeData(strStoreKey, String.valueOf(str2))) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message:The row data " + str2 + " is stored successfully in the key " + strStoreKey
            + ". " + "The items are " + '^' + " seperated.");
      } else {
        Automation.testReport.log(LogStatus.WARNING, "Message: Failed to store the items " + str2
            + " in the key " + strStoreKey + ". Please verify.");
      }
    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.WARNING,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));
    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.WARNING, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));

    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
    return arrayOfString;

  }

  /**
   * @param locator
   * @param strRowNum
   * @param strColumnNum
   */
  public static void ClickCell(WebElement locator, String strRowNum, String strColumnNum) {
    try {
      CommonUtil.Wait(5);
      if ((!NumberUtil.isPostiveInteger(strRowNum))
          || (!NumberUtil.isPostiveInteger(strColumnNum))) {
        Automation.testReport.log(LogStatus.ERROR,
            "Message: Either the row '" + strRowNum + "' OR column '" + strColumnNum
            + "' is not a valid integer, please verify. Row/column index starts from 1.");
      }
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if (locator == null) {
        Automation.testReport.log(LogStatus.ERROR,
            "Message: The webtable does not exist , please verify");

      }
      int k = Integer.parseInt(strRowNum.trim());
      int m = Integer.parseInt(strColumnNum.trim());
      int n = getRowCount(locator);
      if (k > n) {
        Automation.testReport.log(LogStatus.ERROR, "Message: Cannot access row '" + strRowNum
            + "' , table have '" + n + "' rows, please verify");

      }
      int i1 = getColCount(locator, k);
      if (m > i1) {
        Automation.testReport.log(LogStatus.ERROR, "Message: Cannot access column '" + m
            + "' , table have '" + i1 + "' columns, please verify");

      }
      int i2 = locator.findElements(By.xpath("thead/tr")).size();
      WebElement localWebElement2 = getTableCell(locator, i2, k, m);
      localWebElement2.click();
      Automation.testReport.log(LogStatus.INFO, "Message: Click operation successful on row : "
          + String.valueOf(k) + ", and column :" + String.valueOf(m));

    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.ERROR,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));

    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));

    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
  }

  /**
   * @param localWebElement1
   * @param link
   * @param strRowNum
   * @param strColumnNum
   */
  public static void ClickLinkAtCellTH(WebElement localWebElement1, String link, String strRowNum,
      String strColumnNum) {
    CommonUtil.Wait(5);
    try {
      if (link == null) {
        Automation.testReport.log(LogStatus.INFO,
            "Message: The link text is either null or empty , please verify.");
      }
      if ((!NumberUtil.isPostiveInteger(strRowNum))) {
        Automation.testReport.log(LogStatus.INFO,
            "Message: Either the row '" + strRowNum + "' OR column '" + strColumnNum
            + "' is not a valid integer, please verify. Row/column index starts from 1.");
      }
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if (localWebElement1 == null) {
        Automation.testReport.log(LogStatus.ERROR,
            "Message: The webtable does not exist , please verify");
      }
      int k = Integer.parseInt(strRowNum.trim());
      int m = Integer.parseInt(strColumnNum.trim());
      int n = getRowCount(localWebElement1);
      if (k > n) {
        Automation.testReport.log(LogStatus.WARNING, "Message: Cannot access row '" + strRowNum
            + "' , table have '" + n + "' rows, please verify");
      }
      List<WebElement> localList1 = localWebElement1.findElements(By.xpath("thead/tr"));
      int i2 = localList1.size();
      int i3 = getColCount(localWebElement1, k);
      if (m > i3) {
        Automation.testReport.log(LogStatus.WARNING, "Message: Cannot access column '" + m
            + "' , table have '" + i3 + "' columns, please verify");
      }
      WebElement localWebElement2 = getTableCellTH(localWebElement1, i2, k, m);
      List<WebElement> localList2 = localWebElement2.findElements(By.tagName("a"));
      if (localList2.size() == 0) {
        Automation.testReport.log(LogStatus.WARNING, "Message: The cell (row =" + k + " , column ="
            + m + ") does not contain any link , please verify");
      }
      Iterator<WebElement> localIterator = localList2.iterator();
      WebElement localWebElement3 = localIterator.next();
      localWebElement3.click();
      /*
       * while (localIterator.hasNext()) { WebElement localWebElement3 = (WebElement)
       * localIterator.next(); if ((link.equals("")) || (link.isEmpty())) { String str2 =
       * localWebElement3.getText(); localWebElement3.click();
       * Automation.testReport.log(LogStatus.INFO,
       * "Message: Since the link passed is empty, the first link '" + str2 + "' in the cell (row ="
       * + k + " , column =" + m + ") is clicked successfully."); } String str2 =
       * localWebElement3.getText(); if (str2.contains(link)) { localWebElement3.click();
       * Automation.testReport.log(LogStatus.PASS, "Message: The link '" + link +
       * "' in the cell (row =" + k + " , column =" + m + ") is clicked successfully."); } }
       */
      Automation.testReport.log(LogStatus.WARNING, "Message: The link '" + link
          + "' is not found in the cell (row =" + k + " , column =" + m + ").");
    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.ERROR,
          "Message: The element is not found , please verify."
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));
    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));

    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
  }

  /**
   * @param paramObjectInfo
   * @param paramString
   * @param paramObject
   */
  public static void CompareColumnData(WebElement webTableLocator, String colNum,
      Object paramObject) {

    try {
      if (!NumberUtil.isPostiveInteger(colNum)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: The column '" + colNum
            + "' is not a valid integer, please verify. Column index starts from 1.");
      }

      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if ((webTableLocator == null) && (!WebObject.EXISTLOCATORVALUE)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");

      }
      if (webTableLocator == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The webtable does not exist , please verify");

      }
      int k = Integer.parseInt(colNum.trim());
      int m = getColCount(webTableLocator, 1);
      if (k > m) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Cannot access col '" + k
            + "' , table have '" + m + "' columns, please verify");

      }
      ArrayList localArrayList1 = (ArrayList) paramObject;
      String[] arrayOfString = getColumnData(webTableLocator, k, false);
      if (arrayOfString == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Failed to retrieve the '" + k + "' column data, please verify");
      }
      ArrayList localArrayList2 = (ArrayList) CommonUtil.convertArrayToList(arrayOfString);
      boolean bool = CommonUtil.compareStringListInSequence(localArrayList1, localArrayList2, false,
          true);
      if (bool) {
        Automation.testReport.log(LogStatus.PASS, "Message: Expected column data:"
            + localArrayList1.toString() + "\t, Actual row data:" + localArrayList2.toString());
      } else {
        Automation.testReport.log(LogStatus.FAIL, "Message: Expected column data:"
            + localArrayList1.toString() + "\t, Actual row data:" + localArrayList2.toString());
      }
    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.FAIL,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));
    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));
    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }

  }

  /**
   * @param webTableLoactor
   * @param storeKey
   * @param colNum
   * @param colLength
   */
  public static void StoreInstanceOfDataFromColumn(WebElement webTableLoactor, String storeKey,
      String colNum, String colLength) {
    try {
      int j;
      if ((storeKey == null) || (storeKey.trim().equals("")) || (storeKey.isEmpty())) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The key passed is not valid. Please Verify.");

      }
      if (!NumberUtil.isPostiveInteger(colNum)) {
        Automation.testReport.log(LogStatus.INFO, "Message: The column '" + colNum
            + "' is not a valid integer, please verify. Column index starts from 1.");

      }
      if ((colLength == null) || (colLength.equals("")) || (colLength.isEmpty())) {
        Automation.testReport.log(LogStatus.INFO,
            "Since the data is not given, assuming the default value as a zero length empty string");
        colLength = "";
      }
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if ((webTableLoactor == null) && (!WebObject.EXISTLOCATORVALUE)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");

      }
      if (webTableLoactor == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The webtable does not exist , please verify");
      }
      int k = Integer.parseInt(colNum.trim());
      String[] arrayOfString = getColumnData(webTableLoactor, k, true);
      if (arrayOfString == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Failed to retrieve the column '" + k + "' data, please verify");
      }
      int m = getDataInstance(arrayOfString, colLength);
      if (FwUtil.storeData(storeKey, String.valueOf(m))) {
        Automation.testReport.log(LogStatus.PASS, "Message: The instance '" + m
            + "' is stored successfully in the key '" + storeKey + "'.");

      } else {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Failed to store the instance in the key " + storeKey + "'.");
      }
    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.FAIL,
          "Status: Failed    " + "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));
    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.FAIL,
          "Status: Failed    " + "Message: An exception" + "occured:" + localException.getMessage()
          + "<br/>" + FwUtil.getStackTrace(localException));
    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
  }

  /**
   * @param webTableLoactor
   * @param ColNum
   * @param paramObject
   * @param boolValue
   */
  public static void VerifyDataExistenceInColumn(WebElement webTableLoactor, String ColNum,
      Object paramObject, String boolValue) {
    boolean bool1 = false;
    try {
      if (!NumberUtil.isPostiveInteger(ColNum)) {
        Automation.testReport.log(LogStatus.INFO, "Message: The column '" + ColNum
            + "' is not a valid integer, please verify. Column index starts from 1.");

      }
      if ((!CommonUtil.isBoolean(boolValue)) || (boolValue.equals(""))
          || (boolValue.trim().isEmpty())) {
        Automation.testReport.log(LogStatus.INFO,
            "Since a valid value for existence is not given, assuming the default value as 'TRUE'");
        bool1 = true;
      } else {
        bool1 = Boolean.parseBoolean(boolValue.trim());
      }
      ArrayList localArrayList1 = (ArrayList) paramObject;
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if ((webTableLoactor == null) && (!WebObject.EXISTLOCATORVALUE)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");
      }
      if (webTableLoactor == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The webtable does not exist , please verify");
      }
      int m = Integer.parseInt(ColNum.trim());
      int n = getColCount(webTableLoactor, 1);
      if (m > n) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Cannot access col '" + m
            + "' , table have '" + n + "' columns, please verify");

      }
      String[] arrayOfString = getColumnData(webTableLoactor, m, false);
      if (arrayOfString == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Failed to retrieve the '" + m + "' column data, please verify");

      }
      ArrayList localArrayList2 = (ArrayList) CommonUtil.convertArrayToList(arrayOfString);
      boolean bool2 = CommonUtil.isSubList(localArrayList2, localArrayList1);
      if (bool2) {
        if (bool1) {
          Automation.testReport.log(LogStatus.PASS,
              "Message: The data exist in column " + m + ". List of expected data:"
                  + localArrayList1.toString() + " , " + "List of Actual data:"
                  + localArrayList2.toString());
        } else {
          Automation.testReport.log(LogStatus.FAIL,
              "Message: The data exist in column " + m + ". List of expected data:"
                  + localArrayList1.toString() + " , " + "List of Actual data:"
                  + localArrayList2.toString());
        }
      } else if (bool1) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The data does not exist in column " + m + ". List of expected data:"
                + localArrayList1.toString() + " , " + "List of Actual data:"
                + localArrayList2.toString());
      } else {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The data does not exist in column " + m + ". List of expected data:"
                + localArrayList1.toString() + " , " + "List of Actual data:"
                + localArrayList2.toString());
      }

    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.ERROR,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));
    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));
    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
  }

  /**
   * @param webTableLocator
   * @param paramString1
   * @param rowNum
   * @param colNum
   */
  public static void ClickImageInCellTH(WebElement webTableLocator, String paramString1,
      String rowNum, String colNum) {
    try {
      int i;
      if (paramString1 == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The alt text passed is either null or empty , please verify.");
      }
      if ((!NumberUtil.isPostiveInteger(rowNum)) || (!NumberUtil.isPostiveInteger(colNum))) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Either the row '" + rowNum + "' OR column '" + colNum
            + "' is not a valid integer, please verify. Row/column index starts from 1.");
      }
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if ((webTableLocator == null) && (!WebObject.EXISTLOCATORVALUE)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");
      }
      if (webTableLocator == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The webtable does not exist , please verify");

      }
      int j = Integer.parseInt(rowNum.trim());
      int k = Integer.parseInt(colNum.trim());
      int m = getRowCount(webTableLocator);
      if (j > m) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Cannot access row '" + rowNum
            + "' , table have '" + m + "' rows, please verify");

      }
      int n = webTableLocator.findElements(By.xpath("thead/tr")).size();
      int i1 = getColCount(webTableLocator, j);
      if (k > i1) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Cannot access column '" + k
            + "' , table have '" + i1 + "' columns, please verify");
      }
      int i2 = 0;
      WebElement localWebElement3;
      int i8;
      if (j <= n) {
        Automation.testReport.log(LogStatus.INFO, "In Tbody");
        WebElement localWebElement2 = webTableLocator.findElement(
            By.xpath("thead/TR[" + String.valueOf(j) + "]/TH[" + String.valueOf(k) + "]"));
        List<WebElement> localList = localWebElement2.findElements(By.tagName("img"));
        int i3 = localList.size();
        Automation.testReport.log(LogStatus.INFO,
            "The xpath count for the img element is '" + localWebElement2 + "//img'");
        if (i3 == 0) {
          Automation.testReport.log(LogStatus.FAIL, "Message: The cell (row =" + j + " , column ="
              + k + ") does not contain any img , please verify");
        }
        String str2 = "";
        Iterator localIterator1 = localList.iterator();
        while (localIterator1.hasNext()) {
          localWebElement3 = (WebElement) localIterator1.next();
          str2 = localWebElement3.getAttribute("title");
          if (str2.equals(paramString1)) {
            localWebElement3.click();
            Automation.testReport.log(LogStatus.INFO, "The img element clicked is " + str2);
            Automation.testReport.log(LogStatus.PASS,
                "Message: The img element with the title '" + paramString1 + "' in the cell (row ="
                    + j + " , column =" + k + ") is clicked successfully.");
          }
          str2 = localWebElement3.getAttribute("alt");
          if (str2.equals(paramString1)) {
            localWebElement3.click();
            Automation.testReport.log(LogStatus.INFO, "The img element clicked is " + str2);
            Automation.testReport.log(LogStatus.PASS,
                "Message: The img element with the alt '" + paramString1 + "' in the cell (row ="
                    + j + " , column =" + k + ") is clicked successfully.");
          }
        }
        Automation.testReport.log(LogStatus.FAIL, "Message: The img element with the title/alt '"
            + paramString1 + "' is not found in the cell (row =" + j + " , column =" + k + ").");
      }
      Automation.testReport.log(LogStatus.INFO, "In Tbody");
      i2 = j - n;
      WebElement localWebElement2 = webTableLocator.findElement(
          By.xpath("tbody/TR[" + String.valueOf(i2) + "]/TH[" + String.valueOf(k) + "]"));
      Automation.testReport.log(LogStatus.INFO, "cell : " + localWebElement2);
      List localList = localWebElement2.findElements(By.tagName("img"));
      Automation.testReport.log(LogStatus.INFO,
          "The xpath count for the img element is '" + localWebElement2 + "//img'");
      int i3 = localList.size();
      if (i3 == 0) {
        Automation.testReport.log(LogStatus.FAIL, "Message: The cell (row =" + j + " , column =" + k
            + ") does not contain any img , please verify");
      }
      String str3 = "";
      Iterator localIterator2 = localList.iterator();
      while (localIterator2.hasNext()) {
        localWebElement3 = (WebElement) localIterator2.next();
        str3 = localWebElement3.getAttribute("title");
        if (str3.equals(paramString1)) {
          localWebElement3.click();
          Automation.testReport.log(LogStatus.INFO, "The img element clicked is " + str3);
          Automation.testReport.log(LogStatus.PASS,
              "Message: The img element with the title '" + paramString1 + "' in the cell (row ="
                  + j + " , column =" + k + ") is clicked successfully.");
        }
        str3 = localWebElement3.getAttribute("alt");
        if (str3.equals(paramString1)) {
          localWebElement3.click();
          Automation.testReport.log(LogStatus.INFO, "The img element clicked is " + str3);
          Automation.testReport.log(LogStatus.PASS,
              "Message: The img element with the alt '" + paramString1 + "' in the cell (row =" + j
              + " , column =" + k + ") is clicked successfully.");

        }
      }
      Automation.testReport.log(LogStatus.FAIL, "Message: The img element '" + paramString1
          + "' is not found in the cell (row =" + j + " , column =" + k + ").");

    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.FAIL,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));

    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));
    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }

  }

  /**
   * @param webTableLocator
   * @param storeKey
   * @param colNum
   * @param incrementParam
   */
  public static void GenerateAndStoreMaximumValue(WebElement webTableLocator, String storeKey,
      String colNum, String incrementParam) {
    try {
      if ((incrementParam.equals("")) || (incrementParam.trim() == "")
          || (incrementParam.isEmpty())) {
        incrementParam = "0";
        Automation.testReport.log(LogStatus.INFO,
            "Message: The data passed for Increment is '" + incrementParam
            + "' .Hence 'Increment' parameter is considered as the default value '0'");
      } else {
        try {
          Integer.parseInt(incrementParam);
        } catch (NumberFormatException localNumberFormatException1) {
          Automation.testReport.log(LogStatus.WARNING, "Message: The increment data '"
              + incrementParam + "' is not a valid numeric, so it is set to default value of 0.");
          incrementParam = "0";
        }
      }
      incrementParam = incrementParam.trim();
      int i = Integer.parseInt(incrementParam);
      int j;
      if (!NumberUtil.isPostiveInteger(colNum)) {
        Automation.testReport.log(LogStatus.WARNING, "Message: The column '" + colNum
            + "' is not a valid integer, please verify. Column index starts from 1.");

      }
      if ((storeKey == null) || (storeKey.trim().equals("")) || (storeKey.isEmpty())) {
        Automation.testReport.log(LogStatus.WARNING,
            "Message: The key passed is not valid. Please Verify.");

      }
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if ((webTableLocator == null) && (!WebObject.EXISTLOCATORVALUE)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");

      }
      if (webTableLocator == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The webtable does not exist , please verify");

      }
      int m = Integer.parseInt(colNum.trim());
      int n = 0;
      String[] arrayOfString = getColumnData(webTableLocator, m, true);
      if (arrayOfString == null) {
        n = i;
        if (FwUtil.storeData(storeKey, String.valueOf(n))) {
          Automation.testReport.log(LogStatus.PASS, "Message: Table was empty hence The data '" + n
              + "' is stored successfully in the key '" + storeKey + "'.");

        }
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Failed to store the row containing the max data in the key " + storeKey
            + "'.");

      }
      int i1 = 0;
      int i2 = 0;
      while (i2 < arrayOfString.length) {
        try {
          n = Integer.parseInt(arrayOfString[i2]);
          i1 = i2 + 1;
        } catch (NumberFormatException localNumberFormatException2) {
          i2++;
        }
      }
      if (i1 == 0) {
        n = i;
        if (FwUtil.storeData(storeKey, String.valueOf(n))) {
          Automation.testReport.log(LogStatus.PASS, "Message: Table was empty hence The data '" + n
              + "' is stored successfully in the key '" + storeKey + "'.");
          i2 = 0;
        }
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Failed to store the row containing the max data in the key " + storeKey
            + "'.");
      }
      i2 = 0;
      for (int i3 = 0; i3 < arrayOfString.length; i3++) {
        try {
          i2 = Integer.parseInt(arrayOfString[i3]);
          if (i2 > n) {
            n = i2;
            i1 = i3 + 1;
          }
        } catch (NumberFormatException localNumberFormatException3) {
        }
      }
      int i3 = n;
      n += i;
      if (FwUtil.storeData(storeKey, String.valueOf(n))) {
        Automation.testReport.log(LogStatus.PASS,
            "Message:The row '" + i1 + "' contains the max data " + i3 + ". The data '" + n
            + "' after increment by '" + i + "' is stored successfully in the key '" + storeKey
            + "'.");
      }
      Automation.testReport.log(LogStatus.FAIL,
          "Message: Failed to store the row containing the max data in the key " + storeKey + "'.");
    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.ERROR,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));
    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));

    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
  }

  /**
   * @param webTableLoactor
   * @param storeKey
   * @param paramObject
   * @param colNum
   */
  public static void StoreRowNoContainingTextInArray(WebElement webTableLoactor, String storeKey,
      Object paramObject, String colNum) {
    try {
      int j;
      if ((storeKey == null) || (storeKey.trim().equals(""))) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The Key passed is not valid. Please Verify.");

      }
      if (paramObject == null) {
        Automation.testReport.log(LogStatus.FAIL, "Message: The cell text is null. Please verify.");

      }
      if (!NumberUtil.isPostiveInteger(colNum)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: The column '" + colNum
            + "' is not a valid integer, please verify. Column index starts from 1.");

      }
      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if ((webTableLoactor == null) && (!WebObject.EXISTLOCATORVALUE)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");

      }
      if (webTableLoactor == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The webtable does not exist , please verify");

      }
      int k = getRowCount(webTableLoactor);
      if (k < 1) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The table does not have any row, please verify");

      }
      int m = Integer.parseInt(colNum.trim());
      int n = 0;
      boolean bool = false;
      ArrayList localArrayList = (ArrayList) paramObject;
      Automation.testReport.log(LogStatus.INFO, "Array size = " + localArrayList.size());
      String[] arrayOfString = getColumnData(webTableLoactor, m, true);
      for (int i1 = 0; i1 < arrayOfString.length; i1++) {
        String str2 = arrayOfString[i1];
        for (int i2 = 0; i2 < localArrayList.size(); i2++) {
          Automation.testReport.log(LogStatus.INFO,
              "Row no = " + i1 + "Data =" + localArrayList.get(i2).toString());
          if (StringUtil.stringContains(str2, localArrayList.get(i2).toString(), true, true)) {
            bool = true;
            Automation.testReport.log(LogStatus.INFO,
                "in IF Row no = " + i1 + "Data =" + localArrayList.get(i2).toString());
            Automation.testReport.log(LogStatus.INFO, "cellData = " + str2);
            Automation.testReport.log(LogStatus.INFO, "found = " + bool);
          } else {
            bool = false;
            break;
          }
        }
        if (!bool) {
          continue;
        }
        n = i1 + 1;
        break;
      }
      if (bool) {
        if (FwUtil.storeData(storeKey, String.valueOf(n))) {
          Automation.testReport.log(LogStatus.PASS,
              "Message: Row no '" + n + "' is stored successfully in the key '" + storeKey + "'.");
        } else {
          Automation.testReport.log(LogStatus.FAIL,
              "Message: Failed to store the row no for the cell text '" + paramObject + "'.");
        }
      } else {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Failed to store the row no, the cell data '" + paramObject
            + " ' is not present in column '" + m + "'. Please verify.");
      }

    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.ERROR,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));

    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));

    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
  }

  public static void VerifyDataExistenceInRow(WebElement webTableLocator, String colNum,
      Object paramObject, String boolValue) {

    boolean bool1 = false;
    try {
      if (!NumberUtil.isPostiveInteger(colNum)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: The column '" + colNum
            + "' is not a valid integer, please verify. Column index starts from 1.");

      }
      if ((!CommonUtil.isBoolean(boolValue)) || (boolValue.equals(""))
          || (boolValue.trim().isEmpty())) {
        Automation.testReport.log(LogStatus.INFO,
            "Since a valid value for existence is not given, assuming the default value as 'TRUE'");
        bool1 = true;
      } else {
        bool1 = Boolean.parseBoolean(boolValue.trim());
      }

      Automation.driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if ((webTableLocator == null) && (!WebObject.EXISTLOCATORVALUE)) {
        Automation.testReport.log(LogStatus.FAIL, "Message: Failed to retrieve the locator value.");

      }
      if (webTableLocator == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: The webtable does not exist , please verify");
      }
      int k = Integer.parseInt(colNum.trim());
      int m = getRowCount(webTableLocator);
      if (k > m) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Cannot access row '" + k + "' , table have '" + m + "' rows, please verify");

      }
      String[] arrayOfString = getRowData(webTableLocator, k);
      if (arrayOfString == null) {
        Automation.testReport.log(LogStatus.FAIL,
            "Message: Failed to retrieve the '" + k + "' row data, please verify");

      }
      ArrayList localArrayList1 = (ArrayList) CommonUtil.convertArrayToList(arrayOfString);
      ArrayList localArrayList2 = (ArrayList) paramObject;
      boolean bool2 = CommonUtil.isSubList(localArrayList1, localArrayList2);
      if (bool2) {
        if (bool1) {
          Automation.testReport.log(LogStatus.PASS,
              "Message: The data exist in row " + k + ". List of expected data:"
                  + localArrayList2.toString() + " , " + "List of Actual data:"
                  + localArrayList1.toString());

        } else {
          Automation.testReport.log(LogStatus.FAIL,
              "Message: The data exist in row " + k + ". List of expected data:"
                  + localArrayList2.toString() + " , " + "List of Actual data:"
                  + localArrayList1.toString());

        }
      } else if (bool1) {
        Automation.testReport.log(LogStatus.PASS,
            "Message: The data does not exist in row " + k + ". List of expected data:"
                + localArrayList2.toString() + " , " + "List of Actual data:"
                + localArrayList1.toString());

      } else {
        Automation.testReport.log(LogStatus.PASS,
            "Message: The data does not exist in row " + k + ". List of expected data:"
                + localArrayList2.toString() + " , " + "List of Actual data:"
                + localArrayList1.toString());

      }

    } catch (NoSuchElementException localNoSuchElementException) {
      Automation.testReport.log(LogStatus.ERROR,
          "Message: The element is not found , please verify. "
              + localNoSuchElementException.getMessage() + "<br/>"
              + FwUtil.getStackTrace(localNoSuchElementException));

    } catch (Exception localException) {
      Automation.testReport.log(LogStatus.ERROR, "Message: An exception" + "occured:"
          + localException.getMessage() + "<br/>" + FwUtil.getStackTrace(localException));

    } finally {
      Automation.setImplicitWait = false;
      Automation.waitForObjectToExist = true;
    }
  }

  public void SetTextinCellEdit(WebElement tableLocator, String romNum, String colNum,
      String text) {

    try {
      if ((text == null) || (text.trim().equals("")) || (text.isEmpty())) {
        testReport.log(LogStatus.WARNING,
            "Since a valid value for text is not given, assuming the default value as zero length empty string");
        text = "";
      }
      if ((!NumberUtil.isPostiveInteger(romNum)) || (!NumberUtil.isPostiveInteger(colNum))) {
        testReport.log(LogStatus.FAIL, "Status: Failed    "
            + "Message: Either the row '" + romNum + "' OR column '" + colNum
            + "' is not a valid integer, please verify. Row/column index starts from 1.");

      }
      driver.manage().timeouts().implicitlyWait(0L, TimeUnit.SECONDS);
      if ((tableLocator == null) && (!WebObject.EXISTLOCATORVALUE)) {
        testReport.log(LogStatus.FAIL, "Status: Failed    "
            + "Message: Failed to retrieve the locator value.");

      }
      if (tableLocator == null) {
        testReport.log(LogStatus.FAIL, "Status: Failed    "
            + "Message: The webtable does not exist , please verify");

      }
      int j = Integer.parseInt(romNum.trim());
      int k = Integer.parseInt(colNum.trim());
      int m = getRowCount(tableLocator);
      if (j > m) {
        testReport.log(LogStatus.FAIL,"Status: Failed    " + "Message: Cannot access row '"
            + romNum + "' , table have '" + m + "' rows, please verify");

      }
      List localList1 = tableLocator.findElements(By.xpath("thead/tr"));
      int i1 = localList1.size();
      int i2 = getColCount(tableLocator, j);
      if (k > i2) {
        testReport.log(LogStatus.FAIL, "Status: Failed    " + "Message: Cannot access column '"
            + k + "' , table have '" + i2 + "' columns, please verify");

      }
      int i3 = 0;
      if (j <= i1) {
        tableLocator = tableLocator.findElement(
            By.xpath("thead/tr[" + String.valueOf(j) + "]/th[" + String.valueOf(k) + "]"));
      } else {
        i3 = j - i1;
        tableLocator = tableLocator.findElement(
            By.xpath("tbody/tr[" + String.valueOf(i3) + "]/td[" + String.valueOf(k) + "]"));
      }
      List localList2 = tableLocator.findElements(By.tagName("input"));
      if (localList2.size() == 0) {
        testReport.log(LogStatus.FAIL, "Message: The cell (row =" + j
            + " , column =" + k + ") does not contain any webedit , please verify");

      }
      Iterator localIterator = localList2.iterator();
      while (localIterator.hasNext()) {
        WebElement localWebElement2 = (WebElement) localIterator.next();
        if ((localWebElement2.getAttribute("type").equalsIgnoreCase("text"))
            || (localWebElement2.getAttribute("type").equalsIgnoreCase("password"))) {
          if (localWebElement2.isEnabled()) {
              localWebElement2.clear();
              localWebElement2.sendKeys(new CharSequence[] { text });

              testReport.log(LogStatus.PASS, "Message: The value '"
                    + text + "' is set successfully in the webedit in the cell (row =" + j
                    + " , column =" + k + ")");

          }
          testReport.log(LogStatus.FAIL,"Message: The edit box is not enabled.Unable to set the value in the edit box.");
        }
      }
      testReport.log(LogStatus.FAIL, "Message: The cell (row =" + j
          + " , column =" + k + ") does not contain any webedit , please verify");

    } catch (NoSuchElementException localNoSuchElementException) {
      testReport.log(LogStatus.FAIL, "Message: The element is not found , please verify. "
          + localNoSuchElementException.getMessage() + "<br/>"
          + FwUtil.getStackTrace(localNoSuchElementException));

    } catch (Exception localException1) {
      testReport.log(LogStatus.FAIL,"Message: An exception" + "occured:"
          + localException1.getMessage() + "<br/>" + FwUtil.getStackTrace(localException1));

    } finally {
      setImplicitWait = false;
      waitForObjectToExist = true;
    }
  }
}
