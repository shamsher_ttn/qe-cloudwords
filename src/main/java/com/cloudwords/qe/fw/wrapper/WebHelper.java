/**
 *
 */

package com.cloudwords.qe.fw.wrapper;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.openqa.selenium.JavascriptExecutor;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.utillib.FwUtil;

/**
 * @author Shamsher Singh
 * @date 05-Aug-2016
 */
public class WebHelper {

    private static final Logger logger = Logger.getLogger(WebHelper.class);


  /** This Functions Waits for the Ajax Controls To Load on the page **/
  public static Boolean waitFroAjax() throws InterruptedException, IOException {
    Boolean ajaxIsComplete = false;
    try {

      while (true) // Handle timeout somewhere
      {

        ajaxIsComplete = (Boolean) ((JavascriptExecutor) Automation.driver)
            .executeScript("return window.jQuery != undefined && jQuery.active == 0");
        if (ajaxIsComplete) {
          break;
        }
        Thread.sleep(100);
      }
    } catch (Exception e) {
      logger.error("Ajax controls Loading : Stackterace <br>" + FwUtil.getStackTrace(e));
    }
    return ajaxIsComplete;
  }
}
