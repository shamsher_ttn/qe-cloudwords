/**
 * 
 */
package com.cloudwords.qe.fw.utillib;

import java.util.Date;

import org.apache.log4j.FileAppender;

public class CustomFileAppender extends FileAppender {

  public CustomFileAppender() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.apache.log4j.FileAppender#setFile(java.lang.String)
   */
  @Override
  public void setFile(String file) {
    super.setFile(prependDate(file));
  }

  /**
   * @param filename
   * @return
   */
  private static String prependDate(String filename) {
    Date d = new Date();
    String date = d.toString().replaceAll(" ", "_");
    date = date.replaceAll(":", "_");
    date = date.replaceAll("\\+", "_");
    return filename + "_" + date + ".log";
  }
}