package com.cloudwords.qe.fw.utillib;

import java.util.Hashtable;

import com.cloudwords.qe.fw.libraries.PublicVariables;


public class TestUtil {
  public static boolean isExecutable(String testName, XLReader xls) {
    for (int rowNum = 2; rowNum <= xls
        .getRowCount(PublicVariables.TEST_CASE_SHEET_NAME); rowNum++) {
      if (xls.getCellData(PublicVariables.TEST_CASE_SHEET_NAME,
          PublicVariables.TEST_CASE_SHEET_1ST_COLUMN, rowNum).equals(testName)) {
        if (xls
            .getCellData(PublicVariables.TEST_CASE_SHEET_NAME,
                PublicVariables.TEST_CASES_RUNMODE_COLUMN, rowNum)
            .equals(PublicVariables.RUNMODE_YESVALUE))
          return true;
        else
          return false;
      }
    }
    return false;
  }

  public static Object[][] getData(String testName, XLReader xls) {
    int testStartRowNum = 0;
    for (int rNum = 1; rNum <= xls
        .getRowCount(PublicVariables.TEST_CASES_DATA_SHEET_NAME); rNum++) {
      if (xls.getCellData(PublicVariables.TEST_CASES_DATA_SHEET_NAME, 0, rNum).equals(testName)) {
        testStartRowNum = rNum;
        break;
      }
    }
    int colStartRowNum = testStartRowNum + 1;
    int totalCols = 0;
    while (!xls.getCellData(PublicVariables.TEST_CASES_DATA_SHEET_NAME, totalCols, colStartRowNum)
        .equals("")) {
      totalCols++;
    }
    int dataStartRowNum = testStartRowNum + 2;
    int totalRows = 0;
    while (!xls
        .getCellData(PublicVariables.TEST_CASES_DATA_SHEET_NAME, 0, dataStartRowNum + totalRows)
        .equals("")) {
      totalRows++;
    }
    Object[][] data = new Object[totalRows][1];
    int index = 0;
    Hashtable<String, String> table = null;
    for (int rNum = dataStartRowNum; rNum < (dataStartRowNum + totalRows); rNum++) {
      table = new Hashtable<String, String>();
      for (int cNum = 0; cNum < totalCols; cNum++) {
        table.put(xls.getCellData(PublicVariables.TEST_CASES_DATA_SHEET_NAME, cNum, colStartRowNum),
            xls.getCellData(PublicVariables.TEST_CASES_DATA_SHEET_NAME, cNum, rNum));
        System.out.print(
            xls.getCellData(PublicVariables.TEST_CASES_DATA_SHEET_NAME, cNum, rNum) + " -- ");
      }
      data[index][0] = table;
      index++;
    }
    return data;
  }
}
