/**
 *
 */
package com.cloudwords.qe.fw.utillib;

import java.io.File;
import java.util.Date;

import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;

/**
 * @author Shamsher Singh
 * @date 12-Aug-2016
 */
public class ExtentManager {

  private static ExtentReports extent;

  public static ExtentReports getInstance() {

    if (extent == null) {
      Date date = new Date();
      String fileName = date.toString().replace(":", "_").replace(" ", "_") + ".html";
      String reportPath = System.getProperty("user.dir") + PublicVariables.SEP
          + PublicVariables.REPORTPATH + PublicVariables.SEP + fileName;
      extent = new ExtentReports(reportPath, true, DisplayOrder.NEWEST_FIRST);

      extent.loadConfig(new File(System.getProperty("user.dir") + PublicVariables.SEP
          + "src/test/resources" + PublicVariables.SEP + "ReportsConfig.xml"));
      // optional
      extent.addSystemInfo("Selenium Version", "2.53.0").addSystemInfo("Environment", "QA");
    }
    return extent;
  }

}
