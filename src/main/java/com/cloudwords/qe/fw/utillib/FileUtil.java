/**
 *
 */
package com.cloudwords.qe.fw.utillib;

import java.io.File;
import java.util.ArrayList;

import com.cloudwords.qe.fw.libraries.PublicVariables;


/**
 * @author shamsher
 * @date 10-Sep-2016
 */
public class FileUtil {

  /**
   * Count of the files under a directory
   *
   * @param filePath
   */
  public static int getFilesCount(String filePath) {
    File f = new File(filePath);
    int count = 0;
    for (File file : f.listFiles()) {
      if (file.isFile()) {
        count++;
      }
    }
    return count;
  }

  /**
   * List all the files and folders from a directory
   *
   * @param directoryName
   *          to be listed
   */
  public void listFilesAndFolders(String directoryName) {
    File directory = new File(directoryName);
    // get all the files from a directory
    File[] fList = directory.listFiles();
    for (File file : fList) {
      System.out.println(file.getName());
    }
  }

  /**
   * List all the files under a directory
   *
   * @param directoryName
   *          to be listed
   */
  public void listFiles(String directoryName) {
    File directory = new File(directoryName);
    // get all the files from a directory
    File[] fList = directory.listFiles();
    for (File file : fList) {
      if (file.isFile()) {
        System.out.println(file.getName());
      }
    }
  }

  /**
   * List all the folder under a directory
   *
   * @param directoryName
   *          to be listed
   */
  public void listFolders(String directoryName) {
    File directory = new File(directoryName);
    // get all the files from a directory
    File[] fList = directory.listFiles();
    for (File file : fList) {
      if (file.isDirectory()) {
        System.out.println(file.getName());
      }
    }
  }

  /**
   * List all files from a directory and its subdirectories
   *
   * @param directoryName
   *          to be listed
   */
  public static ArrayList<String> listFilesAndFilesSubDirectories(String directoryName) {
    File directory = new File(directoryName);
    // get all the files from a directory
    File[] fList = directory.listFiles();
    ArrayList<String> list = new ArrayList<String>();
    for (File file : fList) {
      if (file.isFile()) {
        list.add(file.getAbsolutePath());
        System.out.println(file.getAbsolutePath());
      } else if (file.isDirectory()) {
        listFilesAndFilesSubDirectories(file.getAbsolutePath());
      }
    }
    return list;
  }

  /* Get the latest file from a specific directory */
  public static File getLatestFilefromDir(String dirPath) {
    File dir = new File(dirPath);
    File[] files = dir.listFiles();
    if (files == null || files.length == 0) {
      return null;
    }

    File lastModifiedFile = files[0];
    for (int i = 1; i < files.length; i++) {
      if (lastModifiedFile.lastModified() < files[i].lastModified()) {
        lastModifiedFile = files[i];
      }
    }
    return lastModifiedFile;
  }

  public static void CopyandMoveFile(String copyLoc, String StoreKey) {

    File file = FileUtil.getLatestFilefromDir(PublicVariables.DOWNLOAFFILEPATH);
    String filePath = file.getAbsolutePath();
    try {
      File afile = new File(filePath);
      System.out.println(afile.getName());
      System.out.println("New location is  : " + copyLoc);
      // String filename = StoreKey+"_"+afile.getName();
      String filename = afile.getName();
      filename = filename.substring(0, filename.indexOf("."));
      filename = filename + "_" + StoreKey;
      System.out.println(filename);
      if (afile.renameTo(new File(copyLoc + PublicVariables.SEP + filename))) {
        System.out.println("File is moved successfully!");
      } else {
        System.out.println("Failed to move the File!");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static boolean deleteDirectory(File dir) {
    if (dir.isDirectory()) {
      File[] children = dir.listFiles();
      for (int i = 0; i < children.length; i++) {
        boolean success = deleteDirectory(children[i]);
        if (!success) {
          return false;
        }
      }
    } // either file or an empty directory
    System.out.println("removing file or directory : " + dir.getName());
    return dir.delete();
  }


}
