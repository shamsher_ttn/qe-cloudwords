/**
 *
 */
package com.cloudwords.qe.fw.dbqueries;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.cloudwords.qe.fw.dbconnection.DBConnection;

/**
 * @author shamsher
 * @date 20-Oct-2016
 */
public class IntegrationQueries {

  static DBConnection db = new DBConnection();

  public static boolean verifyEloquaAccess(String email) throws SQLException {

    ResultSet result = db.executeQuery(
        "select has_permission from user_permission where permission ='ELOQUA_ACCESS' AND user_id in( select id from users where email ='"+email+"')");
    result.first();
    return result.getInt("has_permission") == 1 ? true : false;
  }

  public static String getUserFirstName(String email) throws SQLException{

    ResultSet result = db
        .executeQuery("select first_name from users where email='" + email + "'");
    result.first();
    return (result.getString("first_name"));

  }

  public static String getUserLastName(String email) throws SQLException{


    ResultSet result = db
        .executeQuery("select last_name from users where email='" + email + "'");
    result.first();
    return (result.getString("last_name"));

  }

  public static boolean verifyMarketoAccess(String email) throws SQLException {

    ResultSet result = db.executeQuery(
        "select has_permission from user_permission where permission ='MARKETO_ACCESS' AND user_id in( select id from users where email ='"+email+"')");
    result.first();
    return result.getInt("has_permission") == 1 ? true : false;
  }


  public static boolean verifyHubSpotAccess(String email) throws SQLException {
	  ResultSet result = db.executeQuery( "select has_permission from user_permission where permission ='HUBSPOT_ACCESS' AND user_id in( select id from users where email ='"+email+"')");
    result.first();
    return result.getInt("has_permission") == 1 ? true : false;
  }

}
