/**
 *
 */
package com.cloudwords.qe.fw.dbqueries;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.cloudwords.qe.fw.dbconnection.DBConnection;


/**
 * @author shamsher
 * @date 09-Sep-2016
 */
public class ProjectQueries {


	static DBConnection db = new DBConnection();

	public static int fileUploadCount(String projectName,String language) throws SQLException{

		ResultSet result = db
				.executeQuery("select id from project where name ='"+projectName+"' order by created_date desc");
		result.first();
		int count=Integer.parseInt(result.getString("id"));
		ResultSet result1 =db.executeQuery("select count(id) from project_asset where language='"+language+"' and project_id='"+count+"'");
		result1.first();
		return Integer.parseInt(result1.getString("count(id)"));
	}

}
