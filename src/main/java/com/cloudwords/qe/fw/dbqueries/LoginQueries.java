/**
 *
 */
package com.cloudwords.qe.fw.dbqueries;

import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.cloudwords.qe.fw.dbconnection.DBConnection;


/**
 * @author Shamsher Singh
 * @date 25-Aug-2016
 */
public class LoginQueries extends DBConnection {


  /**
   * @param string
   * @throws SQLException
   */
  public static void disableUserLogin(String userName) throws SQLException {
    DBConnection.getConnection();
    String sql = "update users Set enabled = '0' where email=?";
    PreparedStatement statement = conn.prepareStatement(sql);
    statement.setString(1, userName);
    statement.executeUpdate();
    conn.close();
  }

  /**
   * @param string
   * @throws SQLException
   */
  public static void enableUserLogin(String userName) throws SQLException {

    DBConnection.getConnection();
    String sql = "update users Set enabled = '1' where email=?";
    PreparedStatement statement = conn.prepareStatement(sql);
    statement.setString(1, userName);
    statement.executeUpdate();
    conn.close();
  }

  /**
   * @param string
   * @throws SQLException
   */
  public static void setFirstLogin(String userName) throws SQLException {

    DBConnection.getConnection();
    String sql = "update users Set locked = '1' where email=?";
    PreparedStatement statement = conn.prepareStatement(sql);
    statement.setString(1, userName);
    statement.executeUpdate();
    conn.close();
  }

  /**
   * @param string
   * @throws SQLException
   */
  public static void disableFirstLogin(String userName) throws SQLException {
    DBConnection.getConnection();
    String sql = "update users Set locked = '1' where email=?";
    PreparedStatement statement = conn.prepareStatement(sql);
    statement.setString(1, userName);
    statement.executeUpdate();
    conn.close();

  }


}
