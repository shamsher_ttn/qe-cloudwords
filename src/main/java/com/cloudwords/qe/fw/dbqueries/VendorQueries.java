/**
 *
 */
package com.cloudwords.qe.fw.dbqueries;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.cloudwords.qe.fw.dbconnection.DBConnection;


/**
 * @author Shamsher Singh
 * @date 30-Aug-2016
 */
public class VendorQueries extends DBConnection {

  static DBConnection db = new DBConnection();

  public static int verifyAutomateVendor(String vendorName) throws SQLException {

    ResultSet result = db.executeQuery("select is_automated from vendor where name='"+vendorName+ "'");
    result.first();
    return (result.getInt("is_automated"));
  }

  public static void activateVendor(String email) throws IllegalAccessException, ClassNotFoundException{

    try {
      db.execUpdate("update users Set enabled = '1' where email='"+email+"'");
    } catch (InstantiationException | SQLException | IOException e) {
      e.printStackTrace();
    }
  }

  public static void deactivateVendor(String email) throws IllegalAccessException, ClassNotFoundException{
    try {
      db.execUpdate("update users Set enabled = '0' where email='"+email+"'");
    } catch (InstantiationException | SQLException | IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  public static int getVendorID(String vendorName) throws SQLException {

    ResultSet result = db.executeQuery(" select id from vendor where name='"+ vendorName +"' order by created_date desc");
    result.first();
    return (result.getInt("id"));
  }

}
