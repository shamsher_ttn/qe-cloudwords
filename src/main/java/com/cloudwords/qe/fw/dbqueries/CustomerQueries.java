/**
 *
 */
package com.cloudwords.qe.fw.dbqueries;

import java.sql.ResultSet;
import java.sql.SQLException;

import com.cloudwords.qe.fw.dbconnection.DBConnection;


/**
 * @author Shamsher Singh
 * @date 30-Aug-2016
 */
public class CustomerQueries {

  static DBConnection db = new DBConnection();

  public static boolean verifyCustomer(String custName) throws SQLException {

    ResultSet result = db
        .executeQuery("select count(1) from users where name='" + custName + "'" );
    result.first();
    return result.getInt("count(1)") > 0 ? true : false;  }


  public static boolean verifyUser(String email) throws SQLException {

    ResultSet result = db
        .executeQuery("select count(1) from users where email='" + email + "'");
    result.first();
    return result.getInt("count(1)") > 0 ? true : false;
  }

}
