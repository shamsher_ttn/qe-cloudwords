/**
 *
 */
package com.cloudwords.qe.fw.hubspot;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

/**
 * @author shamsher
 * @date 21-Dec-2016
 */
public class HubSpotToken {

	public static void main(String[] args) throws IOException{
		String body = bodyBuilder();
		String endpoint1 = "https://app.hubspot.com/login-api/v1/login";
		URL url1 = new URL(endpoint1);
		System.out.println(url1);
		HttpsURLConnection urlConn1 = (HttpsURLConnection) url1.openConnection();
		urlConn1.setRequestMethod("POST");
		urlConn1.setRequestProperty("Content-Type", "application/json");
		urlConn1.setDoOutput(true);
		DataOutputStream wr1 = new DataOutputStream(urlConn1.getOutputStream());
		wr1.writeBytes(body);
		wr1.flush();
		wr1.close();

		int responseCode1 = urlConn1.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url1);
		// System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode1);

		BufferedReader in1 = new BufferedReader(new InputStreamReader(urlConn1.getInputStream()));
		String inputLine1;
		StringBuffer response1 = new StringBuffer();

		while ((inputLine1 = in1.readLine()) != null) {
			response1.append(inputLine1);
		}
		in1.close();

		// print result
		System.out.println(response1.toString());


		String endpoint = "https://app.hubspot.com/login-verify?early=true";
		URL url = new URL(endpoint);
		System.out.println(url);
		HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();
		urlConn.setRequestMethod("POST");
		//urlConn.setRequestProperty("accept", "text/json");
		urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		urlConn.setRequestProperty("csrfToken","auifiwZWAbH6IYfGsyorKQ");
		//urlConn.setRequestProperty("Authorization", "Bearer " + getToken());
		// Send post request
		urlConn.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(urlConn.getOutputStream());
		String urlParameters = "portalId=1661426";
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();
		int responseCode = urlConn.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		// System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		System.out.println(response.toString());

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());

	}

	public static String bodyBuilder(){
		StringBuilder body = new StringBuilder();
		body.append("{\"email\":\"dev-integrate@cloudwords.com\",\"password\":\"4Ys0yvlsnW\",\"rememberLogin\":false,\"loginRedirectUrl\":\"https://app.hubspot.com/oauth/authorize?client_id=9d080543-99ca-4bb6-b4c9-6a4b2130a11b&scope=contacts%20automation&redirect_uri=https://www.hubspot.com\"}");

		return body.toString();
	}

	//"email":"dev-integrate@cloudwords.com","password":"4Ys0yvlsnW","rememberLogin":false,"loginRedirectUrl":"https://app.hubspot.com/oauth/authorize?client_id=9d080543-99ca-4bb6-b4c9-6a4b2130a11b&scope=contacts%20automation&redirect_uri=https://www.hubspot.com"
}
