
/**
 *
 */
package com.cloudwords.qe.fw.dbconnection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.log4j.Logger;

import com.cloudwords.qe.fw.base.Automation;


/**
 * @author Shamsher Singh
 * @date 13-Aug-2016
 */

public class DBConnection {

  private static String driver = Automation.configHashMap.get("DB_DRIVER");
  protected static Connection conn;
  private static String DB_URL = Automation.configHashMap.get("DB_URL");
  private static String DB_USERNAME = Automation.configHashMap.get("DB_USERNAME");
  private static String DB_PASSWORD = Automation.configHashMap.get("DB_PASSWORD");
  private static final Logger logger = Logger.getLogger(DBConnection.class);
  private static String DB_NAME = Automation.configHashMap.get("DB_NAME");

  /**
   * @return
   */
  public static java.sql.Connection getConnection() {
    try {
      Class.forName(driver).newInstance();
      conn = DriverManager.getConnection(DB_URL + DB_NAME, DB_USERNAME, DB_PASSWORD);
      if (conn != null) {
        logger.info("Database connection established successfully");
      }
    } catch (Exception e) {
      logger.error(e);
    }
    return conn;
  }

  public ResultSet executeQuery(String query)
  {
    ResultSet rs = null;
    try
    {
      PreparedStatement ps = getConnection().prepareStatement(query);
      rs = ps.executeQuery(query);
    }
    catch (Exception ex)
    {
     logger.error("Exception occurred while executing query: \n" + ex);
    }
    return rs;
  }

  public void execUpdate(String query) throws InstantiationException, IllegalAccessException, ClassNotFoundException, SQLException, IOException
  {

        Statement stmt = getConnection().createStatement();
        stmt.executeUpdate(query);
  }
}
