/**
 *
 */
package com.cloudwords.qe.fw.apputil;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.testng.Assert;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.ChangePwdQueries;
import com.cloudwords.qe.fw.pageobjects.ChangePassword;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.relevantcodes.extentreports.LogStatus;


/**
 * @author shamsher
 * @date 05-Oct-2016
 */
public class ChangePwdUtil extends Automation {

  private static final Logger logger = Logger.getLogger(LoginUtil.class);

  public static void ChangePassword(String oldPwd, String newPwd, String email, String FirstName,
      String LastName) {

    String URL = null;
    try {
      URL = configHashMap.get("BASEURL") + "/unauth/verify-newuser.htm?id=" + generateURl(email);
      System.out.println(URL);
      driver.get(URL);
      ChangePassword changepwd = new ChangePassword(driver, testReport);
      changepwd.enterOldPwd(oldPwd);
      changepwd.enterNewPwd(newPwd);
      changepwd.enterConfirmPwd(newPwd);
      changepwd.clickSubmitBtn();
      CommonUtil.Wait(5);
      if (driver.findElement(By.xpath("//*[@id='nav-button-container']/div/a[1]")).getText()
          .equalsIgnoreCase(FirstName + " " + LastName)) {
        Assert.assertTrue(true);
      } else {
        Assert.assertFalse(false);
      }
    } catch (SQLException e) {
      testReport.log(LogStatus.ERROR, e);
      e.printStackTrace();
    }
  }

  public static void ChangeVendorPassword(String oldPwd, String newPwd, String email, String FirstName,
	      String LastName) {

	    String URL = null;
	    try {
	      URL = configHashMap.get("BASEURL") + "/unauth/verify-newuser.htm?id=" + generateURl(email);
	      System.out.println(URL);
	      driver.get(URL);
	      ChangePassword changepwd = new ChangePassword(driver, testReport);
	     // changepwd.enterOldPwd(oldPwd);
	      changepwd.enterNewPwd(newPwd);
	      changepwd.enterConfirmPwd(newPwd);
	      changepwd.clickSubmitBtn();
	      CommonUtil.Wait(5);
	      if (driver.findElement(By.xpath("//*[@id='nav-button-container']/div/a[1]")).getText()
	          .equalsIgnoreCase(FirstName + " " + LastName)) {
	        Assert.assertTrue(true);
	      } else {
	        Assert.assertFalse(false);
	      }
	    } catch (SQLException e) {
	      testReport.log(LogStatus.ERROR, e);
	      e.printStackTrace();
	    }
	  }

  public static String generateURl(String email) throws SQLException {

    int randomid = ChangePwdQueries.getRandom(email);
    int userId = ChangePwdQueries.getUserId(email);
    String str1 = Integer.toHexString(randomid);
    String str2 = Integer.toHexString(userId);
    String url = str1 + "I" + str2;
    return url;
  }

}
