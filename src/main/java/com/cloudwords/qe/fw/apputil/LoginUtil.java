/**
 *
 */
package com.cloudwords.qe.fw.apputil;


import org.apache.log4j.Logger;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.pageobjects.LandingPage;
import com.cloudwords.qe.fw.pageobjects.LoginPage;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.wrapper.General;
import com.cloudwords.qe.fw.wrapper.WebObject;
import com.relevantcodes.extentreports.LogStatus;


/**
 * @author Shamsher Singh
 * @date 25-Aug-2016
 */
public class LoginUtil extends Automation {

  private static final Logger logger = Logger.getLogger(LoginUtil.class);


  public static  void loginWithCredentials(String strUsrName, String strPassword) {
    LoginPage login = new LoginPage(driver, testReport);
    logger.info("Selenium Driver wait for 3 seconds");
    General.RefreshPage();
    login.enetrUserName(strUsrName);
    login.enterPassWord(strPassword);
    login.clickLoginBtn();

  }

  public static Object doLogin(String strUsrName, String strPassword) {
    loginWithCredentials(strUsrName, strPassword);
   CommonUtil.Wait(5);
    boolean loginSuccess = WebObject.isElementPresent("//*[@id='create-new-button']");

    if (loginSuccess) {
      Automation.testReport.log(LogStatus.INFO, "Login Success");
      LandingPage landingPage = new LandingPage(driver, Automation.testReport);
      return landingPage;
    } else {
      Automation.testReport.log(LogStatus.INFO, "Login Not Success");
      LoginPage loginPage = new LoginPage(driver, Automation.testReport);
      return loginPage;
    }
  }

  public static void logout() {
  /*  TopMenu top = new TopMenu(driver, Automation.testReport);
    top.clickContainer();
    top.clickLogoutLink();
    logger.info("Clicked on log out link..");*/
    driver.get(Automation.configHashMap.get("BASEURL")+"/logout.htm");
    General.DeleteCookiesOnCurrentDomain();
    // provide assertion below
    s_assert.assertTrue(driver.getCurrentUrl().contains("/login"));
  }

  public static void assertLoginFail() throws InterruptedException {

    LoginPage login = new LoginPage(driver, Automation.testReport);
    s_assert.assertTrue(login.isPasswordHidden(), "Password is not masked..");
    login.clickLoginBtn();
    s_assert.assertTrue(login.isAlertDisp(), "False login. Alert must be displayed...");
    CommonUtil.Wait(10);
    s_assert.assertTrue(driver.getCurrentUrl().contains("j_spring_security_check"), "User logged in withdisable user");
    logger.info("Successfully asserted false login...");
  }




  public static void assertLoginSuccess(String Value) {

  //  TopMenu topPane = new TopMenu(driver, Automation.testReport);

    switch (Value) {
    case "/cust":
      s_assert.assertTrue( driver.getCurrentUrl().contains("/cust"), "User is landed on customer users page");
      break;
    case "/lsp":
      s_assert.assertTrue(driver.getCurrentUrl().contains("/lsp"),
          "User is landed on vendor user page");
      break;
    }
   /* s_assert.assertEquals(topPane.userFullName(),
        UserDetails.user_first_name + " " + UserDetails.user_last_name);*/
  }

}
