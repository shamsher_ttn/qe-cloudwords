package com.cloudwords.qe.fw.tm;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;

import com.cloudwords.qe.fw.base.Automation;

import br.eti.kinoshita.testlinkjavaapi.TestLinkAPI;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionStatus;
import br.eti.kinoshita.testlinkjavaapi.constants.ExecutionType;
import br.eti.kinoshita.testlinkjavaapi.constants.TestCaseDetails;
import br.eti.kinoshita.testlinkjavaapi.model.Build;
import br.eti.kinoshita.testlinkjavaapi.model.TestCase;
import br.eti.kinoshita.testlinkjavaapi.util.TestLinkAPIException;

public class TestlinkIntegration {
	private final static String url = "http://testlink.stage.cloudwords.com/testlink/lib/api/xmlrpc/v1/xmlrpc.php";
	private final static String devKey = "7c57d2b17efd489db2e7dabed6bdf9ea";

	public TestlinkIntegration() {

	}

	public ArrayList<Integer> getTestcaseByTestplanId(int testPlanID)
			throws TestLinkAPIException, MalformedURLException {
		TestLinkAPI testlinkAPIClient = new TestLinkAPI(new URL(url), devKey);
		TestCase[] testcases = testlinkAPIClient.getTestCasesForTestPlan(testPlanID, null, null, null, null, null, null,
				null, ExecutionType.AUTOMATED, true, TestCaseDetails.FULL);
		ArrayList<Integer> testcaseNames = new ArrayList<Integer>();
		for (TestCase testCase : testcases) {
			TestCase finalTestcase = testlinkAPIClient.getTestCase(testCase.getId(), null, null);
			testcaseNames.add(finalTestcase.getId());
		}
		return testcaseNames;
	}

	public Integer getTestcaseIDByName(String testcaseName, String projectName)
			throws TestLinkAPIException, MalformedURLException {
		TestLinkAPI testlinkAPIClient = new TestLinkAPI(new URL(url), devKey);
		return testlinkAPIClient.getTestCaseIDByName(testcaseName, null, projectName, null);
	}

	public void setResult(String testcaseId, ExecutionStatus status)
			throws TestLinkAPIException, MalformedURLException {
		Date date = new Date();
		String buildName = null;
		String dateStamp = date.toString().replace(":", "_").replace(" ", "_");
		TestLinkAPI testlinkAPIClient = new TestLinkAPI(new URL(url), devKey);
		testlinkAPIClient.createBuild(Integer.parseInt(Automation.configHashMap.get("PLAN_ID")),
				Automation.configHashMap.get("TESTLINK_BUILDNAME") + "" + dateStamp, "Nightly Build");
		Build[] build = testlinkAPIClient
				.getBuildsForTestPlan(Integer.parseInt(Automation.configHashMap.get("PLAN_ID")));
		for (int i = 0; i < build.length; i++) {
			if (build[i].getName()
					.equalsIgnoreCase(Automation.configHashMap.get("TESTLINK_BUILDNAME") + "" + dateStamp)) {
				buildName = build[i].getName();
				break;
			}
		}
		testlinkAPIClient.setTestCaseExecutionResult(Integer.parseInt(testcaseId), null,
				Integer.parseInt(Automation.configHashMap.get("PLAN_ID")), status, null, buildName, null, true, null,
				null, null, Automation.TESTLINKMAP, false);
	}

}
