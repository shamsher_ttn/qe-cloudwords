/**
 *
 */
package com.cloudwords.qe.fw.eloqua;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * @author shamsher
 * @date 05-Dec-2016
 */
public class EloquaCreateEmail {

	//public static void main(String[] args) {
	public void CreateTestEmail(String EmailName ){

		String testData="This means that in a standard Vendor-Customer step, the review task will be visible "
				+ "to both silos. The review task will be closed by explicit user action after all files have been approved."
				+ " Files can be unapproved.Users may request revisions on one or more files. Revisions requests may include "
				+ "(optional) comments and a due date. Revision requests are not a task, they are new entity in Cloudwords. As"
				+ " long as at least one revision request is open for a language, a REVISE_FILES task will exist, assigned to the "
				+ "assignee of the work phase. At minimium, a revision request consists of a comment, a set of files, a due date, "
				+ "and a status (OPEN or CLOSED). The request is associated with a review task. The request is closed automatically "
				+ "when a revision for each file has been uploaded. The revision request will be shown in the list of comments for "
				+ "each file in the request, as a special comment.";
		try {
			URL url = new URL("https://secure.p03.eloqua.com/API/REST/1.0/assets/email");
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Authorization", "Basic VGVjaG5vbG9neVBhcnRuZXJDbG91ZHdvcmRzXFNoYW1zaGVyLnNpbmdoOlJhZ2hhdjEyMyQ=");

			String input = "{\"name\": \""+EmailName+"\",\"emailFooterId\": 1,\"emailHeaderId\": 1, \"encodingId\": 1, \"emailGroupId\": 1, \"subject\": \"Test subject line\", \"htmlContent\": {\"type\": \"RawHtmlContent\",\"html\": \"<html><head></head><body>"+testData+"</body></html>\"} }";

			OutputStream os = conn.getOutputStream();
			os.write(input.getBytes());
			os.flush();
			if (conn.getResponseCode() != HttpURLConnection.HTTP_CREATED) {
				throw new RuntimeException("Failed : HTTP error code : "
						+ conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader(
					(conn.getInputStream())));
			String output;
			System.out.println("Output from Server .... \n");
			while ((output = br.readLine()) != null) {
				System.out.println(output);
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
