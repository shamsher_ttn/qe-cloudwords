package com.cloudwords.qe.fw.pm;

import javax.naming.AuthenticationException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.cloudwords.qe.fw.base.Automation;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientHandlerException;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.core.util.Base64;

/**
 * @author Shamsher Singh
 * @date 14-Aug-2016
 */
public class JiraHandler {

  private static String JIRA_BASE_URL = Automation.configHashMap.get("JIRA_BASE_URL");

  /**
   * @param Project
   * @param Summary
   * @return
   */
  public static String createIssue(String Project, String Summary) {

    String auth = new String(Base64.encode(Automation.configHashMap.get("JIRA_USERNAME") + ":"
        + Automation.configHashMap.get("JIRA_PASSWORD")));
    String newKey = null;
    try {
      // Get Projects
      String projects = invokeGetMethod(auth, JIRA_BASE_URL + "/rest/api/2/project");
      System.out.println(projects);
      JSONArray projectArray = new JSONArray(projects);
      for (int i = 0; i < projectArray.length(); i++) {
        JSONObject proj = projectArray.getJSONObject(i);
        System.out.println("Key:" + proj.getString("key") + ", Name:" + proj.getString("name"));
      }

      // Create Issue
      String createIssueData = "{\"fields\":{\"project\":{\"key\":\"TST\"},\"summary\":\"REST Test\",\"issuetype\":{\"name\":\"Bug\"}}}";
      String issue = invokePostMethod(auth, JIRA_BASE_URL + "/rest/api/2/issue", createIssueData);
      System.out.println(issue);
      JSONObject issueObj = new JSONObject(issue);
      newKey = issueObj.getString("key");
      System.out.println("Key:" + newKey);

    } catch (AuthenticationException e) {
      System.out.println("Username or Password wrong!");
      e.printStackTrace();
    } catch (ClientHandlerException e) {
      System.out.println("Error invoking REST method");
      e.printStackTrace();
    } catch (JSONException e) {
      System.out.println("Invalid JSON output");
      e.printStackTrace();
    }
    return newKey;
  }

  private static String invokeGetMethod(String auth, String url)
      throws AuthenticationException, ClientHandlerException {
    Client client = Client.create();
    WebResource webResource = client.resource(url);
    ClientResponse response = webResource.header("Authorization", "Basic " + auth)
        .type("application/json").accept("application/json").get(ClientResponse.class);
    int statusCode = response.getStatus();
    if (statusCode == 401) {
      throw new AuthenticationException("Invalid Username or Password");
    }
    return response.getEntity(String.class);
  }

  private static String invokePostMethod(String auth, String url, String data)
      throws AuthenticationException, ClientHandlerException {
    Client client = Client.create();
    WebResource webResource = client.resource(url);
    ClientResponse response = webResource.header("Authorization", "Basic " + auth)
        .type("application/json").accept("application/json").post(ClientResponse.class, data);
    int statusCode = response.getStatus();
    if (statusCode == 401) {
      throw new AuthenticationException("Invalid Username or Password");
    }
    return response.getEntity(String.class);
  }

}
