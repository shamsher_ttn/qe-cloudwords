/**
 *
 */
package com.cloudwords.qe.fw.marketoapi;



import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.libraries.TextFileHandler;

public class JSONReadFromFile {

  public static int ValueID(String JSONResult) {
     JSONParser parser = new JSONParser();

    final String filePath = System.getProperty("user.dir")+PublicVariables.SEP+"temp/temp.json";
    String idValue = "";
    try {

      // read the json file
     TextFileHandler.WriteFile(JSONResult, filePath);
      FileReader reader = new FileReader(filePath);
      JSONParser jsonParser = new JSONParser();
      JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
      JSONArray result= (JSONArray) jsonObject.get("result");
      System.out.println(result);
      for(int i=0; i<result.size(); i++){
        System.out.println("The " + i + " element of the array: "+result.get(i));
      }
      Iterator i = result.iterator();
      while (i.hasNext()) {
        JSONObject innerObj = (JSONObject) i.next();
        System.out.println("id "+ innerObj.get("id") );
        idValue=innerObj.get("id").toString();
        System.out.println(idValue);
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
    return  Integer.parseInt(idValue);
  }

  public static String getValue(String JSONResult , String key) {
    JSONParser parser = new JSONParser();

   final String filePath = System.getProperty("user.dir")+PublicVariables.SEP+"temp/temp.json";
   String idValue = "";
   try {

     // read the json file
    TextFileHandler.WriteFile(JSONResult, filePath);
     FileReader reader = new FileReader(filePath);
     JSONParser jsonParser = new JSONParser();
     JSONObject jsonObject = (JSONObject) jsonParser.parse(reader);
     JSONArray result= (JSONArray) jsonObject.get("urls");
     System.out.println(result);
     for(int i=0; i<result.size(); i++){
       System.out.println("The " + i + " element of the array: "+result.get(i));
     }
     Iterator i = result.iterator();
     while (i.hasNext()) {
       JSONObject innerObj = (JSONObject) i.next();
       System.out.println("id "+ innerObj.get(key) );
       idValue=innerObj.get(key).toString();
       System.out.println(idValue);
     }
   } catch (Exception e) {
     e.printStackTrace();
   }
   return idValue ;
 }

}
