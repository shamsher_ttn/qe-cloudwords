/**
 *
 */
package com.cloudwords.qe.fw.marketoapi;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author shamsher
 * @date 26-Nov-2016
 */
public class MarketoDataDeletion {

  private static final Logger logger = Logger.getLogger(MarketoDataDeletion.class);

  private WebDriver driver;

  /**
   *
   */
  public MarketoDataDeletion(WebDriver driver, ExtentTest test) {

    this.driver = driver;
    PageFactory.initElements(driver, this);
  }


  public void MarketoTestDataDelete(String campaignName,String programName ) throws InterruptedException {

    try{
      int programFolderID = GetProgramIdByName.GetProgramId(programName);
      DeleteProgram.DeleteProgFolder(programFolderID);
      int campaignFolderID= GetFolderIdByName.GetFolderId(campaignName);
      DeleteFolder.DeleteCampFolder(campaignFolderID);
    }catch(Exception e){
      logger.error(e);
    }
  }

  public static void main(String[] args){
  //  int programFolderID = GetProgramIdByName.GetProgramId("CloneForTesting");
   // DeleteProgram.DeleteProgFolder(programFolderID);
    int campaignFolderID= GetFolderIdByName.GetFolderId("TestClone");
    DeleteFolder.DeleteCampFolder(campaignFolderID);
  }
}
