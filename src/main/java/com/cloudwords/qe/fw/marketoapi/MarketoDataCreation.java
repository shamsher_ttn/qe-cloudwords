/**
 *
 */
package com.cloudwords.qe.fw.marketoapi;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author shamsher
 * @date 25-Nov-2016
 */
public class MarketoDataCreation {

  private static final Logger logger = Logger.getLogger(MarketoDataDeletion.class);


  private WebDriver driver;

  /**
   *
   */
  public MarketoDataCreation(WebDriver driver, ExtentTest test) {

    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  public void MarketoTestDataCreation(String campaignfolder,String progamName,String emailName, String programType, String programChannel) throws InterruptedException {

    try{
      logger.info("Creating data in marketo for automatin test ");
    int workspaceID=59931;
    CreateCampaignFolder.CreateFolder(workspaceID,campaignfolder);
    Thread.sleep(10000L);
    int campaignFolderID= GetFolderIdByName.GetFolderId(campaignfolder);
    Thread.sleep(10000L);
    System.out.println(campaignFolderID);
   CreateProgramsFolder.CreateProgram(campaignFolderID, progamName, programType, programChannel, "Program created for automation testing purpose");
    Thread.sleep(10000L);
    int programFolderID = GetProgramIdByName.GetProgramId(progamName);
    System.out.println(programFolderID);
    CloneEmail.CloneEmailTest(emailName, programFolderID);
    }catch(Exception e){
      logger.error(e);
    }
  }

  public static void main(String[] args) throws InterruptedException{

    int campaignFolderID= GetFolderIdByName.GetFolderId("TestProgram");
    Thread.sleep(10000L);
    System.out.println("TestProgram folder ID :"+campaignFolderID);
    CreateProgramsFolder.CreateProgram(campaignFolderID, "Test1234","Default", "Email Blast", "Program created for automation testing purpose");
    Thread.sleep(10000L);
  }

}
