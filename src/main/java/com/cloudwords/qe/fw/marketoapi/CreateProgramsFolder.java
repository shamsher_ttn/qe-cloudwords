/*
   CreatePrograms.java

   Marketo REST API Sample Code
   Copyright (C) 2016 Marketo, Inc.

   This software may be modified and distributed under the terms
   of the MIT license.  See the LICENSE file for details.
*/
package com.cloudwords.qe.fw.marketoapi;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import com.eclipsesource.json.JsonArray;
import com.eclipsesource.json.JsonObject;

public class CreateProgramsFolder {
	public String marketoInstance = "https://529-IDT-654.mktorest.com";
	public String marketoIdURL = marketoInstance + "/identity";
	public String clientId = "1c213e4b-8597-49d2-b03f-0178e9687d18";
	public String clientSecret = "EMtr0YyfRfZZ6TD7kM48CNr8iBLTOgoA";
	public String idEndpoint = marketoIdURL + "/oauth/token?grant_type=client_credentials&client_id=" + clientId
			+ "&client_secret=" + clientSecret;
	public String name; // name of new Program
	public JsonObject folder; // a JSON input, with 2 required members, id and
								// type
	public String description; // Description for the folder, up to 2,000
								// characters
	public String type;// program type, "Default", "Event", or "Event with
						// Webinar"
	public String channel;// channel of program, must be available in Channels
							// list of instance
	public JsonArray tags;// optional list of tags, embedded as a JSON Array of
							// JSON objects with members tagType and tagValue;
	public JsonArray costs;// embedded as a JSON Array of JSON objects with
							// members startDate, cost, and note, optional

	public static void CreateProgram(int campaignFolderId, String programName, String progamType, String programChannel,
			String progDesc) {
		CreateProgramsFolder program = new CreateProgramsFolder();
		program.name = programName;
		program.folder = new JsonObject().add("id", campaignFolderId).add("type", "Folder");
		program.description = progDesc;
		program.type = progamType;
		program.channel = programChannel;
		String result = null;
		try {
			result = program.sendPost(campaignFolderId,programName, progamType, programChannel,progDesc);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} // program.postData();
		System.out.println(result);
	}
	public String getToken() {
		String token = null;
		try {
			URL url = new URL(idEndpoint);
			HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();
			urlConn.setRequestMethod("GET");
			urlConn.setRequestProperty("accept", "application/json");
			int responseCode = urlConn.getResponseCode();
			System.out.println(idEndpoint);
			if (responseCode == 200) {
				InputStream inStream = urlConn.getInputStream();
				Reader reader = new InputStreamReader(inStream);
				JsonObject jsonObject = JsonObject.readFrom(reader);
				System.out.println(jsonObject);
				token = jsonObject.get("access_token").asString();
			} else {
				throw new IOException("Status: " + responseCode);
			}
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return token;
	}

	private String sendPost(int campaignFolderId,String programName, String progamType, String programChannel,String progDesc) throws Exception {
		String endpoint = marketoInstance + "/rest/asset/v1/programs.json";
		URL url = new URL(endpoint);
		System.out.println(url);
		HttpsURLConnection urlConn = (HttpsURLConnection) url.openConnection();
		urlConn.setRequestMethod("POST");
		urlConn.setRequestProperty("accept", "text/json");
		urlConn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
		urlConn.setRequestProperty("Authorization", "Bearer " + getToken());
		// Send post request
		urlConn.setDoOutput(true);
		DataOutputStream wr = new DataOutputStream(urlConn.getOutputStream());
		String urlParameters = "name=" + programName
				+ "&folder={\"id\":"+campaignFolderId+",\"type\":\"Folder\"}&description="+ progDesc+"&type="+ progamType + "&channel=" + programChannel;
		wr.writeBytes(urlParameters);
		wr.flush();
		wr.close();

		int responseCode = urlConn.getResponseCode();
		System.out.println("\nSending 'POST' request to URL : " + url);
		// System.out.println("Post parameters : " + urlParameters);
		System.out.println("Response Code : " + responseCode);

		BufferedReader in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();

		// print result
		System.out.println(response.toString());
		return response.toString();

	}
}