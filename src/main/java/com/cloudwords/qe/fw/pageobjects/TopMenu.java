/**
 * 
 */
package com.cloudwords.qe.fw.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author Shamsher Singh
 * @date 17-Aug-2016
 */
public class TopMenu extends LoadableComponent<TopMenu> {

  private WebDriver driver;
  
  @FindBy(how = How.ID, using = "tab_home")
  private WebElement tabHome;

  @FindBy(how = How.ID, using = "tab_projects")
  private WebElement tabProjects;

  @FindBy(how = How.ID, using = "tab_workspaces")
  private WebElement tabCampaigns;

  @FindBy(how = How.ID, using = "tab_vendors")
  private WebElement tabVendors;

  @FindBy(how = How.ID, using = "tab_reference")
  private WebElement tabReferenc;

  @FindBy(how = How.ID, using = "tab_reports")
  private WebElement tabReports;
  
  @FindBy(how=How.ID,using="nav-button-arrow")
  private WebElement btnContainer;
  
  @FindBy(how=How.ID,using="logout")
  private WebElement logoutLink;
  
  @FindBy(how=How.XPATH,using="//*[@id='nav-button-container']/div/a[1]")
  private WebElement eleUserName;


  
  public TopMenu(WebDriver driver, ExtentTest testReport) {
    this.driver = driver;
    PageFactory.initElements(driver, this);

  }
  
  public void clickContainer(){
    btnContainer.click();
  }
  
  public void clickLogoutLink() {
    logoutLink.click();
  }
  
  public String userFullName(){
    return eleUserName.getText();
  }

  /*
   * (non-Javadoc)
   * 
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   * 
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub

  }

}
