/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.cloudwords.qe.fw.wrapper.WebDriverFactory;


/**
 * @author shamsher
 * @date 22-Nov-2016
 */
public class HubSpotPage extends LoadableComponent<HubSpotPage>{


  private WebDriverFactory driver;
  private static final Logger logger = Logger.getLogger(HubSpotPage.class);



  /* (non-Javadoc)
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub

  }


}
