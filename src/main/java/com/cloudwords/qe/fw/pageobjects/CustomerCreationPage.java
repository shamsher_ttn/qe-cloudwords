/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.wrapper.WebList;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author Shamsher Singh
 * @date 27-Aug-2016
 */
public class CustomerCreationPage extends LoadableComponent<CustomerCreationPage> {

	private WebDriver driver;

	public CustomerCreationPage(WebDriver driver, ExtentTest testReport) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(how = How.ID, using = "customer.name")
	private WebElement customerName;

	@FindBy(how = How.ID, using = "customerUser.email")
	private WebElement customerUsrEmail;

	@FindBy(how = How.ID, using = "customerUser.firstName")
	private WebElement customerfirstName;

	@FindBy(how = How.ID, using = "customerUser.lastName")
	private WebElement customerlastName;

	@FindBy(how = How.ID, using = "customerUser.password")
	private WebElement customerPwd;

	@FindBy(how = How.ID, using = "customer.brand")
	private WebElement customerbrandList;

	@FindBy(how = How.ID, using = "customer.edition")
	private WebElement customerEditionList;

	@FindBy(how = How.XPATH, using = "//*[@id='create-save-button-bottom']/span")
	private WebElement submitBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='cutomerProvisioner']/div[5]/div/a[2]")
	private WebElement cancelBtn;

	@FindBy(how = How.XPATH, using = "html/body/div[1]/div[4]/div/div[1]/div[2]/a[2]/span")
	private WebElement custEditLink;

	@FindBy(how = How.ID, using = "name")
	private WebElement custNameEdit;

	@FindBy(how = How.ID, using = "brand")
	private WebElement custBrandEdit;

	@FindBy(how = How.ID, using = "edition")
	private WebElement custNameEdition;

	@FindBy(how = How.XPATH, using = "//*[@id='contentTypeSupportedForDerivedXLIFF']/span/input")
	private WebElement contentTypeSupportedForDerivedXLIFF;

	@FindBy(how = How.ID, using = "numberOfBidOutsAllowed")
	private WebElement ProjectsPurchased;

	@FindBy(how = How.ID, using = "maxFileUploadSizeGb")
	private WebElement maxFileUploadSizeGb;

	@FindBy(how = How.ID, using = "accountStatus")
	private WebElement accountStatus;

	@FindBy(how = How.ID, using = "cloudwordsCommission")
	private WebElement cloudwordsCommission;

	@FindBy(how = How.ID, using = "subscriptionExpirationDate")
	private WebElement subscriptionExpirationDate;

	@FindBy(how = How.ID, using = "stripeDefaultLimit")
	private WebElement stripeDefaultLimit;

	@FindBy(how = How.ID, using = "stripeOverrideLimit")
	private WebElement stripeOverrideLimit;

	@FindBy(how = How.ID, using = "stripeNewLimit")
	private WebElement stripeNewLimit;

	@FindBy(how = How.ID, using = "poForcedByAdmin1")
	private WebElement poForcedByAdmin1ChkBox;

	@FindBy(how = How.ID, using = "enforcesUserLimitCheckbox")
	private WebElement userLimitEnforced;

	@FindBy(how = How.ID, using = "isBidOutBlockedByFreeDomain1")
	private WebElement isBidOutBlockedByFreeDomain;

	@FindBy(how = How.XPATH, using = "//*[starts-with(@id,'ugb_')]")
	private List<WebElement> bidlessProjectVendor;

	@FindBy(how = How.ID, using = "intendedUseMgmtCheckbox")
	private WebElement allCheckbox;

	@FindBy(how = How.XPATH, using = "//*[@id='create-customer-button-bottom']")
	private WebElement custsaveBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='activated-list']")
	private WebElement activeCustList;

	// *[@id='activated-list']

	public static enum EditionTypeEnum {
		basic, professional, enterprise, free;
	};

	public void createCustomer(String strCustName, String strEmail, String strCustFirstName, String strCustLastName,
			String strPassword, String strBrandInex, String strEditionIndex, String strAccountStatusIndex,
			String strVendorId) throws InterruptedException {
		Thread.sleep(5000L);
		customerName.sendKeys(strCustName);
		WebList.SelectItemByVisibleText(customerbrandList, strBrandInex);
		WebList.SelectItemByVisibleText(customerEditionList, strEditionIndex);
		customerfirstName.sendKeys(strCustFirstName);
		customerlastName.sendKeys(strCustLastName);
		customerUsrEmail.sendKeys(strEmail);
		customerPwd.sendKeys(strPassword);
		custsaveBtn.click();
		editCustomer(strCustName, strAccountStatusIndex, strVendorId);
	}

	public void editCustomer(String strCustName, String strAccountStatusIndex, String strVendorId) {
		driver.findElement(By.xpath("//*[@id='contentTabsContainer']/li[1]/a/span")).click();
		driver.findElement(By.xpath("//*[@id='bodyMain']/div[4]/div/div[2]/div/a[1]")).click();
		int rowCount = WebTable.getRowCount(activeCustList);
		for (int i = 1; i <= rowCount; i++) {
			String custName = driver.findElement(By.xpath("//*[@id='activated-list']/tbody/tr[" + i + "]/td[3]/a/span"))
					.getText();
			if (custName.equalsIgnoreCase(strCustName)) {
				driver.findElement(By.xpath("//*[@id='activated-list']/tbody/tr[" + i + "]/td[3]/a/span")).click();
				break;
			}
		}
		custEditLink.click();
		String strEdition = custNameEdition.getAttribute("value").toLowerCase();
		EditionTypeEnum editionType = EditionTypeEnum.valueOf(strEdition);
		switch (editionType) {
		case basic:
			break;
		case professional:

			break;
		case enterprise:
			try {
				String part1 = "html/body/div[1]/div[4]/div/form/div[3]/div/div[1]/label[";
				String part2 = "]/input[1]";
				for (int i = 1; i <= 75; i++) {
					WebElement intendedchkbox = driver.findElement(By.xpath(part1 + i + part2));
					if (intendedchkbox.isEnabled()) {
						if (!intendedchkbox.isSelected())
							intendedchkbox.click();
					}
				}
				accountStatus.click();
				WebList.SelectItemByIndex(accountStatus, strAccountStatusIndex);
				String storeText = null;

/*				driver.findElement(By.xpath("//*[starts-with(@id,'ug_')]"))
				.sendKeys(storeText + "," + Automation.STOREHASHMAP.get(strVendorId));*/
				String storevendorID = null;
				String[] venodorCount = strVendorId.split(",");
				for (int i = 0; i < venodorCount.length; i++) {
					System.out.println(venodorCount[i]);
					storeText =driver.findElement(By.xpath("//*[starts-with(@id,'ug_')]")).getAttribute("value");
					driver.findElement(By.xpath("//*[starts-with(@id,'ug_')]")).clear();
					driver.findElement(By.xpath("//*[starts-with(@id,'ug_')]"))
					.sendKeys(storeText + "," + Automation.STOREHASHMAP.get(venodorCount[i]));

					storevendorID=driver.findElement(By.xpath("//*[starts-with(@id,'ugb_')]")).getAttribute("value");

					driver.findElement(By.xpath("//*[starts-with(@id,'ugb_')]")).clear();
					driver.findElement(By.xpath("//*[starts-with(@id,'ugb_')]"))
					.sendKeys(storevendorID + "," + Automation.STOREHASHMAP.get(venodorCount[i]));

				}
				/*
				 * driver.findElement(By.xpath("//*[starts-with(@id,'ugb_')]"))
				 * .clear();
				 * driver.findElement(By.xpath("//*[starts-with(@id,'ugb_')]"))
				 * .sendKeys(storevendorID+","+Automation.STOREHASHMAP.get(
				 * strVendorId)); Thread.sleep(5000L);
				 */
				// WaitTool.elementToBeClickable(submitBtn, "Xpath");
				driver.findElement(By.xpath("html/body/div[1]/div[4]/div/form/div[5]/div/a[1]/span")).click();
				CommonUtil.Wait(5);
			} catch (Exception e) {
				System.out.println(e);
				Automation.testReport.log(LogStatus.ERROR,
						"The object is not visible and hence cannot be un-check, please verify.");
			}
			break;
		case free:
			break;
		}
	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.openqa.selenium.support.ui.LoadableComponent#load()
	 */
	@Override
	protected void load() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
	 */
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub

	}

}
