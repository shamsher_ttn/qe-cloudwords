/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.wrapper.WebButton;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author shamsher
 * @date 07-Nov-2016
 */
public class MarketoPage extends LoadableComponent<MarketoPage> {

  private final WebDriver driver;
  private static final Logger logger = Logger.getLogger(MarketoPage.class);

  /**
   *
   */
  public MarketoPage(WebDriver driver, ExtentTest testReport) {
    this.driver = driver;
    PageFactory.initElements(driver, this);

  }

  @FindBy(how = How.XPATH, using = "//*[@id='ext-comp-1022']/a")
  private WebElement adminLink;

  @FindBy(how = How.XPATH, using = "//*[@id='extdd-24']")
  private WebElement workspacepartitionLink;

  @FindBy(how = How.XPATH, using = "html/body/div[2]/div/div/div/div/div/div[2]/div/div/div/div/div/div[2]/div/div/div[1]/div[2]/div/div/div/div[1]/div[1]/table/tbody/tr/td[1]/table/tbody/tr/td[1]/table/tbody/tr[2]/td[2]/em/button")
  private WebElement newworkspaceLink;

  @FindBy(how = How.XPATH, using = "//*[@id='zoneName']")
  private WebElement workspaceName;

  @FindBy(how = How.XPATH, using = "//*[@id='zoneDescription']")
  private WebElement workspaceDesc;

  @FindBy(how = How.XPATH, using = "//*[@id='zoneAllPartitions']")
  private WebElement allleadPartition;

  @FindBy(how = How.XPATH, using = "//*[@id='ext-comp-1134']")
  private WebElement altleadPartition;

  @FindBy(how = How.XPATH, using = "//*[@id='ext-comp-1135']")
  private WebElement defaultPartition;

  @FindBy(how = How.XPATH, using = "//*[@id='primaryPartition']")
  private WebElement primaryPartition;

  @FindBy(how = How.XPATH, using = "//*[@id='language']")
  private WebElement language;

  @FindBy(how = How.XPATH, using = "//button[text()='Create']")
  private WebElement createWorkspaceBtn;

  @FindBy(how = How.XPATH, using = "//*[@id='ext-gen325']")
  private WebElement cancelBtn;

  @FindBy(how = How.XPATH, using = "//*[@id='ext-gen6']")
  private WebElement mymarketoLink;

  @FindBy(how = How.XPATH, using = "html/body/div[8]/div[2]/div/div[2]/div[2]/div[2]/a/span")
  private WebElement marketingactivitiesLink;

  @FindBy(how = How.XPATH, using = "html/body/div[2]/div/div/div/div/div/div[1]/div/div/div/div/div/div[2]/div/div[1]/div/table/tbody/tr/td[1]/table/tbody/tr/td/div/input")
  private WebElement textSearch;

  @FindBy(how = How.XPATH, using = "html/body/div[2]/div/div/div/div/div/div[2]/div/div/div/div/div/div[2]/div/div/div[1]/div[2]/div/div/div/div[1]/div[1]/table/tbody/tr/td[1]/table/tbody/tr/td/table/tbody/tr[2]/td[2]/em/button")
  private WebElement newBtn;

  // new campagian

  @FindBy(how = How.XPATH, using = "//*[@id='ext-comp-1072']")
  private WebElement newCampaignLink;

  @FindBy(how = How.XPATH, using = "//*[@id='text']")
  private WebElement campaignName;

  @FindBy(how = How.XPATH, using = "//*[@id='desc']")
  private WebElement campaignDesc;

  @FindBy(how = How.XPATH, using = "//button[text()='Create']")
  private WebElement createCampaignBtn;

  @FindBy(how = How.XPATH, using = "//button[text()='Cancel']")
  private WebElement cancelbtn;

  @FindBy(how = How.XPATH, using = "html/body/div[11]/ul/li[2]/a/span")
  private WebElement smartCampaignLink;

  @FindBy(how = How.XPATH, using = "//*[@id='ext4-ext-gen1407']")
  private WebElement campaignfolderSelectionLink;

  @FindBy(how = How.XPATH, using = "//*[@id='textfield-1172-inputEl']")
  private WebElement smartCampaignName;

  @FindBy(how = How.XPATH, using = "//*[@id='boundlist-1180-listEl']/ul/li")
  private WebElement folderList;

  @FindBy(how = How.XPATH, using = "//*[@id='textareafield-1173-inputEl']")
  private WebElement smartCampaignDesc;

  @FindBy(how = How.XPATH, using = "//*[@id='button-1176-btnEl']")
  private WebElement smartCampaignCreateBtn;

  @FindBy(how = How.XPATH, using = "//*[@id='button-1175-btnEl']")
  private WebElement smartCampaignCancelBtn;

  @FindBy(how = How.XPATH, using = "//span[text()='New Program']")
  private WebElement newProgramlink;

  @FindBy(how = How.XPATH, using = "html/body/div[22]/div[2]/div[1]/div/div/div/div[2]/div[1]/div/input")
  private WebElement folderSelect;

  @FindBy(how = How.XPATH, using = "//*[@id='ext-gen943']/div")
  private WebElement folderlist;

  @FindBy(how = How.XPATH, using = "html/body/div[22]/div[2]/div[1]/div/div/div/div[3]/div[1]/input")
  private WebElement progName;

  @FindBy(how = How.XPATH, using = "//*[@id='ext-gen935']")
  private WebElement programtypedropdown;

  @FindBy(how = How.XPATH, using = "//*[@id='loginUsername']")
  private WebElement loginUsername;

  @FindBy(how = How.XPATH, using = "//*[@id='loginPassword']")
  private WebElement loginPassword;

  @FindBy(how = How.XPATH, using = "//*[@id='loginButton']")
  private WebElement loginButton;

  @FindBy(how = How.XPATH, using = "//span[text()='New Local Asset']")
  private WebElement newlocalassestLink;

  @FindBy(how = How.XPATH, using = "//span[text()='Form']")
  private WebElement formLink;

  @FindBy(how = How.XPATH, using = "//span[text()='Create']")
  private WebElement localassestcreationLink;

  /*
   * (non-Javadoc)
   *
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub

  }

  public void MarketoLogin() {
    loginUsername.sendKeys("shamsher.singh@cloudwords.com");
    loginPassword.sendKeys("Raghav123$");
    loginButton.click();
  }

  public void CreationWorkspace(String WorkspaceName, String WorkspaceLang)
      throws InterruptedException {
    try {

      adminLink.click();
      workspacepartitionLink.click();
      newworkspaceLink.click();
      workspaceName.sendKeys(WorkspaceName);
      workspaceDesc.sendKeys("Test");
      allleadPartition.click();
      primaryPartition.sendKeys("Default");
      primaryPartition.sendKeys(Keys.ENTER);
      language.click();
      Thread.sleep(5000L);
      String locator = "//*[@class='x-combo-list-item'][text()='" + WorkspaceLang + "']";
      driver.findElement(By.xpath(locator)).click();
      createWorkspaceBtn.click();

    } catch (InterruptedException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (StaleElementReferenceException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (NoSuchElementException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (Exception e) {
      System.out.println(e);
      Assert.assertTrue(false);
    }

  }

  public void CreationCampaignFolder(CharSequence WorkspaceName, CharSequence CampaignName,
      CharSequence CampaignDesc) throws InterruptedException {
    try {
      mymarketoLink.click();
      Thread.sleep(5000L);
      marketingactivitiesLink.click();
      Thread.sleep(5000L);
      textSearch.clear();
      textSearch.sendKeys(WorkspaceName);
      String locator = "//span[text()='" + WorkspaceName + "']";
      driver.findElement(By.xpath(locator)).click();
      newBtn.click();
      newCampaignLink.click();
      Thread.sleep(5000L);
      campaignName.sendKeys(CampaignName);
      campaignDesc.sendKeys(CampaignDesc);
      createCampaignBtn.click();
     CommonUtil.Wait(5);
    } catch (InterruptedException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (StaleElementReferenceException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (NoSuchElementException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (Exception e) {
      System.out.println(e);
      Assert.assertTrue(false);
    }
  }

  public void CreationNewProgram(CharSequence CampaignName, CharSequence ProgramName,
      String ProgramType, String Channel) {
    try {

      Thread.sleep(5000L);
      textSearch.clear();
      textSearch.sendKeys(CampaignName);
      String folderLocator = "//span[text()='" + CampaignName + "']";
      driver.findElement(By.xpath(folderLocator)).click();
      newBtn.click();
      newProgramlink.click();
      List<WebElement> list = driver.findElements(By
          .xpath("//*[@src='/images/spacer.gif'] [@class='x-form-trigger x-form-arrow-trigger']"));
      list.get(0).click();
      String CampaignlistLocator = "//*[text()='" + CampaignName + "'][@class='x-combo-list-item']";
      driver.findElement(By.xpath(CampaignlistLocator)).click();
      Thread.sleep(5000);
      List<WebElement> l = driver.findElements(
          By.xpath("//input[@class='x-form-text x-form-field x-form-invalid'][@size='20']"));
      l.get(0).sendKeys(ProgramName);
      list.get(1).click();
      List<WebElement> itemList = driver.findElements(By.xpath("//*[@class='x-combo-list-item']"));
      for (int i = 0; i < itemList.size(); i++) {
        if (itemList.get(i).getAttribute("innerText").equalsIgnoreCase("Default")) {
          break;
        } else if (itemList.get(i).getAttribute("innerText").equalsIgnoreCase(ProgramType)) {
          itemList.get(i).click();
          break;
        }
      }
      list.get(2).click();
      String channelLocator = "//*[text()='" + Channel + "']";
      driver.findElement(By.xpath(channelLocator)).click();
      createCampaignBtn.click();
    } catch (StaleElementReferenceException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (NoSuchElementException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (Exception e) {
      System.out.println(e);
      Assert.assertTrue(false);
    }
  }

  public void CreationForm(String programName, CharSequence FormName) {
    try {
      CommonUtil.Wait(5);
      textSearch.clear();
      textSearch.sendKeys(programName);
      String programNameFolderLocator = "//span[text()='" + programName + "']";
      driver.findElement(By.xpath(programNameFolderLocator)).click();
      newBtn.click();
      newlocalassestLink.click();
      CommonUtil.Wait(5);
      formLink.click();
      CommonUtil.Wait(5);
      driver.findElement(By.name("name")).sendKeys(FormName);
      List<WebElement> checkboxList = driver
          .findElements(By.xpath("//input[@class='x4-form-field x4-form-checkbox']"));
      checkboxList.get(1).click();
      localassestcreationLink.click();

    } catch (StaleElementReferenceException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (NoSuchElementException e) {
      System.out.println(e);
      Assert.assertTrue(false);
    } catch (Exception e) {
      System.out.println(e);
      Assert.assertTrue(false);
    }

  }

  public void DeleteProgramFolder(CharSequence programName, CharSequence CampaignFolderName)
      throws InterruptedException {
    try {
      mymarketoLink.click();
      Thread.sleep(5000L);
      marketingactivitiesLink.click();
      Thread.sleep(5000L);
      textSearch.sendKeys(CampaignFolderName);
      String campaignNameFolderLocator = "//span[contains(text(),'" + CampaignFolderName + "')]";
      WebElement campaignFolderlocator = driver.findElement(By.xpath(campaignNameFolderLocator));
      driver.findElement(By.xpath(campaignNameFolderLocator)).click();
      textSearch.clear();
      textSearch.sendKeys(programName);
      String programNameFolderLocator = "//span[contains(text(),'" + programName + "')]";
      int programfolderSize = driver.findElements(By.xpath(programNameFolderLocator)).size();
      for (int i = 1; i <= programfolderSize; i++) {
        driver.findElement(By.xpath(programNameFolderLocator)).click();
        WebElement locator = driver.findElement(By.xpath(programNameFolderLocator));
        WebButton.RightClick(locator);
        Automation.testReport.log(LogStatus.INFO , "Deleting Program Folder");
        List<WebElement> elementEdit = driver.findElements(By.xpath("//span[text()='Delete']"));
        elementEdit.get(4).click();
        driver.findElement(By.xpath("//*[@id='deleteRadio']")).click();
        driver.findElement(By.xpath("//button[text()='Delete']")).click();
      }
      Automation.testReport.log(LogStatus.INFO , "Deleting Campaign Folder");
      WebButton.RightClick(campaignFolderlocator);
      driver.findElement(By.xpath("//span[text()='Delete Folder']")).click();
      List<WebElement> deleteList=driver.findElements(By.xpath("//button[@class='x4-btn-center']"));
      CommonUtil.Wait(5);
      deleteList.get(3).click();
      Automation.testReport.log(LogStatus.INFO , "Campaign Folder deleted successfully");

    } catch (StaleElementReferenceException e) {
      System.out.println(e);
    } catch (NoSuchElementException e) {
      System.out.println(e);
    } catch (Exception e) {
      System.out.println(e);
    }

  }

}
