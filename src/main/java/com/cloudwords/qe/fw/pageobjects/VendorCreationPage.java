/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.dbqueries.VendorQueries;
import com.cloudwords.qe.fw.wrapper.WebList;
import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author Shamsher Singh
 * @date 27-Aug-2016
 */
public class VendorCreationPage extends LoadableComponent<VendorCreationPage> {

  private static final Logger logger = Logger.getLogger(LoginPage.class);

  /**
   *
   */
  public VendorCreationPage(WebDriver driver, ExtentTest testReport) {
    PageFactory.initElements(driver, this);

  }

  @FindBy(how = How.ID, using = "vendor.name")
  private WebElement vendorName;

  @FindBy(how = How.ID, using = "vendorUser.email")
  private WebElement vendorUsrEmail;

  @FindBy(how = How.ID, using = "vendorUser.firstName")
  private WebElement vendorfirstName;

  @FindBy(how = How.ID, using = "vendorUser.lastName")
  private WebElement vendorlastName;

  @FindBy(how = How.ID, using = "vendorUser.password")
  private WebElement vendorPwd;

  @FindBy(how = How.ID, using = "vendor.brand")
  private WebElement vendorbrandList;

  @FindBy(how = How.ID, using = "vendor.automated1")
  private WebElement vendorAutomtedChkBox;

  @FindBy(how = How.ID, using = "lsp-create-button-bottom")
  private WebElement saveBtn;

  @FindBy(how = How.XPATH, using = "//*[@id='vendorProvisioner']/div[5]/div/a[2]")
  private WebElement cancelBtn;

  public void createAutomateVendor(String strVendorName, String strEmail, String strVendorFirstName,
      String strVendorLastName, String strVendorPassword, String strIndex,String strVednorType) {
    try {
      vendorName.sendKeys(strVendorName);
      vendorUsrEmail.sendKeys(strEmail);
      vendorfirstName.sendKeys(strVendorFirstName);
      vendorlastName.sendKeys(strVendorLastName);
      vendorPwd.sendKeys(strVendorPassword);
      vendorbrandList.click();
      WebList.SelectItemByIndex(vendorbrandList, strIndex);
      if (strVednorType.equalsIgnoreCase("automate")) {
    	  vendorAutomtedChkBox.click();
	}
      saveBtn.click();
      VendorQueries.activateVendor(strEmail);
    } catch (IllegalAccessException | ClassNotFoundException e) {
      e.printStackTrace();
    }

  }

  public void createManualVendor(String strVendorName, String strEmail, String strVendorFirstName,
      String strVendorLastName, String strVendorPassword, String strIndex) {
    try {
      vendorName.sendKeys(strVendorName);
      vendorUsrEmail.sendKeys(strEmail);
      vendorfirstName.sendKeys(strVendorFirstName);
      vendorlastName.sendKeys(strVendorLastName);
      vendorPwd.sendKeys(strVendorPassword);
      vendorbrandList.click();
      WebList.SelectItemByIndex(vendorbrandList, strIndex);
      saveBtn.click();
      VendorQueries.activateVendor(strEmail);
      Automation.STOREHASHMAP.put("VendorID",
          String.valueOf(VendorQueries.getVendorID(strVendorName)));
      VendorQueries.activateVendor(strEmail);

    } catch (IllegalAccessException | ClassNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (SQLException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  /*
   * (non-Javadoc)
   *
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub

  }

}
