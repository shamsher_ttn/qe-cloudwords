package com.cloudwords.qe.fw.pageobjects;

import java.util.List;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.wrapper.WaitTool;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

public class ProjectDescriptionPage {

  private WebDriver driver;
  private static final Logger logger = Logger.getLogger(ProjectDescriptionPage.class);

  /**
   *
   */
  public ProjectDescriptionPage(WebDriver driver, ExtentTest testReport) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  @FindBy(how = How.ID, using = "tab_projects")
  private WebElement projectTab;

  @FindBy(how = How.XPATH, using = "//*[@id='projectStatusFilter']/ul/li[17]/label/input")
  private WebElement filterClosed;

  @FindBy(how = How.XPATH, using = "//tbody/tr[contains(@class,'table-content request-summary')]")
  private WebElement projectRowsInProjectTable;

  public Object checkProjectStatus(String strProjectName, String strProjectStatus) {
    WaitTool.waitForElementByWebElement(driver, projectTab, 5);
    projectTab.click();
    WaitTool.waitForElementByWebElement(driver, filterClosed, 5);
    filterClosed.click();

    List<WebElement> lw1 = driver
        .findElements(By.xpath("//tbody/tr[contains(@class,'table-content request-summary')]"));

    int rowSize = lw1.size();
    boolean verifyProjectStatus = true;
    for (int i = 0; i < rowSize * 2; i++) {

      if (i % 2 != 0) {

        if (driver.findElement(By.xpath("//tbody/tr[" + (i) + "]/td[2]/a")).getText()
            .equalsIgnoreCase(strProjectName)) {

          if (driver.findElement(By.xpath("//tbody/tr[" + (i) + "]/td[6]")).getText()
              .equalsIgnoreCase(strProjectStatus)) {

            verifyProjectStatus = true;
          }

          else
            verifyProjectStatus = false;

        }
      }
    }

    if (verifyProjectStatus) {
      Automation.testReport.log(LogStatus.INFO, "project status verified Successfully");
      ProjectDescriptionPage projectDescriptionPage = new ProjectDescriptionPage(driver,
          Automation.testReport);
      return projectDescriptionPage;
    } else {
      Automation.testReport.log(LogStatus.INFO, "project status verified Failed");
      ProjectDescriptionPage projectDescriptionPage = new ProjectDescriptionPage(driver,
          Automation.testReport);
      return projectDescriptionPage;
    }
  }

  // boolean loginSuccess = WebObject.isElementPresent("//*[@id='create-new-button']");
  //
  // if(loginSuccess){
  // Automation.testReport.log(LogStatus.INFO, "Login Success");
  // LandingPage landingPage = new LandingPage(driver ,Automation.testReport);
  // return landingPage;
  // }
  // else{
  // Automation.testReport.log(LogStatus.INFO, "Login Not Success");
  // LoginPage loginPage = new LoginPage(driver,Automation.testReport);
  // return loginPage;
  // }

  @FindBy(how = How.CSS, using = ".large-type.blue.table-left-padding")
  private WebElement selectLanguage;
  @FindBy(how = How.CSS, using = "#tab_projects>a>span")
  private WebElement projectVendorTab;
  @FindBy(how = How.CSS, using = "#projectStatusFilter>ul>li:nth-child(5)>label>input")
  private WebElement filterVendorInTranslation;
  @FindBy(how = How.CSS, using = ".btn.btn-blue.doFileUpload")
  private WebElement translatiopnUploadButton;
  @FindBy(how = How.CSS, using = "#popup_ok>span")
  private WebElement confirmLanguageTranslationButton;
  @FindBy(how = How.XPATH, using = ".//*[@id='project.status']")
  private WebElement labelProjectStatus;

  public void vendorUploadLanguageDelieverable(String strProjectName) {

    WaitTool.waitForElementByWebElement(driver, projectVendorTab, 5).click();
    WaitTool.waitForElementByWebElement(driver, filterVendorInTranslation, 5);
    filterVendorInTranslation.click();
    WaitTool.waitForListElementsPresent(driver,
        By.xpath("//tbody/tr[contains(@class,'table-content request-summary')]"), 5);
    List<WebElement> lw1 = driver
        .findElements(By.xpath("//tbody/tr[contains(@class,'table-content request-summary')]"));
    int rowSize = lw1.size();
    boolean verifyProjectStatus = true;
    for (int i = 0; i < rowSize * 2; i++) {
      if (i % 2 != 0) {
        if (driver.findElement(By.xpath("//tbody/tr[" + (i) + "]/td[2]/a")).getText()
            .equalsIgnoreCase(strProjectName)) {
          driver.findElement(By.xpath("//tbody/tr[" + (i) + "]/td[2]/a")).click();
          break;
        }
      }
    }
    selectLanguage.click();
    translatiopnUploadButton.click();
    confirmLanguageTranslationButton.click();
    if (labelProjectStatus.getText().equalsIgnoreCase("")) {
      verifyProjectStatus = true;
    } else {
      verifyProjectStatus = false;
    }
  }

  @FindBy(how = How.CSS, using = ".radio-chooser>input#stripe-radio")
  private WebElement radioCard;

  @FindBy(how = How.CSS, using = "#payflow_card_number")
  private WebElement inputBoxCardNumber;

  @FindBy(how = How.CSS, using = "#payflow_month")
  private WebElement inputBoxMonth;

  @FindBy(how = How.CSS, using = "#payflow_year")
  private WebElement inputBoxYear;

  @FindBy(how = How.CSS, using = "#payflow_cvc")
  private WebElement inputBoxCVV;

  @FindBy(how = How.CSS, using = "#payflow_zip")
  private WebElement inputBoxZipCode;

  @FindBy(how = How.CSS, using = "#payflow-purchase>span")
  private WebElement btnPurchase;

  @FindBy(how = How.CSS, using = "#payflow_card_number")
  private WebElement inputYear;

  @FindBy(how = How.CSS, using = "#payflow-first-enter>span")
  private WebElement btnNext;

  @FindBy(how = How.CSS, using = "#payflow-close")
  private WebElement btnOk;

  @FindBy(how = How.CSS, using = ".standard-payflow-box>p")
  private WebElement vedorSelectionConfirmationMssg;

  @FindBy(how = How.CSS, using = ".project-status-detail")
  private WebElement projectStatusText;

  @FindBy(how = How.XPATH, using = "//*[@id='bidHistoryTable']/tbody/tr[2]/td[4]/a")
  private WebElement btnSelectVendor;

  @FindBy(how = How.CSS, using = "#projectStatusFilter>ul>li:nth-child(7)>label>input")
  private WebElement filterWaitingForBidSelection;

  @FindBy(how = How.ID, using = "tab_bids")
  private WebElement bidTab;

  public Object selectVendorBidding(String strProjectName) throws Exception {
    logger.info("Selenium Driver wait for 3 seconds");
    // WaitTool.waitForElementByWebElement(driver, eleUsrName, 3)

    WaitTool.waitForElementByWebElement(driver, projectTab, 5).click();

    WaitTool.waitForElementByWebElement(driver, filterWaitingForBidSelection, 5);
    Actions action = new Actions(driver);

    action.moveToElement(filterWaitingForBidSelection).build().perform();
    Thread.sleep(3000);

    filterWaitingForBidSelection.click();

    // Waiting for the list of projects to appear
    WaitTool.waitForElementByWebElement(driver, projectRowsInProjectTable, 5);

    // getting list of project
    List<WebElement> lw1 = driver
        .findElements(By.xpath("//tbody/tr[contains(@class,'table-content request-summary')]"));

    int rowSize = lw1.size();

    System.out.println("list of the projects is " + rowSize);

    boolean verifyProjectStatus = true;

    // going through the list of project and selecting the project which need to be worked upon
    for (int i = 0; i < rowSize * 2; i++) {
      if (i % 2 != 0) {
        if (driver.findElement(By.xpath("//tbody/tr[" + (i) + "]/td[2]/a")).getText()
            .equalsIgnoreCase(strProjectName)) {

          driver.findElement(By.xpath("//tbody/tr[" + (i) + "]/td[2]/a")).click();
        }
      }
    }

    // waiting and clicking on the bid tab
    WaitTool.waitForElementByWebElement(driver, bidTab, 5).click();

    // wait and click on selecting the vendor
    WaitTool.waitForElementByWebElement(driver, btnSelectVendor, 5).click();

    // wait and click on selecting the card
    WaitTool.waitForElementByWebElement(driver, radioCard, 3).click();

    // wait and enter the credit card number
    WaitTool.waitForElementByWebElement(driver, inputBoxCardNumber, 3).sendKeys("4111111111111111");

    // enter the credit card month
    inputBoxMonth.sendKeys("02");

    // enter the credit card year
    inputBoxYear.sendKeys("22");

    // enter the credit card cvv
    inputBoxCVV.sendKeys("123");

    // enter the pincode
    inputBoxZipCode.sendKeys("12345");

    // click on the next button
    btnNext.click();

    // click on the purchase button
    btnPurchase.click();

    // verifying the payment message
    if (vedorSelectionConfirmationMssg.getText().equalsIgnoreCase(
        "Your selected vendor has been notified and the project will now proceed.")) {
      verifyProjectStatus = true;
      btnOk.click();
    }

    else {

      verifyProjectStatus = false;

    }

    if (verifyProjectStatus) {
      Automation.testReport.log(LogStatus.INFO, "vendor after bidding is selected successfully");
      ProjectDescriptionPage projectDescriptionPage = new ProjectDescriptionPage(driver,
          Automation.testReport);
      System.out.println("test case is passed");
      return projectDescriptionPage;
    } else {
      Automation.testReport.log(LogStatus.INFO, "vendor after bidding is selected failed");
      ProjectDescriptionPage projectDescriptionPage = new ProjectDescriptionPage(driver,
          Automation.testReport);
      return projectDescriptionPage;
    }
  }

}
