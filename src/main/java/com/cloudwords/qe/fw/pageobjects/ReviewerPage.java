package com.cloudwords.qe.fw.pageobjects;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import com.cloudwords.qe.fw.libraries.PageLoadHelper;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import com.relevantcodes.extentreports.ExtentTest;
import junit.framework.Assert;

public class ReviewerPage extends LoadableComponent<ReviewerPage> {

	private WebDriver driver;
	private String sourceLanguage, targetLanguage, reviewSourceLanguage, reviewTranslationLanguage;
	boolean reviewButton;
	String fileName;
	List<String> items;

	public ReviewerPage(WebDriver driver, ExtentTest test) throws Exception {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.CSS, using = ".status-block.fourth")
	public WebElement viewProjects;

	@FindBy(how = How.CSS, using = ".logo>a")
	private WebElement logoCloudWords;

	@FindBy(how = How.CSS, using = "#tab_projects>a")
	private WebElement projectCloudWords;

	@FindBy(how = How.CSS, using = ".table-content.request-summary:nth-child(1)>td")
	private List<WebElement> columnCount;

	@FindBy(how = How.CSS, using = ".table-content.request-summary>td:nth-of-type(2)>a")
	private List<WebElement> selectProject;

	@FindBy(how = How.XPATH, using = "//*[starts-with(@id, 'tab_language_')]")
	private List<WebElement> selectTranslationLanguage;

	@FindBy(how = How.CSS, using = "#tab_details>a>span")
	private WebElement viewProjectDetails;

	@FindBy(how = How.CSS, using = ".files-action-column>span>a")
	private WebElement clickReviewButton;

	@FindBy(how = How.CSS, using = ".files-action-column>a")
	private WebElement clickViewButton;

	@FindBy(how = How.CSS, using = "div>p:nth-of-type(2)>span")
	private WebElement editAuthorName;

	@FindBy(how = How.CSS, using = "#contextTextMenuContainer>ul>li:nth-child(1)>a")
	private WebElement provideFeedback;

	@FindBy(how = How.CSS, using = "tbody:nth-child(3)>tr:nth-child(2)>td>textarea")
	private WebElement inputFeedbackComments;

	@FindBy(how = How.CSS, using = "#centerTableSplitter")
	private WebElement splitPage;

	@FindBy(how = How.CSS, using = "#complete-review-btn")
	private WebElement submitReview;

	@FindBy(how = How.CSS, using = "#actions-btn")
	private WebElement actionsButton;

	@FindBy(how = How.CSS, using = "#revision-request-create-form>p>label:nth-child(1)>input")
	private WebElement clickApprove;

	@FindBy(how = How.CSS, using = "#complete-review-submit-btn")
	private WebElement clickFinishReview;

	@FindBy(how = How.CSS, using = ".project-detail.sub.return")
	private WebElement backToWhereICameFrom;

	public void scrollToWebElement(WebDriver driver, WebElement element) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		Thread.sleep(5000);
	}

	public boolean elementIsPresent(String element) {
		boolean present;
		present = driver.findElement(By.xpath(element)).isDisplayed();

		return present;
	}

	public int getColumnCount() {
		int columnSize = columnCount.size();
		return columnSize;
	}

	public List<WebElement> selectIndividualProject() {
		return selectProject;
	}

	public void clickProjects() {
		projectCloudWords.click();
	}

	public void clickLogo() {
		logoCloudWords.click();
	}

	public int countTargetLanguage() {
		int countTargetlang = selectTranslationLanguage.size();
		return countTargetlang;
	}

	public void switchBackToSelectedCategory() {
		backToWhereICameFrom.click();
	}

	public void switchToProjectListing() throws InterruptedException {
		clickLogo();
	CommonUtil.Wait(10);
		viewProjects.click();
		CommonUtil.Wait(2);
	}

	public int reviewProjects() throws InterruptedException {
		Thread.sleep(3000);
		WebElement reviewProjects = driver.findElement(By.cssSelector(".status-block.fourth>span:nth-child(1)"));
		int countReviewProjects = Integer.parseInt(reviewProjects.getText());
		viewProjects.click();
		List<WebElement> projectsToBeReviewed = driver.findElements(
				By.xpath("//*[@id='project_search_list']/div[2]/table/tbody/tr/td[6][contains(text(),'In Review')]"));
		System.out.println("You are yet to Review " + projectsToBeReviewed.size() + " Projects");
		Assert.assertEquals(countReviewProjects, projectsToBeReviewed.size());

		return projectsToBeReviewed.size();
	}

	public Map<String, Object> isReviewButtonPresent(int j) {
		Map<String, Object> getTranslationLanguage = new HashMap<String, Object>();
		String transLanguage = null;
		try {
			selectTranslationLanguage.get(j).click();
			transLanguage = selectTranslationLanguage.get(j).getText();
			Thread.sleep(3000);
			String reviewApproved = "html/body/div[4]/div[3]/div[1]/div[2]/div[1]/div/div/div[2]/table[2]/thead/tr/th[3]";
			String viewButtonCell = "html/body/div[4]/div[3]/div[1]/div[2]/div[1]/div/div/div[2]/table[3]/tbody/tr/td[5]";

			WebElement reveiewApprovedProject = driver.findElement(By.xpath(reviewApproved));
			WebElement viewButton = driver.findElement(By.xpath(viewButtonCell));

			if (elementIsPresent(reviewApproved)) {
				if (reveiewApprovedProject.getText().trim().equalsIgnoreCase("Reopen Language")) {
					reviewButton = false;
				} else {
					if (elementIsPresent(viewButtonCell)) {
						String viewButtonText = viewButton.getText();
						if (viewButtonText.trim().equals("View")) {
							reviewButton = false;
						} else {

							Calendar cal = Calendar.getInstance();
							cal.add(Calendar.MINUTE, 10);
							Date newtDate = cal.getTime();

							boolean result = false;
							while (!result && Calendar.getInstance().getTime().compareTo(newtDate) < 0) {
								result = locateReviewButton((j + 1));
							}

							if (result == true) {
								WebElement reviewButtonCss = driver.findElement(
										By.cssSelector("tbody>tr:nth-child(" + (j + 1) + ")>td:last-child>span>a"));

								if (reviewButtonCss.isDisplayed()) {
									System.out.println("You can now proceed with Reviewer WorkFlow");
									Assert.assertTrue(reviewButtonCss.isDisplayed());
									result = false;
								}

								reviewButton = true;
							} else {
								System.out.println("Can not process the request post specified Time duration");
								Assert.assertTrue("Can not process the request post specified Time duration", false);
							}
						}
					}
				}
			} else {
				reviewButton = false;
			}
		} catch (InterruptedException e) {
			System.out.println(e);
			e.printStackTrace();
		} catch (ElementNotFoundException e) {
			System.out.println(e);
		} catch (Exception e) {
			System.out.println(e);
		}

		getTranslationLanguage.put(transLanguage, reviewButton);
		return getTranslationLanguage;
	}

	public void submitFeedback(int lineNum, String Text, String fileExt) throws InterruptedException {
		WebElement clickFeedBackIcon = driver.findElement(By.cssSelector("#line-by-line-review-table>tbody:nth-child("
				+ lineNum + ")>tr:nth-child(1)>td:nth-child(6)>span:nth-child(2)>i"));
		scrollToWebElement(driver, clickFeedBackIcon);
		clickFeedBackIcon.click();
		Thread.sleep(3000);
		WebElement inputFeedbackComments = driver
				.findElement(By.cssSelector("tbody:nth-child(" + lineNum + ")>tr:nth-child(2)>td>textarea"));
		WebElement submitFeedBackButton = driver.findElement(
				By.cssSelector("tbody:nth-child(" + lineNum + ")>tr:nth-child(2)>td>button.btn.btn-green"));

		Actions Action = new Actions(driver);
		Action.moveToElement(inputFeedbackComments).sendKeys(Text).build().perform();
		Action.moveToElement(submitFeedBackButton).click().build().perform();
		Thread.sleep(5000);

		verifyFeedback(lineNum, Text, fileExt);
	}

	public void verifyFeedback(int lineNum, String text, String fileExtension) throws InterruptedException {
		CommonUtil.Wait(5);
		WebElement verifyFeedback;
		if (!fileExtension.equals("psd")) {
			verifyFeedback = driver.findElement(By.xpath("// tbody[" + lineNum + "]/tr[4]/td/div/div[3]/div[3]"));
		} else {
			verifyFeedback = driver.findElement(By.xpath("// tbody[" + lineNum + "]/tr[4]/td[2]/div/div[2]/div[3]"));
		}

		scrollToWebElement(driver, verifyFeedback);
		String feedbackText = verifyFeedback.getText();
		Assert.assertTrue(feedbackText.contains(text));

	}

	public void editTranslation(int lineNum, String translationText) throws InterruptedException {
		WebElement originalText;
		String translatedText = null;
		originalText = driver.findElement(By.xpath("//tbody[" + lineNum + "]/tr[1]/td[4]/div[1]"));
		scrollToWebElement(driver, originalText);

		if (originalText.isDisplayed()) {
			translatedText = originalText.getText();
		} else {
			System.out.println("This text has never been translated earlier");
		}

		Actions Action = new Actions(driver);
		WebElement editTranslationIcon = driver.findElement(By.cssSelector("#line-by-line-review-table>tbody:nth-child("
				+ lineNum + ")>tr:nth-child(1)>td:nth-child(6)>span:nth-child(1)>i"));
		scrollToWebElement(driver, editTranslationIcon);

		Action.moveToElement(editTranslationIcon).click().build().perform();
		CommonUtil.Wait(5);

		WebElement editTranslation = driver.findElement(By.cssSelector("#line-by-line-review-table>tbody:nth-child("
				+ lineNum + ")>tr>td:nth-child(4)>div:nth-child(2)>div:nth-child(1)>div"));
		String editTranslationText = editTranslation.getText();

		Action.moveToElement(editTranslation).build().perform();
		editTranslation.clear();
		CommonUtil.Wait(5);

		Action.moveToElement(editTranslation).sendKeys((translationText + editTranslationText));
		CommonUtil.Wait(4);

		WebElement submitTranslation = driver.findElement(By
				.cssSelector("tbody:nth-child(" + lineNum + ")>tr>td:nth-child(4)>div:nth-child(2)>button.btn-green"));

		scrollToWebElement(driver, submitTranslation);
		CommonUtil.Wait(3);

		Action.click(submitTranslation).build().perform();
		CommonUtil.Wait(3);

		compareTranslationText(originalText, translatedText, lineNum, translationText);
	}

	public void compareTranslationText(WebElement element, String Text, int lineNum, String translationText)
			throws InterruptedException {

		WebElement originalText;
		String expectedTextPostTranslation;

		if (Text != null && Text != "") {
			originalText = driver.findElement(By.xpath("//tbody[" + lineNum + "]/tr[1]/td[4]/div[1]"));
			expectedTextPostTranslation = translationText + Text;

		} else {
			originalText = driver.findElement(By.xpath("//tbody[" + lineNum + "]/tr[last()]/td/div/div[3]/div[1]"));
			expectedTextPostTranslation = translationText + originalText.getText();
		}

		scrollToWebElement(driver, originalText);
		String actualTextPostTranslation = driver.findElement(By.xpath("//tbody[" + lineNum + "]/tr[1]/td[4]/div[1]"))
				.getText();
		Assert.assertEquals(expectedTextPostTranslation.trim(), actualTextPostTranslation);
	}

	public String getProjectDetails() {
		CommonUtil.Wait(3);
		viewProjectDetails.click();
		CommonUtil.Wait(5);
		sourceLanguage = driver.findElement(By.xpath("//*[@id='project.sourceLanguage']")).getText();
		sourceLanguage += " Source";
		targetLanguage = driver.findElement(By.xpath("//*[@id='project.targetLanguages']")).getText();
		items = Arrays.asList(targetLanguage.split(","));
		String projectId = driver.findElement(By.xpath("//*[@id='projectDetail_details']/div[1]/div[4]/p[1]/span"))
				.getText();
		return projectId;
	}

	public void getTranslationHeaders() {
		reviewSourceLanguage = driver.findElement(By.cssSelector("#status-line>div:nth-child(1)>h5")).getText();
		reviewSourceLanguage = reviewSourceLanguage.trim();
		reviewTranslationLanguage = driver.findElement(By.cssSelector("#status-line>div:nth-child(3)>h5")).getText();
		reviewTranslationLanguage = reviewTranslationLanguage.trim();
		System.out.println("Source language on Reviewer Page: " + reviewSourceLanguage);
		System.out.println("Target language on Reviewer Page: " + reviewTranslationLanguage);
	}

	public String getFileNameForTranslation(int k, String testCaseName) {
		Actions action = new Actions(driver);
		fileName = driver.findElement(By.xpath(
				"html/body/div[4]/div[3]/div[1]/div[2]/div[1]/div/div/div[2]/table[3]/tbody/tr[" + k + "]/td[2]"))
				.getText();

		WebElement downloadIcon = driver.findElement(
				By.xpath("//*[@id='projectContainer']/div[2]/table[3]/tbody/tr[" + k + "]/td[4]/div/div/a"));

		if (!testCaseName.equals("TestTargetTranslation")) {
			action.moveToElement(downloadIcon).click().build().perform();
		}
		return fileName;
	}


	public String getFileNameForTranslation() {
		fileName = driver
				.findElement(By
						.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div[1]/div/div/div[2]/table[3]/tbody/tr[1]/td[2]"))
				.getText();

		WebElement downloadIcon = driver
				.findElement(By.xpath("//*[@id='projectContainer']/div[2]/table[3]/tbody/tr[1]/td[4]/div/div/a"));

		downloadIcon.click();
		return fileName;
	}

	public void translateWithoutFeedback(int langNum, int fileNum) throws InterruptedException {
		WebElement reviewButtonForAllPendingTasks = driver.findElement(By.cssSelector(
				".files-list.content.full-width>tbody>tr:nth-child(" + fileNum + ")>td:nth-child(6)>span>a"));

		Actions action = new Actions(driver);
		WebElement bottomOfPage = driver.findElement(By.cssSelector(".footer>p>a:nth-child(1)"));
		scrollToWebElement(driver, bottomOfPage);
		action.moveToElement(reviewButtonForAllPendingTasks).click().build().perform();
		Thread.sleep(5000);

		getTranslationHeaders();
		Assert.assertEquals(sourceLanguage.trim(), reviewSourceLanguage.trim());
		Assert.assertEquals(items.get(langNum).trim() + " Translation", reviewTranslationLanguage);
		Thread.sleep(3000);
	}

	public void isDocumentLoaded() {
		WebElement htmlDocument = driver.findElement(By.cssSelector("#inContextHolderSrc>div"));
		if (htmlDocument.isDisplayed()) {
			Assert.assertTrue(true);
		}
	}

	public void reviewTranslation(int itemNum, String setText, String translationText, String feedbackLineNum,
			String ediTranslationLineNum, String fileExtension, String fileName, int rowNum, ReviewerPage reviewPage)
			throws InterruptedException {
		String[] feedbackLineNumber = feedbackLineNum.split(",");
		String[] editTranslationLineNumber = ediTranslationLineNum.split(",");
		scrollToWebElement(driver, clickReviewButton);
		clickReviewButton.click();
		if(driver.findElements(By.xpath("//*[@id='first-time-instructions-close-btn']")).size()!=0){
		      driver.findElement(By.xpath("//*[@id='first-time-instructions-close-btn']")).click();
		    }
		CommonUtil.Wait(5);

		if (fileExtension.equals("psd")) {
			reviewPage.isDocumentLoaded();
		} else {
			System.out.println("Document Verification is not required for this file");
		}

		List<WebElement> rowsTobereviewed = driver.findElements(By.cssSelector("#line-by-line-review-table>tbody"));
		int rowCount = rowsTobereviewed.size();
		if (fileExtension.equals("docx") ||fileExtension.equals("psd")|| (fileExtension.equals("xlf") && !fileName.equals("assets-metadata")
				&& !fileName.equals("tags-metadata"))) {
			CommonUtil.Wait(5);
			getTranslationHeaders();
			Assert.assertEquals(sourceLanguage.trim(), reviewSourceLanguage.trim());
			Assert.assertEquals(items.get(itemNum).trim() + " Translation", reviewTranslationLanguage);

			for (int i = 0; i < feedbackLineNumber.length; i++) {
				if (Integer.parseInt(feedbackLineNumber[i]) <= rowCount) {
					submitFeedback(Integer.parseInt(feedbackLineNumber[i]), setText, fileExtension);
					for (int p = i; p < editTranslationLineNumber.length; p++) {
						if (Integer.parseInt(editTranslationLineNumber[p]) <= rowCount) {
							editTranslation(Integer.parseInt(editTranslationLineNumber[p]), translationText);
							Thread.sleep(3000);
							break;
						}
					}
				}
			}

			submitTranslationReview(rowNum);
		} else {
			WebElement approveasset = driver.findElement(By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div[1]/div/div/div[2]/div[3]/div/div[2]/button[1]"));
			approveasset.click();
			CommonUtil.Wait(3);
			WebElement approveDialog = driver.findElement(By.xpath("html/body/div[11]/div/div[9]/div/div/form/div[2]/a[1]/span"));
			approveDialog.click();
			CommonUtil.Wait(3);
		}
	}

	public void submitTranslationReview(int rowNum) throws InterruptedException {
		Actions Action = new Actions(driver);
		Action.moveToElement(actionsButton).click().build().perform();
		CommonUtil.Wait(3);
		WebElement selectApprove = driver
				.findElement(By.xpath("html/body/div[4]/div/div[1]/div/div/div[1]/div[7]/div/div/ul/li[2]/a"));
		selectApprove.click();
		WebElement approveDeliverable = driver.findElement(By.cssSelector("#approve-deliverable-btn"));
		approveDeliverable.click();
		CommonUtil.Wait(3);

		boolean result = false;
		while (!result) {
			result = locateViewButton(rowNum);
		}

		WebElement viewButton = driver
				.findElement(By.cssSelector("tbody>tr:nth-child(" + rowNum + ").table-content>td:nth-child(5)>span>a"));

		if (viewButton.isDisplayed()) {
			System.out.println("Translation for a file completed Successfully");
			Assert.assertTrue(viewButton.isDisplayed());
			result = false;
		}
	}

	public boolean locateViewButton(int viewButtonRowNum) {
		String viewButtonCSS = "tbody>tr:nth-child(" + viewButtonRowNum + ").table-content>td:nth-child(5)>span>a";
		Boolean ele = false;
		try {
			CommonUtil.Wait(30);
			driver.navigate().refresh();
			CommonUtil.Wait(3);
			WebElement element = driver.findElement(By.cssSelector(viewButtonCSS));
			if (element.isDisplayed()) {
				ele = true;
			}
			return ele;
		} catch (Exception e1) {
			System.out.println("Exception Message : " + e1.getMessage());
		}
		return ele;
	}

	public boolean locateReviewButton(int reviewButtonRowNum) {
		String reviewButtonCSS = "tbody>tr:nth-child(" + reviewButtonRowNum + ")>td:last-child>span>a";
		Boolean ele = false;
		try {
			CommonUtil.Wait(30);
			driver.navigate().refresh();
			CommonUtil.Wait(3);
			WebElement element = driver.findElement(By.cssSelector(reviewButtonCSS));
			if (element.isDisplayed()) {
				ele = true;
			}
			return ele;
		} catch (Exception e1) {
			ele = false;
			return ele;
		}
	}

	public void waitUntilElementGetsValue(final String cssSelector, final String value, int timeOut, int pollingTime) {
		new FluentWait<WebDriver>(driver).withTimeout(timeOut, TimeUnit.SECONDS)
				.pollingEvery(pollingTime, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
				.until(new ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver wd) {
						driver.navigate().refresh();
						CommonUtil.Wait(3);
						WebElement element = driver.findElement(By.cssSelector(cssSelector));
						return element.getText().equalsIgnoreCase(value);
					}
				});
	}

	public void isImageLoaded() throws InterruptedException {
		CommonUtil.Wait(5);
		driver.switchTo().frame(driver.findElement(By.id("previewiFrameSrc")));
		WebElement image = driver.findElement(By.cssSelector("html>body>img"));
		String imgSource = image.getAttribute("src");
		driver.switchTo().defaultContent();

		// Handle SSL Security Exception
		javax.net.ssl.HttpsURLConnection.setDefaultHostnameVerifier(new javax.net.ssl.HostnameVerifier() {
			public boolean verify(String hostname, javax.net.ssl.SSLSession sslSession) {
				return hostname.equals("localhost");
			}
		});

		int size;
		URL url = null;
		try {
			url = new URL(imgSource);
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		URLConnection conn = null;
		try {
			conn = url.openConnection();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		size = conn.getContentLength();
		if (size < 0) {
			System.out.println("Could not determine file size.");
			Assert.assertTrue(false);
		} else {
			Assert.assertTrue(size > 0);
			try {
				conn.getInputStream().close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub
		PageLoadHelper.isLoaded().isElementIsClickable(driver, viewProjects).isElementIsClickable(driver,
				clickReviewButton);
		// .isElementIsVisible(driver, selectTranslationLanguage)
		// .isElementIsClickable(driver, selectTranslationLanguage);
	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub

	}
}
