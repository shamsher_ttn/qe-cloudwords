/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.libraries.CommonExpectedConditions;
import com.cloudwords.qe.fw.libraries.PublicVariables;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.FileUtil;
import com.cloudwords.qe.fw.wrapper.General;
import com.cloudwords.qe.fw.wrapper.WaitTool;
import com.cloudwords.qe.fw.wrapper.WebList;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author Shamsher Singh
 * @date 05-Sep-2016
 */
public class ProjectCreationPage extends LoadableComponent<ProjectCreationPage> {

	private WebDriver driver;

	/**
	 *
	 */
	public ProjectCreationPage(WebDriver driver, ExtentTest test) {

		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID, using = "create-new-button")
	private WebElement newCreateBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='_dropdown_0']/li[1]/a")
	private WebElement newProjectCreateBtn;

	@FindBy(how = How.ID, using = "projectName")
	private WebElement prjName;

	@FindBy(how = How.ID, using = "intendedUse")
	private WebElement intendedUse;

	@FindBy(how = How.ID, using = "priority")
	private WebElement priority;

	@FindBy(how = How.ID, using = "projectDescription")
	private WebElement projectDescription;

	@FindBy(how = How.XPATH, using = "//*[@id='project_tags']/div/input")
	private WebElement tags;

	@FindBy(how = How.ID, using = "nextStepBtnId")
	private WebElement nextStepBtn;

	@FindBy(how = How.ID, using = "sourceLanguage")
	private WebElement sourceLanguage;

	@FindBy(how = How.ID, using = "languageSelectViewSelector")
	private WebElement targetLanguages;

	@FindBy(how = How.ID, using = "selectSourceFileLink")
	private WebElement selectSourceFileLink;

	@FindBy(how = How.CLASS_NAME, using = "_fileUploadBinary")
	private WebElement fileUploadLink;

	@FindBy(xpath = "//*[contains(@class,'addNewFileLink')]")
	private WebElement addNewFileLink;

	@FindBy(xpath = "//*[contains(@class,'doFileUpload')]")
	private WebElement uploadBtn;

	@FindBy(xpath = "//*[contains(@class,'closeFileUpload')]")
	private WebElement closeFileUpload;

	@FindBy(how = How.ID, using = "selectRefFileLink")
	private WebElement selectRefFileLink;

	@FindBy(xpath = "//*[@id='wizardContent']/ul/li/div/div[3]/label[2]/input")
	private WebElement externalBidless;

	@FindBy(xpath = "//*[@id='selected-vendors-display']/li")
	private WebElement selectedVendors;

	@FindBy(xpath = "//*[@id='selected-bidless-vendors-display']/li")
	private WebElement selectedBidlessVendors;

	@FindBy(id = "ProjectDatesViewbidDueDate")
	private WebElement ProjectDatesViewbidDueDate;

	@FindBy(id = "ProjectDatesViewdeliveryDueDate")
	private WebElement ProjectDatesViewdeliveryDueDate;

	@FindBy(xpath = "//*[@id='finish-link']")
	private WebElement finishBtn;

	@FindBy(id = "popup_ok")
	private WebElement popup_ok;

	@FindBy(id = "popup_cancel")
	private WebElement popup_cancel;

	@FindBy(id = "view-project-button")
	private WebElement viewProjectBtn;

	@FindBy(id = "cloneProjectButton")
	private WebElement cloneProjectBtn;

	@FindBy(how = How.ID, using = "project-select-STANDARD")
	private WebElement standardBtn;

	@FindBy(how = How.ID, using = "project-select-MARKETO")
	private WebElement marketoBtn;

	@FindBy(how=How.ID,using="project-select-HUBSPOT")
	private WebElement hubspotBtn;

	@FindBy(how = How.ID, using = "changeContentTypeLink")
	private WebElement changeContentTypeLink;

	@FindBy(how = How.XPATH, using = "//*[@id='selection-type-select']/li[1]")
	private WebElement newassestLink;

	@FindBy(how = How.XPATH, using = "//*[@id='selection-type-select']/li[2]")
	private WebElement newprogramLink;

	@FindBy(how = How.XPATH, using = "//*[@id='selection-type-select']/li[3]")
	private WebElement dynamicontentLink;

	@FindBy(how = How.XPATH, using = "//*[@id='selection-type-select']/li[2]")
	private WebElement eloquadynamicontentLink;

	@FindBy(how = How.ID, using = "changeSelectionTypeLink")
	private WebElement changeSelectionTypeLink;

	@FindBy(how = How.ID, using = "marketingTabLink")
	private WebElement marketingTabLink;

	@FindBy(how = How.ID, using = "designTabLink")
	private WebElement designTabLink;

	@FindBy(how = How.ID, using = "marketingTreeSearch")
	private WebElement marketingTreeSearch;

	@FindBy(how = How.ID, using = "designTreeSearch")
	private WebElement designTreeSearch;

	@FindBy(how = How.ID, using = "asset-second-step")
	private WebElement assetsecondstep;

	@FindBy(how = How.ID, using = "source-language-select")
	private WebElement sourcelanguageselect;

	@FindBy(how = How.ID, using = "bulk-tl-selector")
	private WebElement targetLangLink;

	@FindBy(how = How.ID, using = "overrideTemplate")
	private WebElement overrideTemplate;

	@FindBy(how = How.XPATH, using = "//*[@id='overrideTemplateLightbox']/div[2]/div/div/div/table")
	private WebElement templateWebTable;

	@FindBy(how = How.XPATH, using = "//*[@id='marketingTab']/div[1]/div[2]/a[1]")
	private WebElement expandBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='saveTemplateOverrides']")
	private WebElement saveTemplateOverrideBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='project-select-ELOQUA']")
	private WebElement eloquaBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='landingPageTabLink']")
	private WebElement landingPageTabLink;

	@FindBy(how = How.XPATH, using = "//*[@id='emailsTabLink']")
	private WebElement emailsTabLink;

	@FindBy(how = How.XPATH, using = "//*[@id='sitePageTabLink'']")
	private WebElement webpagesTabLink;

	@FindBy(how = How.XPATH, using = "//*[@id='formsTabLink']")
	private WebElement formsTabLink;

	@FindBy(how = How.XPATH, using = "html/body/div[4]/div[4]/div/div[3]/div/div/ul/li/div/div[4]/div[3]/div[1]/div[1]/div[1]/ul/li[5]/a")
	private WebElement blogpostsTabLink;

	@FindBy(how = How.XPATH, using = "//*[@id='landingPageAssetSearch']")
	private WebElement landingPageAssetSearch;

	@FindBy(how = How.XPATH, using = "//*[@id='emailAssetSearch']")
	private WebElement emailAssetSearch;

	@FindBy(how = How.XPATH, using = "//*[@id='formAssetSearch']")
	private WebElement formAssetSearch;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.openqa.selenium.support.ui.LoadableComponent#load()
	 */
	@Override
	protected void load() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
	 */
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub

	}

	public void SimpleSingleFileProjectCreation(String strUsrName, String strPassword,
			CharSequence strPrjName, String strIndexIntendedUse, String strIndexPriority, String filePath,
			String strIndexSrcLang, String strIndexTargetLang, String date) {
		LoginUtil.loginWithCredentials(strUsrName, strPassword);
		try {
			newCreateBtn.click();
			newProjectCreateBtn.click();
			prjName.sendKeys(strPrjName);
			WebList.SelectItemByIndex(intendedUse, strIndexIntendedUse);
			WebList.SelectItemByIndex(priority, strIndexPriority);
			nextStepBtn.click();
			selectSourceFileLink.click();
			General.AttachFile(fileUploadLink, filePath);
			uploadBtn.click();
			CommonExpectedConditions.elementToBeClickable(closeFileUpload);
			closeFileUpload.click();
			WebList.SelectItemByIndex(sourceLanguage, strIndexSrcLang);
			WebList.SelectItemByIndex(targetLanguages, strIndexTargetLang);
			nextStepBtn.click();
			driver.findElement(By.xpath("html/body/div[4]/div[4]/div/div[3]/div/div/ul/li/div/ul/li[1]"))
			.click();
			externalBidless.click();
			selectedBidlessVendors.click();
			ProjectDatesViewdeliveryDueDate.click();
			General.selectDate(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		nextStepBtn.click();
		CommonUtil.Wait(WaitTool.DEFAULT_WAIT_4_PAGE);
		finishBtn.click();
		popup_ok.click();
	}

	public void SimpleMultipleFilesProjectCreation(String strUsrName, String strPassword,
			CharSequence strPrjName, String strIndexIntendedUse, String strIndexPriority,
			String strDirPath, String strIndexSrcLang, String strIndexTargetLang, String date, String VendorName) {

		LoginUtil.doLogin(strUsrName, strPassword);
		try {
			newCreateBtn.click();
			newProjectCreateBtn.click();
			prjName.sendKeys(strPrjName);
			WebList.SelectItemByIndex(intendedUse, strIndexIntendedUse);
			WebList.SelectItemByIndex(priority, strIndexPriority);
			nextStepBtn.click();
			selectSourceFileLink.click();
			int fileCount = FileUtil.getFilesCount(strDirPath);
			for (int i = 0; i < fileCount - 1; i++) {
				addNewFileLink.click();
			}
			ArrayList<String> l = FileUtil.listFilesAndFilesSubDirectories(strDirPath);
			System.out.println(l);
			for (int i = 1; i <= fileCount; i++) {
				String part1 = "html/body/div[11]/div/div[9]/div/div/div[1]/div[2]/div[2]/div[";
				String endPart = "]/div[2]/form/a/input";
				WebElement element = driver.findElement(By.xpath(part1 + i + endPart));
				System.out.println(l.get(i - 1));
				General.AttachFile(element, l.get(i - 1));
				CommonUtil.Wait(3);
			}
			uploadBtn.click();
			CommonUtil.Wait(30);
			CommonExpectedConditions.elementToBeClickable(closeFileUpload);
			closeFileUpload.click();
			WebList.SelectItemByIndex(sourceLanguage, strIndexSrcLang);
			WebList.SelectItemByIndex(targetLanguages, strIndexTargetLang);

			nextStepBtn.click();
			CommonExpectedConditions.elementToBeClickable(nextStepBtn);
			Thread.sleep(5000L);
			nextStepBtn.click();
			externalBidless.click();
			List<WebElement> lsitBidlessVendor = driver.findElements(By.xpath("//*[@id='selected-bidless-vendors-display']/li"));
			for (int i = 1; i <= lsitBidlessVendor.size(); i++) {
				if (driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]/div/span")).getAttribute("title").equalsIgnoreCase(VendorName)) {
					driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]")).click();
					break;
				}
			}
			//selectedBidlessVendors.click();
			ProjectDatesViewdeliveryDueDate.click();
			General.selectDate(date);
			nextStepBtn.click();
			WaitTool.waitForElementByWebElement(driver, finishBtn, WaitTool.DEFAULT_WAIT_4_ELEMENT);
			finishBtn.click();
			popup_ok.click();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}catch (InterruptedException e) {
			// TODO: handle exception
		}

	}

	public void LocalProjectCreation(String strUsrName, String strPassword,
			CharSequence strPrjName, String strIntendedUse, String strPriority, String strDirPath,
			String strSrcLang, String strTargetLang, String date, String VendorName) {
		WaitTool.setImplicitWait(driver, 15);
		LoginUtil.doLogin(strUsrName, strPassword);
		ProjectDetails(strPrjName, strIntendedUse, strPriority);
		try {
			if (driver.findElements(By.id("project-select-STANDARD")).size()!=0) {
				standardBtn.click();
			}
			selectSourceFileLink.click();
			int fileCount = FileUtil.getFilesCount(PublicVariables.SORUCEFILEPATH);
			for (int i = 0; i < fileCount - 1; i++) {
				addNewFileLink.click();
			}
			ArrayList<String> l = FileUtil.listFilesAndFilesSubDirectories(PublicVariables.SORUCEFILEPATH);
			System.out.println(l);
			for (int i = 1; i <= fileCount; i++) {
				String part1 = "html/body/div[11]/div/div[9]/div/div/div[1]/div[2]/div[2]/div[";
				String endPart = "]/div[2]/form/a/input";
				WebElement element = driver.findElement(By.xpath(part1 + i + endPart));
				System.out.println(l.get(i - 1));
				General.AttachFile(element, l.get(i - 1));
				CommonUtil.Wait(3);
			}
			uploadBtn.click();
			CommonUtil.Wait(60);
			closeFileUpload.click();
			WebList.SelectItemByVisibleText(sourceLanguage, strSrcLang);
			String[] targetLang = strTargetLang.split(",");
			for (int i = 0; i < targetLang.length; i++) {
				WebList.SelectItemByVisibleText(targetLanguages, targetLang[i]);
			}
			CommonExpectedConditions.elementToBeClickable(nextStepBtn);
			Thread.sleep(5000L);
			nextStepBtn.click();
			externalBidless.click();
			List<WebElement> lsitBidlessVendor = driver.findElements(By.xpath("//*[@id='selected-bidless-vendors-display']/li"));
			for (int i = 1; i <= lsitBidlessVendor.size(); i++) {
				if (driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]/div/span")).getAttribute("title").equalsIgnoreCase(VendorName)) {
					driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]")).click();
					break;
				}
			}
			//selectedBidlessVendors.click();
			ProjectDatesViewdeliveryDueDate.click();
			General.selectDate(date);
			nextStepBtn.click();
			CommonUtil.Wait(15);
			finishBtn.click();
			popup_ok.click();
		} catch (ParseException e) {
			e.printStackTrace();
		}catch (InterruptedException e) {
			// TODO: handle exception
		}

	}

	public void ProjectDetails(CharSequence strPrjName, String strIntendedUse, String strPriority) {
		newCreateBtn.click();
		newProjectCreateBtn.click();
		prjName.sendKeys(strPrjName);
		WebList.SelectItemByVisibleText(intendedUse, strIntendedUse);
		WebList.SelectItemByVisibleText(priority, strPriority);
		CommonUtil.Wait(WaitTool.DEFAULT_WAIT_4_PAGE);
		nextStepBtn.click();
	}

	public void MarketoProjectCreation(String strUsrName, String strPassword,
			String DeliveryType, CharSequence strPrjName, String strIntendedUse, String strPriority,String SrcMaterials, String strSrcLang,
			String strTargetLang, String templateStatus, String template, String date, String strfileName, String VendorName)
					throws ParseException, InterruptedException {
		LoginUtil.doLogin(strUsrName, strPassword);
		ProjectDetails(strPrjName, strIntendedUse, strPriority);
		marketoBtn.click();
		CommonUtil.Wait(10);
		if (DeliveryType.equalsIgnoreCase("New Asset")) {
			newassestLink.click();
		} else if (DeliveryType.equalsIgnoreCase("New Program")) {
			newprogramLink.click();
		} else if (DeliveryType.equalsIgnoreCase("Dynamic Content")) {
			dynamicontentLink.click();
		}
		String[] file = strfileName.split(",");
		List<WebElement> query = null;
		if (SrcMaterials.equalsIgnoreCase("marketing activities")) {
			for (int i = 0; i < file.length; i++) {
				marketingTreeSearch.clear();
				marketingTreeSearch.sendKeys(file[i]);
				marketingTreeSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				expandBtn.click();
				CommonUtil.Wait(10);
				query = driver.findElements(By.xpath("//*[(text()='"+file[i]+"')]//parent::span/span[2]"));
				for (WebElement element : query) {
					element.click();
				}
			}
		} else if (SrcMaterials.equalsIgnoreCase("design studio")) {
			designTabLink.click();
			for (int i = 0; i < file.length; i++) {
				designTreeSearch.clear();
				designTreeSearch.sendKeys(file[i]);
				designTreeSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				expandBtn.click();
				CommonUtil.Wait(10);
				query = driver.findElements(By.xpath("//*[contains(text(),'"+file[i]+"')]//parent::span/span[2]"));
				for (WebElement element : query) {
					element.click();
				}
			}
		}
		assetsecondstep.click();
		WebList.SelectItemByVisibleText(sourcelanguageselect, strSrcLang);
		String[] targetLang = strTargetLang.split(",");
		for (int i = 0; i < targetLang.length; i++) {
			WebList.SelectItemByVisibleText(targetLangLink, targetLang[i]);
		}
		if (templateStatus.equalsIgnoreCase("true")) {
			overrideTemplate.click();
			CommonUtil.Wait(WaitTool.DEFAULT_WAIT_4_ELEMENT);
			for (int i = 1; i <= query.size(); i++) {
				for (int i1 = 1; i1 <= targetLang.length; i1++) {
					String xpath = "//*[@id='overrideTemplateLightbox']/div[2]/div/div["+i+"]/div/table/tbody/tr[" + i1 + "]/td[2]/select";
					WebElement query1 = driver.findElement(By.xpath(xpath));
					WebList.SelectItemByVisibleText(query1, template);
				}
			}
			Thread.sleep(5000L);
			saveTemplateOverrideBtn.click();
		}
		CommonExpectedConditions.elementToBeClickable(nextStepBtn);
		Thread.sleep(5000L);
		nextStepBtn.click();
		externalBidless.click();
		List<WebElement> lsitBidlessVendor = driver.findElements(By.xpath("//*[@id='selected-bidless-vendors-display']/li"));
		for (int i = 1; i <= lsitBidlessVendor.size(); i++) {
			if (driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]/div/span")).getAttribute("title").equalsIgnoreCase(VendorName)) {
				driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]")).click();
				break;
			}
		}
		//selectedBidlessVendors.click();
		ProjectDatesViewdeliveryDueDate.click();
		General.selectDate(date);
		nextStepBtn.click();
		CommonUtil.Wait(15);
		finishBtn.click();
		popup_ok.click();
	}

	public void EloquaProjectCreation(String strUsrName, String strPassword,
			String DeliveryType, CharSequence strPrjName, String strIntendedUse, String strPriority,String SrcMaterials, String strSrcLang,
			String strTargetLang, String templateStatus, String template, String date, String strfileName, String VendorName)
					throws ParseException, InterruptedException {
		LoginUtil.doLogin(strUsrName, strPassword);
		ProjectDetails(strPrjName, strIntendedUse, strPriority);
		eloquaBtn.click();
		CommonUtil.Wait(10);
		if (DeliveryType.equalsIgnoreCase("New Asset")) {
			newassestLink.click();
		} else if (DeliveryType.equalsIgnoreCase("Dynamic Content")) {
			eloquadynamicontentLink.click();
		}
		String[] file = strfileName.split(",");
		WebElement query = null;
		if (SrcMaterials.equalsIgnoreCase("landing page")) {
			landingPageTabLink.click();
			for (int i = 0; i < file.length; i++) {
				landingPageAssetSearch.clear();
				landingPageAssetSearch.sendKeys(file[i]);
				landingPageAssetSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				driver.findElement(By.xpath("html/body/div[4]/div[4]/div/div[3]/div/div/ul/li/div/div[4]/div[3]/div/div[1]/div[3]/div/div[4]/table/thead/tr/th[1]/div/input")).click();
				driver.findElement(By.id("landingPageClearSearchText")).click();
			}

		} else if (SrcMaterials.equalsIgnoreCase("emails")) {
			emailsTabLink.click();
			for (int i = 0; i < file.length; i++) {
				emailAssetSearch.clear();
				emailAssetSearch.sendKeys(file[i]);
				emailAssetSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				driver.findElement(By.xpath("//*[@id='selectUnselectAll-email']")).click();
				driver.findElement(By.id("emailClearSearchText")).click();
			}
		}else if (SrcMaterials.equalsIgnoreCase("forms")) {
			formsTabLink.click();
			for (int i = 0; i < file.length; i++) {
				formAssetSearch.clear();
				formAssetSearch.sendKeys(file[i]);
				formAssetSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				driver.findElement(By.xpath("//*[@id='selectUnselectAll-form']"));
				driver.findElement(By.id("formClearSearchText")).click();
			}
		}
		assetsecondstep.click();
		CommonUtil.Wait(10);
		WebList.SelectItemByVisibleText(sourcelanguageselect, strSrcLang);
		String[] targetLang = strTargetLang.split(",");
		for (int i = 0; i < targetLang.length; i++) {
			WebList.SelectItemByVisibleText(targetLangLink, targetLang[i]);
		}
		if (templateStatus.equalsIgnoreCase("true")) {
			overrideTemplate.click();
			CommonUtil.Wait(WaitTool.DEFAULT_WAIT_4_ELEMENT);
			query = driver.findElement(By.xpath("//*[@id='eloquaChooseLanguages']/div[3]/table"));
			int count = WebTable.getRowCount(query);
			for (int i = 1; i <= count; i++) {
				for (int i1 = 1; i1 <= targetLang.length; i1++) {
					String xpath = "//*[@id='overrideTemplateLightbox']/div[2]/div/div["+i+"]/div/table/tbody/tr[" + i1 + "]/td[2]/select";
					WebElement query1 = driver.findElement(By.xpath(xpath));
					WebList.SelectItemByVisibleText(query1, template);
				}
			}
			Thread.sleep(5000L);
			saveTemplateOverrideBtn.click();
		}
		CommonExpectedConditions.elementToBeClickable(nextStepBtn);
		Thread.sleep(5000L);
		nextStepBtn.click();
		externalBidless.click();
		List<WebElement> lsitBidlessVendor = driver.findElements(By.xpath("//*[@id='selected-bidless-vendors-display']/li"));
		for (int i = 1; i <= lsitBidlessVendor.size(); i++) {
			if (driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]/div/span")).getAttribute("title").equalsIgnoreCase(VendorName)) {
				driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]")).click();
				break;
			}
		}
		//selectedBidlessVendors.click();
		ProjectDatesViewdeliveryDueDate.click();
		General.selectDate(date);
		nextStepBtn.click();
		CommonUtil.Wait(15);
		finishBtn.click();
		popup_ok.click();
	}

	public void HubSpotProjectCreation(String strUsrName, String strPassword,
			String DeliveryType, CharSequence strPrjName, String strIntendedUse, String strPriority,String SrcMaterials, String strSrcLang,
			String strTargetLang, String templateStatus, String template, String date, String strfileName)
					throws ParseException, InterruptedException {
		LoginUtil.doLogin(strUsrName, strPassword);
		ProjectDetails(strPrjName, strIntendedUse, strPriority);
		hubspotBtn.click();
		CommonUtil.Wait(10);
		String[] file = strfileName.split(",");
		List<WebElement> query = null;
		if (SrcMaterials.equalsIgnoreCase("landing page")) {
			landingPageTabLink.click();
			for (int i = 0; i < file.length; i++) {
				landingPageAssetSearch.clear();
				landingPageAssetSearch.sendKeys(file[i]);
				landingPageAssetSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				driver.findElement(By.xpath("html/body/div[4]/div[4]/div/div[3]/div/div/ul/li/div/div[4]/div[3]/div/div[1]/div[3]/div/div[4]/table/thead/tr/th[1]/div/input")).click();
				driver.findElement(By.id("landingPageClearSearchText")).click();
			}

		} else if (SrcMaterials.equalsIgnoreCase("emails")) {
			emailsTabLink.click();
			for (int i = 0; i < file.length; i++) {
				emailAssetSearch.clear();
				emailAssetSearch.sendKeys(file[i]);
				emailAssetSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				driver.findElement(By.xpath("//*[@id='selectUnselectAll-email']"));
				driver.findElement(By.id("emailClearSearchText")).click();
			}
		}else if (SrcMaterials.equalsIgnoreCase("forms")) {
			formsTabLink.click();
			for (int i = 0; i < file.length; i++) {
				formAssetSearch.clear();
				formAssetSearch.sendKeys(file[i]);
				formAssetSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				driver.findElement(By.xpath("//*[@id='selectUnselectAll-form']"));
				driver.findElement(By.id("formClearSearchText")).click();
			}
		}
		assetsecondstep.click();
		WebList.SelectItemByVisibleText(sourcelanguageselect, strSrcLang);
		String[] targetLang = strTargetLang.split(",");
		for (int i = 0; i < targetLang.length; i++) {
			WebList.SelectItemByVisibleText(targetLangLink, targetLang[i]);
		}
		nextStepBtn.click();
		externalBidless.click();
		selectedBidlessVendors.click();
		ProjectDatesViewdeliveryDueDate.click();
		General.selectDate(date);
		nextStepBtn.click();
		WaitTool.waitForElementByWebElement(driver, finishBtn, WaitTool.DEFAULT_WAIT_4_ELEMENT);
		finishBtn.click();
		popup_ok.click();
	}

	public void MarketoProjectRestAPI(String strUsrName, String strPassword,
			String DeliveryType, CharSequence strPrjName, String strIntendedUse, String strPriority,String SrcMaterials, String strSrcLang,
			String strTargetLang, String templateStatus, String template, String date, String strfileName, String VendorName)
					throws ParseException, InterruptedException {
		LoginUtil.doLogin(strUsrName, strPassword);
		ProjectDetails(strPrjName, strIntendedUse, strPriority);
		marketoBtn.click();
		CommonUtil.Wait(10);
		if (DeliveryType.equalsIgnoreCase("New Asset")) {
			newassestLink.click();
		} else if (DeliveryType.equalsIgnoreCase("New Program")) {
			newprogramLink.click();
		} else if (DeliveryType.equalsIgnoreCase("Dynamic Content")) {
			dynamicontentLink.click();
		}
		String[] file = strfileName.split(",");
		List<WebElement> query = null;
		if (SrcMaterials.equalsIgnoreCase("marketing activities")) {
			for (int i = 0; i < file.length; i++) {
				marketingTreeSearch.clear();
				marketingTreeSearch.sendKeys(file[i]);
				marketingTreeSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				expandBtn.click();
				CommonUtil.Wait(10);
				query = driver.findElements(By.xpath("//*[(text()='"+file[i]+"')]//parent::span/span[2]"));
				for (WebElement element : query) {
					element.click();
				}
			}
		} else if (SrcMaterials.equalsIgnoreCase("design studio")) {
			designTabLink.click();
			for (int i = 0; i < file.length; i++) {
				designTreeSearch.clear();
				designTreeSearch.sendKeys(file[i]);
				designTreeSearch.sendKeys(Keys.ENTER);
				CommonUtil.Wait(10);
				expandBtn.click();
				CommonUtil.Wait(10);
				query = driver.findElements(By.xpath("//*[contains(text(),'"+file[i]+"')]//parent::span/span[2]"));
				for (WebElement element : query) {
					element.click();
				}
			}
		}
		assetsecondstep.click();
		WebList.SelectItemByVisibleText(sourcelanguageselect, strSrcLang);
		String[] targetLang = strTargetLang.split(",");
		for (int i = 0; i < targetLang.length; i++) {
			WebList.SelectItemByVisibleText(targetLangLink, targetLang[i]);
		}
		if (templateStatus.equalsIgnoreCase("true")) {
			overrideTemplate.click();
			CommonUtil.Wait(WaitTool.DEFAULT_WAIT_4_ELEMENT);
			for (int i = 1; i <= query.size(); i++) {
				for (int i1 = 1; i1 <= targetLang.length; i1++) {
					String xpath = "//*[@id='overrideTemplateLightbox']/div[2]/div/div["+i+"]/div/table/tbody/tr[" + i1 + "]/td[2]/select";
					WebElement query1 = driver.findElement(By.xpath(xpath));
					WebList.SelectItemByVisibleText(query1, template);
				}
			}
			Thread.sleep(5000L);
			saveTemplateOverrideBtn.click();
		}
		CommonExpectedConditions.elementToBeClickable(nextStepBtn);
		Thread.sleep(5000L);
		nextStepBtn.click();
		externalBidless.click();
		List<WebElement> lsitBidlessVendor = driver.findElements(By.xpath("//*[@id='selected-bidless-vendors-display']/li"));
		for (int i = 1; i <= lsitBidlessVendor.size(); i++) {
			if (driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]/div/span")).getAttribute("title").equalsIgnoreCase(VendorName)) {
				driver.findElement(By.xpath("//*[@id='selected-bidless-vendors-display']/li["+i+"]")).click();
				break;
			}
		}
		//selectedBidlessVendors.click();
		ProjectDatesViewdeliveryDueDate.click();
		General.selectDate(date);
		nextStepBtn.click();
		WaitTool.waitForElementByWebElement(driver, finishBtn, WaitTool.DEFAULT_WAIT_4_ELEMENT);
		finishBtn.click();
		popup_ok.click();
	}



}
