/**
 *
 */
package com.cloudwords.qe.fw.pageobjects.indesign;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.libraries.CommonExpectedConditions;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.utillib.FileUtil;
import com.cloudwords.qe.fw.wrapper.General;
import com.cloudwords.qe.fw.wrapper.WaitTool;
import com.cloudwords.qe.fw.wrapper.WebList;
import com.relevantcodes.extentreports.ExtentTest;


/**
 * @author gaurav
 * @date 05-Oct-2016
 */
public class ProjectCreationPage extends LoadableComponent<ProjectCreationPage>{

  /* (non-Javadoc)
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub

  }

  public ProjectCreationPage(WebDriver driver, ExtentTest test) {

    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

@FindBy(how = How.ID, using = "create-new-button")
private WebElement newCreateBtn;

@FindBy(how = How.XPATH, using = "//*[@id='_dropdown_0']/li[1]/a")
private WebElement newProjectCreateBtn;

@FindBy(how = How.ID, using = "projectName")
private WebElement prjName;

@FindBy(how = How.ID, using = "intendedUse")
private WebElement intendedUse;

@FindBy(how = How.ID, using = "priority")
private WebElement priority;

@FindBy(how = How.ID, using = "projectDescription")
private WebElement projectDescription;

@FindBy(how = How.XPATH, using = "//*[@id='project_tags']/div/input")
private WebElement tags;

@FindBy(how = How.ID, using = "nextStepBtnId")
private WebElement nextStepBtn;

@FindBy(how = How.ID, using = "sourceLanguage")
private WebElement sourceLanguage;

@FindBy(how = How.ID, using = "languageSelectViewSelector")
private WebElement targetLanguages;

@FindBy(how = How.ID, using = "selectSourceFileLink")
private WebElement selectSourceFileLink;

@FindBy(how = How.CLASS_NAME, using = "_fileUploadBinary")
private WebElement fileUploadLink;

@FindBy(xpath = "//*[contains(@class,'addNewFileLink')]")
private WebElement addNewFileLink;

@FindBy(xpath = "//*[contains(@class,'doFileUpload')]")
private WebElement uploadBtn;

@FindBy(xpath = "//*[contains(@class,'closeFileUpload')]")
private WebElement closeFileUpload;

@FindBy(how = How.ID, using = "selectRefFileLink")
private WebElement selectRefFileLink;

@FindBy(xpath = "//*[@id='wizardContent']/ul/li/div/div[3]/label[2]/input")
private WebElement externalBidless;

@FindBy(xpath = "//*[@id='selected-vendors-display']/li")
private WebElement selectedVendors;

@FindBy(xpath = "//*[@id='selected-bidless-vendors-display']/li")
private WebElement selectedBidlessVendors;

@FindBy(id = "ProjectDatesViewbidDueDate")
private WebElement ProjectDatesViewbidDueDate;

@FindBy(id = "ProjectDatesViewdeliveryDueDate")
private WebElement ProjectDatesViewdeliveryDueDate;

@FindBy(xpath = "//*[@id='finish-link']")
private WebElement finishBtn;

@FindBy(id = "popup_ok")
private WebElement popup_ok;

@FindBy(id = "popup_cancel")
private WebElement popup_cancel;

@FindBy(id = "view-project-button")
private WebElement viewProjectBtn;

@FindBy(id = "cloneProjectButton")
private WebElement cloneProjectBtn;
@FindBy(xpath = "//*[@id='vendor-type-select']/li[2]")
private WebElement internalTranslatorSelection;

@FindBy(xpath = "//span[contains(text(), 'IT_gaurav IT_gupta')]")
private WebElement internalTranslatorSelectionByName;

@FindBy(id = "selectWorkflow")
private WebElement selectWorkFlowBtn;

@FindBy(id = "configure-mode-advanced")
private WebElement setReviewDateAndReviewerRadioBtn;

@FindBy(css = "#step_1 .assignee-select-dropdown")
private WebElement selectReviewerDropDown;

// popup_ok
@FindBy(id = "popup_ok")
private WebElement acceptBtnOnRequestTranslationPopUp;

// .select-arrow
@FindBy(css = ".select-arrow")
private WebElement reviewDateBtn;
// selectWorkflow

private WebDriver driver;

public String SimpleMultipleFilesProjectCreationForInDesign(String strUsrName, String strPassword,
    CharSequence strPrjName, String strIndexIntendedUse, String strIndexPriority,
    String strDirPath, String strIndexSrcLang, String strIndexTargetLang, String translationDate,
    String reviewDate, String reviewerName) {
  LoginUtil.doLogin(strUsrName, strPassword);
  newCreateBtn.click();
  newProjectCreateBtn.click();
  prjName.sendKeys(strPrjName);
  WebList.SelectItemByIndex(intendedUse, strIndexIntendedUse);
  WebList.SelectItemByIndex(priority, strIndexPriority);
  nextStepBtn.click();
  selectSourceFileLink.click();
  int fileCount = FileUtil.getFilesCount(strDirPath);
  for (int i = 0; i < fileCount - 1; i++) {
    addNewFileLink.click();
  }
  ArrayList<String> l = FileUtil.listFilesAndFilesSubDirectories(strDirPath);
  System.out.println(l);
  for (int i = 1; i <= fileCount; i++) {
    String part1 = "html/body/div[11]/div/div[9]/div/div/div[1]/div[2]/div[2]/div[";
    String endPart = "]/div[2]/form/a/input";
    WebElement element = driver.findElement(By.xpath(part1 + i + endPart));
    System.out.println(l.get(i - 1));
    General.AttachFile(element, l.get(i - 1));
    CommonUtil.Wait(3);
  }
  uploadBtn.click();
  CommonUtil.Wait(30);
  CommonExpectedConditions.elementToBeClickable(closeFileUpload);
  closeFileUpload.click();
  WebList.SelectItemByIndex(sourceLanguage, strIndexSrcLang);
  WebList.SelectItemByIndex(targetLanguages, strIndexTargetLang);
  nextStepBtn.click();
  // select internal translator
  WaitTool.waitForElementByWebElement(driver, internalTranslatorSelection, 5).click();
  // select internal translator by name
  WaitTool.waitForElementByWebElement(driver, internalTranslatorSelectionByName, 5).click();
  ProjectDatesViewdeliveryDueDate.click();
  try {
    General.selectDate(translationDate);
  } catch (ParseException e) {
    e.printStackTrace();
  }
  nextStepBtn.click();
  CommonUtil.Wait(3);
  // driver.findElement(By.id("tab-8")).click();
  WaitTool.waitForElementByWebElement(driver, selectWorkFlowBtn, 5).click();
  driver.findElement(By.cssSelector("#step_1 .select-arrow")).click();
  CommonUtil.Wait(3);
  try {
    General.selectDate(reviewDate);
  } catch (ParseException e) {
    e.printStackTrace();
  }
  CommonUtil.Wait(3);
  /*
   * List<WebElement> lWeb = driver.findElements(By.cssSelector("#step_2 .select-arrow"));
   * lWeb.get(0).click(); CommonUtil.Wait(3); try { General.selectDate(reviewDate); } catch
   * (ParseException e) { e.printStackTrace(); } CommonUtil.Wait(3); lWeb =
   * driver.findElements(By.cssSelector("#step_2 .select-arrow")); lWeb.get(1).click();
   * CommonUtil.Wait(3); try { General.selectDate(reviewDate); } catch (ParseException e) {
   * e.printStackTrace(); }
   */
  Select sel = new Select(selectReviewerDropDown);
  sel.selectByVisibleText(reviewerName);

  /*
   * sel= new Select(driver.findElement(By.cssSelector("#step_2 .assignee-select-dropdown")));
   * sel.selectByVisibleText("reviewer_gaurav reviewer_gupta (Reviewer)");
   */
  finishBtn.click();
  WaitTool.waitForElementByWebElement(driver, acceptBtnOnRequestTranslationPopUp, 5).click();
  CommonUtil.Wait(5);
  //System.out.println("Project creation test case completed");
  driver.findElement(By.id("view-project-button")).click();
  CommonUtil.Wait(3);
  driver.findElement(By.id("tab_details")).click();
  String projectId = driver.findElement(By.xpath("//div[4]/p[1]/span")).getText();
  return projectId;
}

public void waitUntilElementGetsValue(final String elementId, final String value, int timeOut, int pollingTime) {
  new FluentWait<WebDriver>(driver).withTimeout(timeOut, TimeUnit.SECONDS)
      .pollingEvery(pollingTime, TimeUnit.SECONDS).ignoring(NoSuchElementException.class)
      .until(new ExpectedCondition<Boolean>() {
        public Boolean apply(WebDriver wd) {
          driver.navigate().refresh();
          CommonUtil.Wait(3);
          WebElement element = wd.findElement(By.id(elementId));
          return element.getText().equalsIgnoreCase(value);
        }
      });
}
}
