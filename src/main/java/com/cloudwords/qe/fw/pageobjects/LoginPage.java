/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.cloudwords.qe.fw.libraries.PageLoadHelper;
import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author Shamsher Singh
 * @date 16-Aug-2016
 */
public class LoginPage extends LoadableComponent<LoginPage> {

  private final WebDriver driver;
  private static final Logger logger = Logger.getLogger(LoginPage.class);

  /**
   *
   */
  public LoginPage(WebDriver driver, ExtentTest testReport) {
    this.driver = driver;
    PageFactory.initElements(driver, this);

  }

  @FindBy(how = How.ID, using = "j_username")
  private WebElement eleUsrName;

  @FindBy(how = How.ID, using = "j_password")
  private WebElement elePassword;

  @FindBy(how = How.ID, using = "login-button")
  private WebElement loginButton;

  @FindBy(how = How.ID, using = "j_remember_email")
  private WebElement eleRememberMyEmail;

  @FindBy(how = How.ID, using = "forgot-password-link")
  private WebElement eleForgotPwdLink;

  @FindBy(how = How.XPATH, using = "//*[@id='loginContainer']/p[2]/a")
  private WebElement eleSignLinkedAcc;

  @FindBy(how = How.XPATH, using = "//*[@id='loginContainer']/div[1]")
  private WebElement errorMsg;

  /**
   * Page Objects related methods
   */

  @Override
  protected void isLoaded() throws Error {

    PageLoadHelper.isLoaded().isElementIsVisible(driver, eleUsrName)
        .isElementIsVisible(driver, elePassword).isElementIsClickable(driver, loginButton);
  }

  public boolean isPasswordHidden() {
    if (elePassword.getAttribute("type").equals("password"))
      return true;
    else
      return false;
  }

  public String alertText() {
    return errorMsg.getText();

  }

  public boolean isAlertDisp() {
    return errorMsg.isDisplayed();
  }

  public LoginPage enterPassWord(String passWordValue) {
    elePassword.clear();
    elePassword.sendKeys(passWordValue);
    return this;
  }

  public void clickLoginBtn() {
    loginButton.click();
  }

  public LoginPage enetrUserName(String userNameValue) {
    eleUsrName.clear();
    eleUsrName.sendKeys(userNameValue);
    return this;
  }

  @Override
  protected void load() {

  }

}
