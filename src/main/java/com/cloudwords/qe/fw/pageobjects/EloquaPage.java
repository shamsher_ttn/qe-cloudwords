/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.HasInputDevices;
import org.openqa.selenium.interactions.Mouse;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.wrapper.WebButton;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author shamsher
 * @date 13-Nov-2016
 */
public class EloquaPage extends LoadableComponent<EloquaPage>{

	private final WebDriver driver;
	private static final Logger logger = Logger.getLogger(EloquaPage.class);

	/**
	 *
	 */
	public EloquaPage(WebDriver driver, ExtentTest testReport) {
		this.driver = driver;
		PageFactory.initElements(driver, this);

	}

	@FindBy(id="sitename")
	private WebElement CompanyName;

	@FindBy(id="username")
	private WebElement Username;

	@FindBy(id="password")
	private WebElement Password;

	@FindBy(id="submitButton")
	private WebElement SubmitButton;

	@FindBy(xpath="//*[@id='navigation-list-item-assets']/a")
	private WebElement assetsLnk;

	@FindBy(xpath="//*[@id='navigation-list-item-assets-emails']/a")
	private WebElement emailsLink;

	@FindBy(xpath="//*[@id='sc20099-2']/label")
	private WebElement createdbymeLink;

	@FindBy(xpath="//*[@id='navigation-list-item-user']/a/span/i[2]")
	private WebElement profileLink;

	@FindBy(xpath="//*[@id='navigation-list-item-user-logout']/a")
	private WebElement logout;

	public void EloquaLogin(){
		CompanyName.sendKeys("TechnologyPartnerCloudwords");
		Username.sendKeys("Shamsher.singh");
		Password.sendKeys("Raghav123$");
		SubmitButton.click();

	}

	public void DeleteContent(String programName) throws InterruptedException{

		Locatable hoverItem = (Locatable)assetsLnk;
		Mouse mouse = ((HasInputDevices)driver).getMouse();
		mouse.mouseMove(hoverItem.getCoordinates());
		emailsLink.click();
		driver.findElement(By.xpath("html/body/div[4]/div[1]/div/div[2]/div/div[2]/div/div[1]/div[5]/div[3]/label")).click();
		Thread.sleep(10000L);
		int rowCount = driver.findElements(By.xpath("html/body/div[4]/div[1]/div/div[2]/div/div[2]/div/div[2]/div/div[6]/div/div[1]/div[2]/div/div")).size();
		if (!(rowCount==0)) {
			for (int i = 1; i <= rowCount; i++) {
				driver.findElement(By.xpath("html/body/div[4]/div[1]/div/div[2]/div/div[2]/div/div[2]/div/div[6]/div/div[1]/div[2]/div/div[1]/div[1]")).click();
				WebElement locator = driver.findElement(By.xpath("html/body/div[4]/div[1]/div/div[2]/div/div[2]/div/div[2]/div/div[6]/div/div[1]/div[2]/div/div[1]/div[1]"));
				WebButton.RightClick(locator);
				Automation.testReport.log(LogStatus.INFO , "Deleting Program Folder");
				driver.findElement(By.xpath("//span[text()='Delete']")).click();
				driver.findElement(By.xpath("//*[@id='warning-alert-pane-view-buttonOne']/span/label")).click();
				Thread.sleep(5000L);
			}
		}
		hoverItem = (Locatable) profileLink;
		mouse = ((HasInputDevices)driver).getMouse();
		mouse.mouseMove(hoverItem.getCoordinates());
		profileLink.click();
		logout.click();
		driver.switchTo().alert().accept();
		//*[@id='navigation-list-item-user']/a/span/i[1]
		//*[@id='navigation-list-item-user-logout']/a
	}

	/* (non-Javadoc)
	 * @see org.openqa.selenium.support.ui.LoadableComponent#load()
	 */
	@Override
	protected void load() {
		// TODO Auto-generated method stub

	}


	/* (non-Javadoc)
	 * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
	 */
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub

	}

}
