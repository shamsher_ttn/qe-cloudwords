package com.cloudwords.qe.fw.pageobjects;

import java.io.IOException;
import java.util.Iterator;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.asserts.SoftAssert;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.cloudwords.qe.fw.base.Automation;
import com.relevantcodes.extentreports.ExtentTest;

public class ReviewTranslationPage extends LoadableComponent<ReviewTranslationPage> {

	private static final Logger logger = Logger.getLogger(ReviewTranslationPage.class);
	private WebDriver driver;

	@FindBy(how = How.CSS, using = ".icon-single-arrow-rt")
	private WebElement scrolltoBottom;

	@FindBy(how = How.CSS, using = "#maximizeTable")
	private WebElement maxTable;

	@FindBy(how = How.CSS, using = ".pagination>li.active")
	private WebElement defaultActivePage;

	@FindBy(how = How.CSS, using = ".pagination>li")
	private List<WebElement> pages;

	public ReviewTranslationPage(WebDriver driver, ExtentTest testReport) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	public void scrollToWebElement(WebDriver driver, WebElement element) throws InterruptedException {
		((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView();", element);
		Thread.sleep(5000);
	}

	public NodeList readXMLData(String xmlFilePath, String xmlTagName)
			throws ParserConfigurationException, SAXException, IOException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(xmlFilePath);
		NodeList nodeList = document.getElementsByTagName(xmlTagName);
		return nodeList;
	}

	public void maximizeTable(WebDriver driver) throws InterruptedException {
		WebElement maxTable = driver.findElement(By.cssSelector("#maximizeTable"));
		maxTable.click();
		Thread.sleep(5000);
	}

	public void writeWebDataToExcel(WebDriver driver, WebElement locator, XSSFRow excelRow, int cellNumXL) {
		String rowText = locator.getAttribute("innerText");
		XSSFCell excelCell = excelRow.createCell(cellNumXL);
		excelCell.setCellType(XSSFCell.CELL_TYPE_STRING);
		excelCell.setCellValue(rowText.trim());
	}

	public XSSFRow writeXMLToExcel(String testName, XSSFSheet spreadSheet, NodeList nodeList, int j, int k,
			int cellNumXML) {
		XSSFCell cell;
		XSSFRow excelRow = null;

		if (testName == "TestTranslation") {
			excelRow = spreadSheet.createRow(k + 1);
		} else if (testName == "TestTargetTranslation") {
			excelRow = spreadSheet.getRow(k + 1);
			if (excelRow == null) {
				excelRow = spreadSheet.createRow(k + 1);
			}
		}

		if (k <= nodeList.getLength()) {
			cell = excelRow.createCell(cellNumXML);
			cell.setCellType(XSSFCell.CELL_TYPE_STRING);
			cell.setCellValue((nodeList.item(k - 1).getTextContent().trim()));

		}
		return excelRow;
	}

	public void clickPageAndScrollToBottom(WebDriver driver, WebElement element, int pagecount, int i)
			throws InterruptedException {
		if (i > 2 && i <= pagecount) {
		      driver.findElement(By.cssSelector(".pagination>li:nth-child(" + (i + 2) + ")>a")).click();
		      Thread.sleep(3000);
		      if (i != pagecount) {
		        WebElement scroll = driver.findElement(By.cssSelector(".pagination>li.active"));
		        scrollToWebElement(driver, scroll);
		      }
		    } else if (i == 2) {
		      driver.findElement(By.cssSelector(".pagination>li:nth-child(" + i + ")>a")).click();
		      Thread.sleep(3000);
		      WebElement scroll = driver.findElement(By.cssSelector(".pagination>li.active"));
		      scrollToWebElement(driver, scroll);
		}
	}

	public void compareWebAndXMLData(XSSFSheet spreadSheet, String strKey, int cell1, int cell2, int celldiff,
			CellStyle style) {
		Iterator<Row> rowIterator = spreadSheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			// System.out.println(row);
			if (row.getRowNum() == 1) {
				row.createCell(Integer.parseInt(Automation.appconfigHashMap.get(strKey)))
						.setCellValue(Automation.appconfigHashMap.get("diffCellHeader"));
				row.getCell(Integer.parseInt(Automation.appconfigHashMap.get(strKey))).setCellStyle(style);
			} else {
				if ((row.getPhysicalNumberOfCells() == 2) || (row.getPhysicalNumberOfCells() > 2)) {
					if ((row.getCell(cell1).getStringCellValue().trim())
							.compareTo(row.getCell(cell2).getStringCellValue().trim()) != 0) {
						row.createCell(Integer.parseInt(Automation.appconfigHashMap.get(strKey)))
								.setCellValue(row.getCell(celldiff).getStringCellValue().trim());
						row.getCell(Integer.parseInt(Automation.appconfigHashMap.get(strKey))).setCellStyle(style);
					} else {
						row.createCell(Integer.parseInt(Automation.appconfigHashMap.get(strKey)))
								.setCellValue("Web and XML data are Identical");
					}
				} else {
					row.createCell(Integer.parseInt(Automation.appconfigHashMap.get(strKey)))
							.setCellValue("Missing value in either of the cells");
					row.getCell(Integer.parseInt(Automation.appconfigHashMap.get(strKey))).setCellStyle(style);
				}
			}
		}
	}

	public void assertTranslationComparison(XSSFSheet spreadSheet, String strKey, int cell1, int cell2,
			SoftAssert s_assert) {
		Iterator<Row> rowIterator = spreadSheet.iterator();
		while (rowIterator.hasNext()) {
			Row row = rowIterator.next();
			if (row.getRowNum() > 1) {
				Automation.s_assert.assertFalse((row.getCell(cell1).getStringCellValue().trim() == null));
				Automation.s_assert.assertFalse((row.getCell(cell2).getStringCellValue().trim() == null));
				Automation.s_assert.assertTrue((row.getCell(cell1).getStringCellValue().trim())
						.compareTo(row.getCell(cell2).getStringCellValue().trim()) == 0);
				Automation.s_assert.assertTrue(
						(row.getCell(Integer.parseInt(Automation.appconfigHashMap.get(strKey))).getStringCellValue())
								.equals("Web and XML data are Identical"));
				// s_assert.assertAll();

			}
		}

	}

	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub

	}

	@Override
	protected void load() {
		// TODO Auto-generated method stub

	}
}
