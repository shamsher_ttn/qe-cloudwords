/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author shamsher
 * @date 05-Oct-2016
 */
public class ChangePassword extends LoadableComponent<ChangePassword> {

  private WebDriver driver;
  private static final Logger logger = Logger.getLogger(LoginPage.class);

  /**
   *
   */
  public ChangePassword(WebDriver driver, ExtentTest testReport) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  @FindBy(xpath="//*[@id='oldPassword']")
  private WebElement oldPassword;

  @FindBy(xpath="//*[@id='password']")
  private WebElement password;

  @FindBy(xpath="//*[@id='confirmPassword']")
  private WebElement confirmPassword;


  @FindBy(xpath="//*[@id='submit-button-bottom']")
  private WebElement submitBtn;

  @FindBy(xpath="//*[@id='passwordManagement']/div[3]/div/a[2]")
  private WebElement cancelBtn;


  public ChangePassword enterOldPwd(String oldPwd){
    oldPassword.clear();
    oldPassword.sendKeys(oldPwd);
    return this;

  }

  public ChangePassword enterNewPwd(String Pwd){
    password.clear();
    password.sendKeys(Pwd);
    return this;

  }
  public ChangePassword enterConfirmPwd(String newPwd){
    confirmPassword.clear();
    confirmPassword.sendKeys(newPwd);
    return this;

  }

  public void clickSubmitBtn() {
    submitBtn.click();
  }


  /* (non-Javadoc)
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub

  }

}
