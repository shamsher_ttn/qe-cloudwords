/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author Shamsher Singh
 * @date 16-Aug-2016
 */
public class CustomerAdminPage extends LoadableComponent<CustomerAdminPage> {

  private WebDriver driver;
  private static final Logger logger = Logger.getLogger(LoginPage.class);

  /**
   *
   */
  public CustomerAdminPage(WebDriver driver, ExtentTest testReport) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  @FindBy(how = How.CSS, using = ".nav-button.with-border.settings>a")
  private WebElement eleAccountDropDown;

  @FindBy(how = How.XPATH, using = "//*[@id='dropdown-nav-menu']/li[4]/a")
  private WebElement eleUserManagement;

  @FindBy(how = How.XPATH, using = "//*[@id='user_management']/div[1]/div[2]/a")
  private WebElement eleNewUser;

  @FindBy(how = How.XPATH, using = "//*[@id='new_user']/div[3]/fieldset/input[1]")
  private WebElement eleFirstName;

  @FindBy(how = How.XPATH, using = "//*[@id='new_user']/div[3]/fieldset/input[2]")
  private WebElement eleLastName;

  @FindBy(how = How.XPATH, using = "//*[@id='new_user']/div[3]/fieldset/input[3]")
  private WebElement eleEmail;

  @FindBy(how = How.ID, using = "user-roleType")
  private WebElement eleRole;

  @FindBy(how = How.CSS, using = ".control.includeInFollowersList")
  private WebElement checkBoxAvailableAsProjectFollower;

  @FindBy(how = How.CSS, using = ".#updateExistingProjectsCheckbox")
  private WebElement checkBoxApplytoAllCampaignsAndOpenProjects;

  @FindBy(how = How.CSS, using = "#languageSelectViewSelector")
  private WebElement dropDownLangaugde;


  @FindBy(how = How.ID, using = "save")
  private WebElement eleSave;

  @FindBy(how = How.XPATH, using = "//*[@id='new_user']/div[4]/div/a[2]")
  private WebElement eleCancel;


  /*
   * (non-Javadoc)
   *
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub

  }

  public Object createNewCustomer(String firstName, String lastName, String email, String role) {

    eleFirstName.sendKeys(firstName);
    eleLastName.sendKeys(lastName);
    eleEmail.sendKeys(email);
    eleRole.sendKeys(role);
    eleSave.click();
    return "user created";
  }


}
