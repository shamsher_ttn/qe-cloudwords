/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.openqa.selenium.support.ui.Select;

import com.cloudwords.qe.fw.base.Automation;
import com.cloudwords.qe.fw.wrapper.WaitTool;
import com.relevantcodes.extentreports.ExtentTest;
import com.relevantcodes.extentreports.LogStatus;

/**
 * @author Shamsher Singh
 * @date 16-Aug-2016
 */
public class LandingPage extends LoadableComponent<LandingPage> {

  private WebDriver driver;
  private static final Logger logger = Logger.getLogger(LandingPage.class);

  /**
   *
   */
  public LandingPage(WebDriver driver, ExtentTest testReport) {
    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  @FindBy(how = How.CSS, using = ".nav-button.with-border.settings>a")
  private WebElement eleAccountDropDown;

  @FindBy(how = How.XPATH, using = "//*[@id='dropdown-nav-menu']/li[4]/a")
  private WebElement eleUserManagement;

  @FindBy(how = How.XPATH, using = "//*[@id='user_management']/div[1]/div[2]/a")
  private WebElement eleNewUser;

  @FindBy(how = How.XPATH, using = "//*[@id='new_user']/div[3]/fieldset/input[1]")
  private WebElement eleFirstName;

  @FindBy(how = How.XPATH, using = "//*[@id='new_user']/div[3]/fieldset/input[2]")
  private WebElement eleLastName;

  @FindBy(how = How.XPATH, using = "//*[@id='new_user']/div[3]/fieldset/input[3]")
  private WebElement eleEmail;

  @FindBy(how = How.ID, using = "user-roleType")
  private WebElement eleRole;

  @FindBy(how = How.CSS, using = ".control.includeInFollowersList")
  private WebElement checkBoxAvailableAsProjectFollower;

  @FindBy(how = How.CSS, using = ".#updateExistingProjectsCheckbox")
  private WebElement checkBoxApplytoAllCampaignsAndOpenProjects;

  @FindBy(how = How.CSS, using = "#languageSelectViewSelector")
  private WebElement dropDownLangaugde;

  @FindBy(how = How.ID, using = "save")
  private WebElement eleSave;

  @FindBy(how = How.XPATH, using = ".//*[@id='new_user']/div[4]/div/a[2]")
  private WebElement eleCancel;

  @FindBy(how = How.CSS, using = "#edit-profile-button")
  private WebElement eleEdit;

  @FindBy(how = How.CSS, using = "#newSrcLangSelect")
  private WebElement eleSelectSrcLanguage;

  @FindBy(how = How.CSS, using = "#trgLangSelect-en")
  private WebElement eleSelectTragetLanguage;

  @FindBy(how = How.CSS, using = "#save-profile-button")
  private WebElement eleSaveButton;

  @FindBy(how = How.CSS, using = "#cust-language-pairs>p")
  private WebElement eleLanguagePair;

  /*
   * (non-Javadoc)
   *
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /*
   * (non-Javadoc)
   *
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub
  }

  public Object createNewPMUserbyCustomerAdmin(String firstName, String lastName, String email,
      String role) {
    eleAccountDropDown.click();
    logger.info("clicked on the user account dropdown menu");
    eleUserManagement.click();
    logger.info("clicked on the UserManagement in dropdown list");
    WaitTool.waitForElementByWebElement(driver, eleNewUser, 3);
    eleNewUser.click();
    logger.info("clicked on the new user button");
    WaitTool.waitForElementByWebElement(driver, eleFirstName, 3);
    eleFirstName.sendKeys(firstName);
    logger.info("entered the first name of the user");
    eleLastName.sendKeys(lastName);
    logger.info("entered the last name of the user");
    eleEmail.sendKeys(email);
    logger.info("entered the email of the user");
    Select sel = new Select(eleRole);
    sel.selectByVisibleText("Project Manager");
    logger.info("selected the program manager role from the drop down");
    eleSave.click();
    logger.info("user clicked on the save button");
    boolean userCreated;
    WaitTool.waitForElement(driver, By.cssSelector("span#notificationText"), 10);
    if (driver.findElement(By.cssSelector("span#notificationText")).getText()
        .contains("New User Added")) {
      userCreated = true;
      logger.info("notification message for the user created is validated successfully");
    } else {
      userCreated = false;
    }
    if (userCreated) {
      Automation.testReport.log(LogStatus.INFO, "PM User created successfully");
      // LandingPage landingPage = new LandingPage(driver ,Automation.testReport);
      UserRoleInfoPage page = new UserRoleInfoPage(driver, Automation.testReport);
      return page;
    } else {
      Automation.testReport.log(LogStatus.INFO, "PM user is not created");
      LandingPage LandingPage = new LandingPage(driver, Automation.testReport);
      return LandingPage;
    }
  }

  public Object createNewPRUserbyCustomerAdmin(String firstName, String lastName, String email,
      String role) {
    eleAccountDropDown.click();
    logger.info("clicked on the user account dropdown menu");
    eleUserManagement.click();
    logger.info("clicked on the UserManagement in dropdown list");
    WaitTool.waitForElementByWebElement(driver, eleNewUser, 3);
    eleNewUser.click();
    logger.info("clicked on the new user button");
    WaitTool.waitForElementByWebElement(driver, eleFirstName, 3);
    eleFirstName.sendKeys(firstName);
    logger.info("entered the first name of the user");
    eleLastName.sendKeys(lastName);
    logger.info("entered the last name of the user");
    eleEmail.sendKeys(email);
    logger.info("entered the email of the user");
    Select sel = new Select(eleRole);
    sel.selectByVisibleText("Project Requester");
    logger.info("selected the project requester role from the drop down");
    eleSave.click();
    logger.info("user clicked on the save button");
    boolean userCreated;
    WaitTool.waitForElement(driver, By.cssSelector("span#notificationText"), 10);
    if (driver.findElement(By.cssSelector("span#notificationText")).getText()
        .contains("New User Added")) {
      userCreated = true;
      logger.info("notification message for the user created is validated successfully");
    } else {

      userCreated = false;
    }
    if (userCreated) {
      Automation.testReport.log(LogStatus.INFO, "PM User created successfully");
      UserRoleInfoPage page = new UserRoleInfoPage(driver, Automation.testReport);
      return page;
    } else {
      Automation.testReport.log(LogStatus.INFO, "PM user is not created");
      LandingPage LandingPage = new LandingPage(driver, Automation.testReport);
      return LandingPage;
    }
  }

  public Object createNewReviewerUserbyCustomerAdmin(String firstName, String lastName,
      String email, String role) {
    eleAccountDropDown.click();
    logger.info("clicked on the user account dropdown menu");
    eleUserManagement.click();
    logger.info("clicked on the UserManagement in dropdown list");
    WaitTool.waitForElementByWebElement(driver, eleNewUser, 3);
    eleNewUser.click();
    logger.info("clicked on the new user button");
    WaitTool.waitForElementByWebElement(driver, eleFirstName, 3);
    eleFirstName.sendKeys(firstName);
    logger.info("entered the first name of the user");
    eleLastName.sendKeys(lastName);
    logger.info("entered the last name of the user");
    eleEmail.sendKeys(email);
    logger.info("entered the email of the user");
    Select sel = new Select(eleRole);
    sel.selectByVisibleText("Reviewer");
    logger.info("selected the Reviewer role from the drop down");
    eleSave.click();
    logger.info("user clicked on the save button");
    boolean userCreated;
    WaitTool.waitForElement(driver, By.cssSelector("span#notificationText"), 10);
    if (driver.findElement(By.cssSelector("span#notificationText")).getText()
        .contains("New User Added")) {
      userCreated = true;
      logger.info("notification message for the user created is validated successfully");
    } else {

      userCreated = false;
    }
    if (userCreated) {
      Automation.testReport.log(LogStatus.INFO, "PM User created successfully");
      UserRoleInfoPage page = new UserRoleInfoPage(driver, Automation.testReport);
      return page;
    } else {
      Automation.testReport.log(LogStatus.INFO, "PM user is not created");
      LandingPage LandingPage = new LandingPage(driver, Automation.testReport);
      return LandingPage;
    }
  }

  public Object createNewInternalTranslatorUserbyCustomerAdmin(String firstName, String lastName,
      String email, String role) {
    eleAccountDropDown.click();
    logger.info("clicked on the user account dropdown menu");
    eleUserManagement.click();
    logger.info("clicked on the UserManagement in dropdown list");
    WaitTool.waitForElementByWebElement(driver, eleNewUser, 3);
    eleNewUser.click();
    logger.info("clicked on the new user button");
    WaitTool.waitForElementByWebElement(driver, eleFirstName, 3);
    eleFirstName.sendKeys(firstName);
    logger.info("entered the first name of the user");
    eleLastName.sendKeys(lastName);
    logger.info("entered the last name of the user");
    eleEmail.sendKeys(email);
    logger.info("entered the email of the user");
    Select sel = new Select(eleRole);
    sel.selectByVisibleText("Internal Translator");
    logger.info("selected the Internal Translator role from the drop down");
    eleSave.click();
    logger.info("user clicked on the save button");
    boolean userCreated;
    WaitTool.waitForElement(driver, By.cssSelector("span#notificationText"), 10);
    if (driver.findElement(By.cssSelector("span#notificationText")).getText()
        .contains("New User Added")) {
      userCreated = true;
      logger.info("notification message for the user created is validated successfully");
    } else {

      userCreated = false;
    }
    if (userCreated) {
      Automation.testReport.log(LogStatus.INFO, "PM User created successfully");
      UserRoleInfoPage page = new UserRoleInfoPage(driver, Automation.testReport);
      return page;
    } else {
      Automation.testReport.log(LogStatus.INFO, "PM user is not created");
      LandingPage LandingPage = new LandingPage(driver, Automation.testReport);
      return LandingPage;
    }
  }

  public Object createNewAdminUserbyCustomerAdmin(String firstName, String lastName, String email,
      String role) {
    eleAccountDropDown.click();
    logger.info("clicked on the user account dropdown menu");
    eleUserManagement.click();
    logger.info("clicked on the UserManagement in dropdown list");
    WaitTool.waitForElementByWebElement(driver, eleNewUser, 3);
    eleNewUser.click();
    logger.info("clicked on the new user button");
    WaitTool.waitForElementByWebElement(driver, eleFirstName, 3);
    eleFirstName.sendKeys(firstName);
    logger.info("entered the first name of the user");
    eleLastName.sendKeys(lastName);
    logger.info("entered the last name of the user");
    eleEmail.sendKeys(email);
    logger.info("entered the email of the user");
    Select sel = new Select(eleRole);
    sel.selectByVisibleText("Administrator");
    logger.info("selected the Internal Translator role from the drop down");
    eleSave.click();
    logger.info("user clicked on the save button");
    boolean userCreated;
    WaitTool.waitForElement(driver, By.cssSelector("span#notificationText"), 10);
    if (driver.findElement(By.cssSelector("span#notificationText")).getText()
        .contains("New User Added")) {
      userCreated = true;
      logger.info("notification message for the user created is validated successfully");
    } else {

      userCreated = false;
    }
    if (userCreated) {
      Automation.testReport.log(LogStatus.INFO, "PM User created successfully");
      UserRoleInfoPage page = new UserRoleInfoPage(driver, Automation.testReport);
      return page;
    } else {
      Automation.testReport.log(LogStatus.INFO, "PM user is not created");
      LandingPage LandingPage = new LandingPage(driver, Automation.testReport);
      return LandingPage;
    }
  }

  public Object createNewInternalTranslatorUserbyCustomerAdminAndEnabled(String firstName,
      String lastName, String email, String role) throws InterruptedException {
    eleAccountDropDown.click();
    logger.info("clicked on the user account dropdown menu");
    eleUserManagement.click();
    logger.info("clicked on the UserManagement in dropdown list");
    WaitTool.waitForElementByWebElement(driver, eleNewUser, 3);
    eleNewUser.click();
    logger.info("clicked on the new user button");
    WaitTool.waitForElementByWebElement(driver, eleFirstName, 3);
    eleFirstName.sendKeys(firstName);
    logger.info("entered the first name of the user");
    eleLastName.sendKeys(lastName);
    logger.info("entered the last name of the user");
    eleEmail.sendKeys(email);
    logger.info("entered the email of the user");
    Select sel = new Select(eleRole);
    sel.selectByVisibleText("Internal Translator");
    logger.info("selected the Internal Translator role from the drop down");
    WaitTool
        .waitForElementByWebElement(driver,
            driver.findElement(By.cssSelector("input.control.availableAsInternalTranslator")), 5)
        .click();
    Thread.sleep(5000);
    eleSave.click();
    boolean setLanguage = false;
    logger.info("user clicked on the save button");
    boolean userCreated = false;
    WaitTool.waitForElement(driver, By.cssSelector("span#notificationText"), 10);
    if (driver.findElement(By.cssSelector("span#notificationText")).getText()
        .contains("New User Added")) {
      WaitTool.waitForElementByWebElement(driver, eleEdit, 3).click();
      logger.info("clicked on the edit user button");
      JavascriptExecutor jse = (JavascriptExecutor) driver;
      jse.executeScript("window.scrollTo(0, document.body.scrollHeight)");
      sel = new Select(eleSelectSrcLanguage);
      sel.selectByVisibleText("English");
      logger.info("selected the source language");
      sel = new Select(eleSelectTragetLanguage);
      sel.selectByVisibleText("French");
      logger.info("selected the translated language");
      eleSaveButton.click();
      logger.info("clicked the save button");
      if (eleLanguagePair.getText().equalsIgnoreCase("English To French")) {

        setLanguage = true;
      } else {
        setLanguage = false;
      }
    } else {

      userCreated = false;
    }
    if (userCreated && setLanguage) {
      Automation.testReport.log(LogStatus.INFO,
          "Internal Translator is created and enabled and successfully");
      UserRoleInfoPage page = new UserRoleInfoPage(driver, Automation.testReport);
      return page;
    } else {
      Automation.testReport.log(LogStatus.INFO,
          "Internal Translator is not created and enabled successfully");
      LandingPage LandingPage = new LandingPage(driver, Automation.testReport);
      return LandingPage;
    }
  }


}
