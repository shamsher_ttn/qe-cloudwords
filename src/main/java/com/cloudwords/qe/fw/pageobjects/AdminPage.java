/**
 * 
 */
package com.cloudwords.qe.fw.pageobjects;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author Shamsher Singh
 * @date 27-Aug-2016 
 */
public class AdminPage {
  
 private WebDriver driver;


/**
   * 
   */
  public AdminPage(WebDriver driver, ExtentTest testReport) {
    this.driver = driver;
    PageFactory.initElements(driver, this);

  }
  
  @FindBy(how = How.XPATH, using = "//*[@id='bodyMain']/div[4]/div/div[2]/div/p/a[1]")
  private WebElement custCreateLink ;

  @FindBy(how = How.XPATH, using = "//*[@id='bodyMain']/div[4]/div/div[2]/div/p/a[2]")
  private WebElement vendorCreateLink ;

  @FindBy(how = How.XPATH, using = "//*[@id='bodyMain']/div[4]/div/div[2]/div/p/a[2]")
  private WebElement emalMgmtLink;

  @FindBy(how = How.XPATH, using = "//*[@id='bodyMain']/div[4]/div/div[2]/div/a[1]")
  private WebElement custListLink ;

  @FindBy(how = How.XPATH, using = "//*[@id='bodyMain']/div[4]/div/div[2]/div/a[1]")
  private WebElement vendorListLink;

  
  public void clickvendorCreateLink(){
    vendorCreateLink.click();
  }
  
  public void clickCustCreateLink(){
    custCreateLink.click();
  }

}