/**
 *
 */
package com.cloudwords.qe.fw.pageobjects.indesign;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.cloudwords.qe.fw.apputil.LoginUtil;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author gaurav
 * @date 05-Oct-2016
 */
public class UploadTranslationPage extends LoadableComponent<ProjectCreationPage>{

  private WebDriver driver;

  public UploadTranslationPage(WebDriver driver, ExtentTest test) {

    this.driver = driver;
    PageFactory.initElements(driver, this);
  }

  public void selectProjectTask(String val) {

    // login with the user
    LoginUtil.loginWithCredentials("it_gaurav@gmail.com", "asdfghA2");

    CommonUtil.Wait(2);
    // click on the project tab
    driver.findElement(By.xpath("html/body/div[4]/div[2]/div/div/ul/li[2]/a/span")).click();

    CommonUtil.Wait(3);

    // getting project id column no
    String[] rowData = WebTable.StoreRowData(
        driver.findElement(
            By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div/div[3]/div[2]/div/div[2]/table")),
        "storeRow", "1");

    // System.out.println(">>>>>>>>>>>>>>>>>>>>>>>>"+rowData.length);

    // getting the projectID column no
    int i = 1;

    for (String data : rowData) {
      if (data.equalsIgnoreCase("Project ID")) {
        break;
      }
      i++;
    }

    int columnNo = i;

    // String[] s =Automation.STOREHASHMAP.get("").split(",");

    // getting all projectId's in list
    List<WebElement> listElements = driver.findElements(By.xpath(
        "html/body/div[4]/div[3]/div[1]/div[2]/div/div[3]/div[2]/div/div[2]/table/tbody/tr/td["
            + columnNo + "]"));

    System.out.println("size of the column " + listElements.size());

    for (int j = 0; j < listElements.size(); j++) {

      if (listElements.get(j).getText().equalsIgnoreCase(val)) {

        int rowNo = ((j + 1) * 2) - 1;

        CommonUtil.Wait(3);

        driver.findElement(By.xpath("//table/tbody/tr[" + rowNo + "]/td[2]/a")).click();
        break;
      }

    }
    CommonUtil.Wait(3);

    driver.findElement(By.id("tab_tasks")).click();

    CommonUtil.Wait(3);

    if (driver.findElement(By.xpath("//table/tbody/tr/td[2]/span/a")).getText()
        .equalsIgnoreCase("Respond to Internal Translation Request")) {

      driver.findElement(By.xpath("//table/tbody/tr/td[2]/span/a")).click();
    }
    CommonUtil.Wait(3);

    driver.findElement(By.id("accept-internal-translator-request")).click();

    driver.findElement(By.id("popup_ok")).click();

    CommonUtil.Wait(3);

    driver.findElement(By.id("tab_tasks")).click();

    CommonUtil.Wait(3);

    driver.findElement(By.xpath("//tbody/tr/td[2]/span/a")).click();

    CommonUtil.Wait(3);

   //driver.findElement(By.cssSelector("._fileUploadBinary")).click();

   // General.AttachFile(driver.findElement(By.cssSelector("._fileUploadBinary")), "/Users/gaurav/Desktop/testData_InDesign/translation");

    driver.findElement(By.cssSelector("._fileUploadBinary")).sendKeys("/Users/gaurav/Desktop/testData_InDesign/translation/French.zip");

    CommonUtil.Wait(3);

    driver.findElement(By.cssSelector(".btn.btn-blue.doFileUpload")).click();

    CommonUtil.Wait(3);

    driver.findElement(By.id("popup_ok")).click();

    CommonUtil.Wait(30);

    System.out.println("uploaded successfully");
  }

  /* (non-Javadoc)
   * @see org.openqa.selenium.support.ui.LoadableComponent#load()
   */
  @Override
  protected void load() {
    // TODO Auto-generated method stub

  }

  /* (non-Javadoc)
   * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
   */
  @Override
  protected void isLoaded() throws Error {
    // TODO Auto-generated method stub

  }

}
