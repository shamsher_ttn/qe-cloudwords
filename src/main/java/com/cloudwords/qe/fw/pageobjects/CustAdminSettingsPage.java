/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;

import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.wrapper.WebList;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.relevantcodes.extentreports.ExtentTest;

/**
 * @author shamsher
 * @date 18-Oct-2016
 */
public class CustAdminSettingsPage extends LoadableComponent<CustAdminSettingsPage>{

    private WebDriver driver;
    private static final Logger logger = Logger.getLogger(LoginPage.class);

    /**
     *
     */
    public CustAdminSettingsPage(WebDriver driver, ExtentTest testReport) {
	this.driver = driver;
	PageFactory.initElements(driver, this);
    }

    @FindBy(how = How.CSS, using = ".nav-button.with-border.settings>a")
    private WebElement eleAccountDropDown;

    @FindBy(how = How.XPATH, using = "//*[@id='dropdown-nav-menu']/li[1]/a")
    private WebElement adminSettings;

    @FindBy(how = How.XPATH, using = "//*[@id='settings_detail']/div/div/h5[8]/a")
    private WebElement marketoSettings;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[1]/div/a")
    private WebElement startBtn;

    @FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[1]/a")
    private WebElement saveBtn;

    @FindBy(how = How.XPATH, using = "//*[@id='settings_detail']/div/div/h5[1]/a")
    private WebElement myAccountLink;

    @FindBy(how = How.XPATH, using = "//*[@id='myAccount_integration_tab']/a")
    private WebElement integrationTab;

    @FindBy(how = How.XPATH, using = "//*[@id='marketo-username-input']")
    private WebElement marketoUserName;

    @FindBy(how = How.XPATH, using = "//*[@id='marketo-pw-input']")
    private WebElement marketoPwd;

    @FindBy(how = How.XPATH, using = "//*[@id='save-credentials-button']")
    private WebElement marketoSaveCredBtn;

    @FindBy(how = How.XPATH, using = "//*[@id='eloqua-companyname-input']")
    private WebElement companyNameEloqua;

    @FindBy(how = How.XPATH, using = "//*[@id='eloqua-username-input']")
    private WebElement eloquaUserName;

    @FindBy(how = How.XPATH, using = "//*[@id='eloqua-pw-input']")
    private WebElement eloquaPwd;

    @FindBy(how = How.XPATH, using = "//*[@id='save-eloqua-credentials-button']")
    private WebElement eloquaSaveBtn;

    @FindBy(how = How.XPATH, using = "//*[@id='nav-button-arrow']")
    private WebElement navBtn;

    /* (non-Javadoc)
     * @see org.openqa.selenium.support.ui.LoadableComponent#load()
     */
    @Override
    protected void load() {
	// TODO Auto-generated method stub

    }

    /* (non-Javadoc)
     * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
     */
    @Override
    protected void isLoaded() throws Error {
	// TODO Auto-generated method stub

    }

    public void marketoUserPermission(String UserRole , String Permission){
	eleAccountDropDown.click();
	adminSettings.click();
	CommonUtil.Wait(5);
	marketoSettings.click();
	//startBtn.click();
	WebElement tableWebelement = driver.findElement(By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/table"));
	int rowCount = WebTable.getRowCount(tableWebelement);
	String[] roleArray =UserRole.split(",");//{"Administrator","Project Manager"};

	for (int i = 0 ; i<roleArray.length;i++) {
	    for (int j = 1; j <rowCount; j++) {
		String elementPart1 = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/table/tbody/tr["+j+"]";
		String elementPart2="/td[2]";
		String userRole =driver.findElement(By.xpath(elementPart1+elementPart2)).getText();
		if (userRole.equalsIgnoreCase(roleArray[i])) {
		    WebElement query=driver.findElement(By.xpath(elementPart1+"/td[4]/select"));
		    WebList.SelectItemByVisibleText(query, Permission);
		}
	    }
	}
	saveBtn.click();
    }

    public void enableMarketoforUsers(String MarketoUserName , String MarketoPassword){
	try {
	    eleAccountDropDown.click();
	    adminSettings.click();
	    myAccountLink.click();
	    integrationTab.click();
	    marketoUserName.clear();
	    marketoUserName.sendKeys(MarketoUserName);
	    marketoPwd.sendKeys(MarketoPassword);
	    marketoSaveCredBtn.click();
	    Thread.sleep(10000L);

	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }

    public void enableEloquaforUsers(String CompanyName,String EloquaUserName , String EloquaPassword){
	try {
	    eleAccountDropDown.click();
	    adminSettings.click();
	    myAccountLink.click();
	    integrationTab.click();
	    companyNameEloqua.clear();
	    companyNameEloqua.sendKeys(CompanyName);
	    eloquaUserName.clear();
	    eloquaUserName.sendKeys(EloquaUserName);
	    eloquaPwd.clear();
	    eloquaPwd.sendKeys(EloquaPassword);
	    eloquaSaveBtn.click();
	    Thread.sleep(1000L);

	} catch (InterruptedException e) {
	    e.printStackTrace();
	}
    }


}

