/**
 *
 */
package com.cloudwords.qe.fw.pageobjects;

import java.sql.SQLException;
import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.cloudwords.qe.fw.dbqueries.IntegrationQueries;
import com.cloudwords.qe.fw.utillib.CommonUtil;
import com.cloudwords.qe.fw.wrapper.WebList;
import com.cloudwords.qe.fw.wrapper.WebTable;
import com.relevantcodes.extentreports.ExtentTest;
import com.thoughtworks.selenium.webdriven.commands.GetAllWindowTitles;

/**
 * @author shamsher
 * @date 18-Oct-2016
 */
public class CustSettingsPage extends LoadableComponent<CustSettingsPage> {

	private WebDriver driver;
	private static final Logger logger = Logger.getLogger(CustSettingsPage.class);

	/**
	 *
	 */
	public CustSettingsPage(WebDriver driver, ExtentTest testReport) {
		this.driver = driver;
		PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.CSS, using = ".nav-button.with-border.settings>a")
	private WebElement eleAccountDropDown;

	@FindBy(how = How.XPATH, using = "//*[@id='dropdown-nav-menu']/li[1]/a")
	private WebElement adminSettings;

	@FindBy(how = How.XPATH, using = "//*[@id='settings_detail']/div/div/h5[8]/a")
	private WebElement marketoSettings;

	@FindBy(how = How.XPATH, using = "//*[@id='settings_detail']/div/div/h5[9]/a")
	private WebElement eloquaSettings;

	@FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[1]/div/a")
	private WebElement startBtn;

	@FindBy(how = How.XPATH, using = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[1]/a")
	private WebElement saveBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='settings_detail']/div/div/h5[1]/a")
	private WebElement myAccountLink;

	@FindBy(how = How.XPATH, using = "//*[@id='myAccount_integration_tab']/a")
	private WebElement integrationTab;

	@FindBy(how = How.XPATH, using = "//*[@id='marketo-username-input']")
	private WebElement marketoUserName;

	@FindBy(how = How.XPATH, using = "//*[@id='marketo-pw-input']")
	private WebElement marketoPwd;

	@FindBy(how = How.XPATH, using = "//*[@id='save-credentials-button']")
	private WebElement marketoSaveCredBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='eloqua-companyname-input']")
	private WebElement companyNameEloqua;

	@FindBy(how = How.XPATH, using = "//*[@id='eloqua-username-input']")
	private WebElement eloquaUserName;

	@FindBy(how = How.XPATH, using = "//*[@id='eloqua-pw-input']")
	private WebElement eloquaPwd;

	@FindBy(how = How.XPATH, using = "//*[@id='save-eloqua-credentials-button']")
	private WebElement eloquaSaveBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='nav-button-arrow']")
	private WebElement navBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='settings_detail']/div/div/h5[8]/a")
	private WebElement hubspotSettings;

	@FindBy(how = How.ID, using = "hubspot-portalid-input")
	private WebElement hubID;

	@FindBy(how = How.ID, using = "authorize-hubspot-button")
	private WebElement authhubspotBtn;

	@FindBy(how = How.ID, using = "username")
	private WebElement hubspotUsrName;

	@FindBy(how = How.ID, using = "password")
	private WebElement hubspotPwd;

	@FindBy(how = How.ID, using = "loginBtn")
	private WebElement hubspotLoginBtn;

	@FindBy(how = How.XPATH, using = "//*[@id='authorize']/a[1]")
	private WebElement authorizeBtn;

	/*
	 * (non-Javadoc)
	 *
	 * @see org.openqa.selenium.support.ui.LoadableComponent#load()
	 */
	@Override
	protected void load() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 *
	 * @see org.openqa.selenium.support.ui.LoadableComponent#isLoaded()
	 */
	@Override
	protected void isLoaded() throws Error {
		// TODO Auto-generated method stub

	}

	public void marketoUserPermission(String UserEmail, String Permission) {
		try {
			eleAccountDropDown.click();
			adminSettings.click();
			CommonUtil.Wait(5);
			marketoSettings.click();
			if (driver.findElement(By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[1]/div/a"))
					.isDisplayed()) {
				startBtn.click();
			}
			WebElement tableWebelement = driver
					.findElement(By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/table"));
			int rowCount = WebTable.getRowCount(tableWebelement);
			String[] roleArray = UserEmail.split(",");
			for (int i = 0; i < roleArray.length; i++) {
				for (int j = 1; j < rowCount; j++) {
					String elementPart1 = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/table/tbody/tr["
							+ j + "]";
					String elementPart2 = "/td[1]";
					String userRole = driver.findElement(By.xpath(elementPart1 + elementPart2)).getText();

					String firstName = IntegrationQueries.getUserFirstName(roleArray[i]);
					String lastName = IntegrationQueries.getUserLastName(roleArray[i]);
					if (userRole.equalsIgnoreCase(firstName + " " + lastName)) {
						WebElement query = driver.findElement(By.xpath(elementPart1 + "/td[4]/select"));
						WebList.SelectItemByVisibleText(query, Permission);
					}
				}
			}
			saveBtn.click();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void eloquaUserPermission(String UserEmail, String Permission) {
		try {
			eleAccountDropDown.click();
			adminSettings.click();
			CommonUtil.Wait(5);
			eloquaSettings.click();
			if (driver.findElement(By.id("start-using-eloqua-button")).isDisplayed()) {
				startBtn.click();
			}
			WebElement tableWebelement = driver
					.findElement(By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/table"));
			int rowCount = WebTable.getRowCount(tableWebelement);
			String[] roleArray = UserEmail.split(",");
			for (int i = 0; i < roleArray.length; i++) {
				for (int j = 1; j < rowCount; j++) {
					String elementPart1 = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/table/tbody/tr["
							+ j + "]";
					String elementPart2 = "/td[1]";
					String userRole = driver.findElement(By.xpath(elementPart1 + elementPart2)).getText();

					String firstName = IntegrationQueries.getUserFirstName(roleArray[i]);
					String lastName = IntegrationQueries.getUserLastName(roleArray[i]);
					if (userRole.equalsIgnoreCase(firstName + " " + lastName)) {
						WebElement query = driver.findElement(By.xpath(elementPart1 + "/td[4]/select"));
						WebList.SelectItemByVisibleText(query, Permission);
					}
				}
			}
			saveBtn.click();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void enableMarketoforUsers(String MarketoUserName, String MarketoPassword) {
		try {
			eleAccountDropDown.click();
			CommonUtil.Wait(15);
			adminSettings.click();
			CommonUtil.Wait(5);
			myAccountLink.click();
			CommonUtil.Wait(5);
			integrationTab.click();
			marketoUserName.clear();
			marketoUserName.sendKeys(MarketoUserName);
			marketoPwd.sendKeys(MarketoPassword);
			marketoSaveCredBtn.click();
			Thread.sleep(10000L);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void enableEloquaforUsers(String CompanyName, String EloquaUserName, String EloquaPassword) {
		try {
			CommonUtil.Wait(5);
			eleAccountDropDown.click();
			CommonUtil.Wait(15);
			adminSettings.click();
			CommonUtil.Wait(5);
			myAccountLink.click();
			CommonUtil.Wait(5);
			integrationTab.click();
			companyNameEloqua.clear();
			companyNameEloqua.sendKeys(CompanyName);
			eloquaUserName.clear();
			eloquaUserName.sendKeys(EloquaUserName);
			eloquaPwd.clear();
			eloquaPwd.sendKeys(EloquaPassword);
			eloquaSaveBtn.click();
			Thread.sleep(1000L);

		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public void hubspotoUserPermission(String UserEmail, String Permission) {
		try {
			eleAccountDropDown.click();
			adminSettings.click();
			CommonUtil.Wait(5);
			hubspotSettings.click();
			if (driver.findElement(By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[1]/div/a"))
					.isDisplayed()) {
				startBtn.click();
			}
			WebElement tableWebelement = driver
					.findElement(By.xpath("html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/table"));
			int rowCount = WebTable.getRowCount(tableWebelement);
			String[] roleArray = UserEmail.split(",");
			for (int i = 0; i < roleArray.length; i++) {
				for (int j = 1; j < rowCount; j++) {
					String elementPart1 = "html/body/div[4]/div[3]/div[1]/div[2]/div/div/div[2]/div[2]/div[2]/table/tbody/tr["
							+ j + "]";
					String elementPart2 = "/td[1]";
					String userRole = driver.findElement(By.xpath(elementPart1 + elementPart2)).getText();

					String firstName = IntegrationQueries.getUserFirstName(roleArray[i]);
					String lastName = IntegrationQueries.getUserLastName(roleArray[i]);
					if (userRole.equalsIgnoreCase(firstName + " " + lastName)) {
						WebElement query = driver.findElement(By.xpath(elementPart1 + "/td[4]/select"));
						WebList.SelectItemByVisibleText(query, Permission);
					}
				}
			}
			saveBtn.click();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	public void enableHubSpotforUsers(String HubSpotID, CharSequence HubSpotUserName, CharSequence HubSpotPwd) {
		try {
			eleAccountDropDown.click();
			CommonUtil.Wait(15);
			adminSettings.click();
			CommonUtil.Wait(5);
			myAccountLink.click();
			CommonUtil.Wait(5);
			integrationTab.click();
			hubID.sendKeys(HubSpotID);
			authhubspotBtn.click();
			Set<String> handles = driver.getWindowHandles();
			if (handles.size() >= 2) {
				String firstWinHandle = driver.getWindowHandle();
				handles.remove(firstWinHandle);
				String winHandle = (String) handles.iterator().next();
				if (winHandle != firstWinHandle) {
					String secondWinHandle = winHandle; // Storing handle of second window handle

					// Switch control to new window
					driver.switchTo().window(secondWinHandle);
					CommonUtil.Wait(8);
					String Title = driver.getTitle();
					if (Title.equals("HubSpot Login")) {
						System.out.println(driver.getTitle());
						hubspotUsrName.clear();
						hubspotUsrName.sendKeys(HubSpotUserName);
						hubspotPwd.sendKeys(HubSpotPwd);
						hubspotLoginBtn.click();
						Thread.sleep(10000L);
						authorizeBtn.click();
						driver.switchTo().window(firstWinHandle);
					} else if (Title.equals("HubSpot")) {
						authorizeBtn.click();
						driver.switchTo().window(firstWinHandle);

					} else {
						System.out.println("You have landed on to incorrect WebPage");
						Assert.assertTrue(false);
					}
				}
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
					}
		}
}
