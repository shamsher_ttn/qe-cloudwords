/**
 *
 */
package com.cloudwords.qe.fw.libraries;

import java.io.InputStream;
import java.util.Properties;

/**
 * @author shamsher
 * @date 05-Nov-2016
 */
public class Messages {
  //public static String CONFIGERROR = "ConfigError";
  //public static String SELENIUMSERVERNOTFOUND="SeleniumServerNotFound";
  private static Properties messageList = new Properties();

  static {
    try { Messages singleObject = new Messages();
      InputStream input = singleObject.getClass().getClassLoader().getResourceAsStream("Messages.Properties");
      if (input != null) messageList.load(input);
    }
    catch (Exception e)
    {
      System.out.println(e.getMessage());
    }
  }

  public static String getProperty(String key)
  {
    return messageList.getProperty(key);
  }

}
