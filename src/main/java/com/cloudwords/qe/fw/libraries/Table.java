/**
 * 
 */
package com.cloudwords.qe.fw.libraries;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;

/**
 * @author Shamsher Singh
 * @date 13-Aug-2016
 */

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = { "row" })
@XmlRootElement(name = "table")
public class Table {

  @XmlElement(required = true)
  protected List<Row> row;

  public List<Row> getRow() {
    if (this.row == null) {
      this.row = new ArrayList<Row>();
    }
    return this.row;
  }

  public static Row createTableRow() {
    return new Row();
  }

  public static Table createTable() {
    return new Table();
  }

  public static Table.Row.Column createTableRowColumn() {
    return new Table.Row.Column();
  }

  @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(name = "", propOrder = { "column" })
  public static class Row {

    @XmlElement(required = true)
    protected List<Column> column;

    public List<Column> getColumn() {
      if (this.column == null) {
        this.column = new ArrayList<Column>();
      }
      return this.column;
    }

    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = { "value" })
    public static class Column {

      @XmlValue
      protected String value;

      @XmlAttribute(required = true)
      protected String name;

      public String getValue() {
        return this.value.toString();
      }

      public void setValue(String value) {
        this.value = value;
      }

      public String getName() {
        return this.name;
      }

      public void setName(String value) {
        this.name = value;
      }
    }
  }
}
