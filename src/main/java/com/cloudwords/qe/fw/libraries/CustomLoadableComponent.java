/**
 *
 */
package com.cloudwords.qe.fw.libraries;

import org.apache.log4j.Logger;

import com.cloudwords.qe.fw.utillib.FwUtil;


/**
 * @author Shamsher Singh
 * @date 03-Aug-2016
 */

public abstract class CustomLoadableComponent<T extends CustomLoadableComponent<T>> {

  private static final Logger logger = Logger.getLogger(CustomLoadableComponent.class);

  /**
   * @return
   */
  @SuppressWarnings("unchecked")
  public T get() {
    try {
      isLoaded();
      return (T) this;
    } catch (Exception e) {
      logger.error("Error encountered during page load: " + FwUtil.getStackTrace(e));
      load();
    }
    isLoaded();
    return (T) this;
  }

  protected abstract void load();

  protected abstract void isLoaded() throws Error;

}
