/**
 *
 */
package com.cloudwords.qe.fw.libraries;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

import com.cloudwords.qe.fw.libraries.Table.Row;
import com.cloudwords.qe.fw.libraries.Table.Row.Column;

/**
 * @author Shamsher Singh
 * @date 13-Aug-2016
 */
public class XMLFileHandler extends FileHandler {
  String xmlFilePath = "";

  /*
   * (non-Javadoc)
   *
   * @see com.cloudwords.Libraries.FileHandler#writeFile(java.sql.ResultSet, java.lang.String)
   */
  public boolean writeFile(ResultSet results, String filePath) {
    try {
      if (FileHandler.fileCreated != null) {
        JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { Table.class });
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty("jaxb.formatted.output", new Boolean(true));

        ResultSetMetaData metadata = results.getMetaData();

        Table table = Table.createTable();

        while (results.next()) {
          Table.Row row = Table.createTableRow();
          List<Column> columnsList = row.getColumn();
          List<Row> rowList = table.getRow();
          for (int i = 1; i <= metadata.getColumnCount(); i++) {
            Table.Row.Column column = Table.createTableRowColumn();
            column.setName(metadata.getColumnName(i));
            column.setValue(String.valueOf(results.getObject(i)));
            columnsList.add(column);
          }
          rowList.add(row);
        }

        marshaller.marshal(table, new FileOutputStream(filePath));
      }

      return true;
    } catch (Exception e) {
    }
    return false;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.cloudwords.Libraries.FileHandler#convertToTableData(java.lang.String)
   */
  public ArrayList<String[]> convertToTableData(String file) {
    ArrayList<String[]> result = null;
    try {
      result = new ArrayList<String[]>();
      JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { Table.class });
      Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
      Table xmlFileInTableFormat = (Table) unmarshaller.unmarshal(new FileInputStream(file));
      List<?> rowsList = xmlFileInTableFormat.getRow();
      String[] columsInEachResult = (String[]) null;
      int i = 0;
      if (i < rowsList.size()) {
        Table.Row row = (Table.Row) rowsList.get(i);
        List<?> columns = row.getColumn();
        columsInEachResult = new String[columns.size()];
        for (int j = 0; j < columns.size(); j++) {
          if (i != 0)
            break;
          columsInEachResult[j] = ((Table.Row.Column) columns.get(j)).getName();
        }

        result.add(columsInEachResult);
      }

      for (int i1 = 0; i1 < rowsList.size(); i1++) {
        Table.Row row = (Table.Row) rowsList.get(i1);
        List<?> columns = row.getColumn();
        columsInEachResult = new String[columns.size()];
        for (int j = 0; j < columns.size(); j++) {
          columsInEachResult[j] = ((Table.Row.Column) columns.get(j)).getValue();
        }

        result.add(columsInEachResult);
      }

      return result;
    } catch (Exception e) {
      result = null;
    }

    return result;
  }

  /*
   * (non-Javadoc)
   *
   * @see com.cloudwords.Libraries.FileHandler#convertToXMLData()
   */
  public void convertToXMLData() {
  }

  /*
   * (non-Javadoc)
   *
   * @see com.cloudwords.Libraries.FileHandler#writeTableData(java.util.ArrayList, java.lang.String)
   */
  public boolean writeTableData(ArrayList<String[]> results, String file) {
    boolean status = false;
    try {
      JAXBContext jaxbContext = JAXBContext.newInstance(new Class[] { Table.class });
      Marshaller marshaller = jaxbContext.createMarshaller();
      marshaller.setProperty("jaxb.formatted.output", new Boolean(true));

      Table table = Table.createTable();

      for (String[] result : results) {
        Table.Row row = Table.createTableRow();
        List<Column> columnsList = row.getColumn();
        List<Row> rowList = table.getRow();

        if (results.indexOf(result) == 0) {
          continue;
        }
        for (int i = 0; i < result.length; i++) {
          Table.Row.Column column = Table.createTableRowColumn();
          column.setName(((String[]) results.get(0))[i]);
          column.setValue(result[i]);
          columnsList.add(column);
        }
        rowList.add(row);
      }

      marshaller.marshal(table, new FileOutputStream(file));
      status = true;
    } catch (Exception e) {
      status = false;
    }
    return status;
  }

}
