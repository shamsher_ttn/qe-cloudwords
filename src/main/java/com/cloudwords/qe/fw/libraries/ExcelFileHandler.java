/**
 * 
 */
package com.cloudwords.qe.fw.libraries;

import java.io.File;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Locale;

import jxl.Cell;
import jxl.CellView;
import jxl.Sheet;
import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

/**
 * @author Shamsher Singh
 * @date 13-Aug-2016
 */

public class ExcelFileHandler extends FileHandler {
  WritableCellFormat arial12Header;
  WritableCellFormat arial10Cell;

  /*
   * (non-Javadoc)
   * 
   * @see com.cloudwords.Libraries.FileHandler#writeFile(java.sql.ResultSet, java.lang.String)
   */
  public boolean writeFile(ResultSet results, String filePath) {
    try {

      if (FileHandler.fileCreated != null) {
        WorkbookSettings wbSettings = new WorkbookSettings();
        wbSettings.setLocale(new Locale("en", "EN"));
        WritableWorkbook workbook = Workbook.createWorkbook(FileHandler.fileCreated, wbSettings);
        workbook.createSheet("Report", 0);
        WritableSheet excelSheet = workbook.getSheet(0);
        formatColumnHeaders();
        formatCell();

        for (int i = 0; i < results.getMetaData().getColumnCount(); i++) {
          Label l = new Label(i, 0, results.getMetaData().getColumnName(i + 1), this.arial12Header);
          excelSheet.addCell(l);
        }

        while (results.next()) {
          for (int i = 0; i < results.getMetaData().getColumnCount(); i++) {
            Label l = new Label(i, results.getRow(), String.valueOf(results.getObject(i + 1)),
                this.arial10Cell);
            excelSheet.addCell(l);
          }
        }
        workbook.write();
        workbook.close();
        return true;
      }

      return false;
    } catch (Exception e) {
    }
    return false;
  }

  /**
   * 
   */
  public void formatCell() {
    WritableFont times10pt = new WritableFont(WritableFont.ARIAL, 10);
    this.arial10Cell = new WritableCellFormat(times10pt);
    CellView cv = new CellView();

    cv.setFormat(this.arial10Cell);
    cv.setFormat(this.arial10Cell);

  }

  /**
   * 
   */
  public void formatColumnHeaders() {
    try {
      WritableFont arial12pt = new WritableFont(WritableFont.ARIAL, 12);
      this.arial12Header = new WritableCellFormat(arial12pt);
      this.arial12Header.setWrap(true);
    } catch (Exception localException) {
    }
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.cloudwords.Libraries.FileHandler#convertToTableData(java.lang.String)
   */
  public ArrayList<String[]> convertToTableData(String file) {
    String[] columnsInResult = (String[]) null;
    ArrayList<String[]> result = new ArrayList<String[]>();
    try {
      File excelFile = new File(file);
      Workbook workBook = Workbook.getWorkbook(excelFile);
      result = new ArrayList<String[]>();
      Sheet sheet = workBook.getSheet(0);

      for (int i = 0; i < sheet.getRows(); i++) {
        columnsInResult = new String[sheet.getColumns()];
        for (int j = 0; j < sheet.getColumns(); j++) {
          Cell cell = sheet.getCell(j, i);
          columnsInResult[j] = cell.getContents().toString();
        }
        result.add(columnsInResult);
      }
    } catch (Exception e) {
      result = null;
    }
    return result;
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.cloudwords.Libraries.FileHandler#convertToXMLData()
   */
  public void convertToXMLData() {
  }

  /*
   * (non-Javadoc)
   * 
   * @see com.cloudwords.Libraries.FileHandler#writeTableData(java.util.ArrayList, java.lang.String)
   */
  public boolean writeTableData(ArrayList<String[]> results, String file) {
    boolean status = false;
    try {
      WorkbookSettings wbSettings = new WorkbookSettings();
      wbSettings.setLocale(new Locale("en", "EN"));
      WritableWorkbook workbook = Workbook.createWorkbook(FileHandler.fileCreated, wbSettings);
      workbook.createSheet("Report", 0);
      WritableSheet excelSheet = workbook.getSheet(0);

      formatColumnHeaders();
      formatCell();

      for (String[] result : results) {
        for (int i = 0; i < result.length; i++) {
          Label l = new Label(i, results.indexOf(result), result[i], this.arial10Cell);
          excelSheet.addCell(l);
        }
      }
      workbook.write();
      workbook.close();
      status = true;
    } catch (Exception e) {
      status = false;
    }

    return status;
  }

}
