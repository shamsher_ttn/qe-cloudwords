/**
 * 
 */
package com.cloudwords.qe.fw.libraries;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * @author Shamsher Singh
 * @date 10-Aug-2016
 */
public class PageLoadHelper {

  /**
   * @return
   */
  public static PageLoadHelper isLoaded() {
    PageLoadHelper load = new PageLoadHelper();
    return load;
  }

  /**
   * @param driver
   * @param element
   * @return
   */
  public PageLoadHelper isElementIsClickable(WebDriver driver, WebElement element) {

    try {
      new WebDriverWait(driver, 10).until(ExpectedConditions.elementToBeClickable(element));
      return this;
    } catch (WebDriverException ex) {
      throw new Error("Element is not clickable");
    }
  }

  /**
   * @param driver
   * @param element
   * @return
   */
  public PageLoadHelper isElementIsVisible(WebDriver driver, WebElement element) {

    try {
      new WebDriverWait(driver, 10).until(ExpectedConditions.visibilityOf(element));
      return this;
    } catch (WebDriverException ex) {
      throw new Error("Element is not visible");
    }
  }

}
