package com.cloudwords.qe.fw.libraries;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * @author Shamsher Singh
 * @date 23-Aug-2016
 */
public class HighlightElement {

  /**
   * @param driver
   * @param element
   * @param result
   */
  public void highlightElement(WebDriver driver, WebElement element, String result) {
    if (result.equalsIgnoreCase("PASS")) {
      for (int i = 0; i < 2; i++) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
            "color: yellow; border: 2px solid yellow;");
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
      }
    } else {
      for (int i = 0; i < 2; i++) {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element,
            "color: red; border: 2px solid red;");
        js.executeScript("arguments[0].setAttribute('style', arguments[1]);", element, "");
      }
    }
  }

}
